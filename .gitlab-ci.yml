image: openjdk:11-jdk-slim-buster

variables:
  COURSIER_CACHE: $CI_PROJECT_DIR/sbt-cache/coursier
  SBT_OPTS: >
      -server -Xmx2G -XX:+CMSClassUnloadingEnabled -Xss2M
      -Dsbt.global.base=$CI_PROJECT_DIR/sbt-cache/sbtboot
      -Dsbt.boot.directory=$CI_PROJECT_DIR/sbt-cache/boot
      -Dsbt.ivy.home=$CI_PROJECT_DIR/sbt-cache/ivy

stages:
  - test
  - package
  - deploy
  - integration

cache:
  key: ${CI_COMMIT_REF_SLUG}
  untracked: true
  paths:
    - "$CI_PROJECT_DIR/sbt-cache"

testPublish:
  stage: test
  services:
    - postgres:latest
  variables:
    POSTGRES_URL: "jdbc:postgresql://postgres:5432/postgres"
    POSTGRES_USER: "postgres"
    POSTGRES_PASSWORD: "postgres"
    MASCARENE_DEV_DIR: $CI_PROJECT_DIR
  before_script:
    - apt-get update -y
    - apt-get install apt-transport-https gnupg2 -y
    # Install SBT
    - echo "deb http://dl.bintray.com/sbt/debian /" | tee -a /etc/apt/sources.list.d/sbt.list
    - apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 642AC823
    - apt-get update -y
    - apt-get install sbt -y
    - mkdir ~/.gnupg && chmod 700 ~/.gnupg
    - echo -e "use-agent\npinentry-mode loopback" > ~/.gnupg/gpg.conf
    - echo "allow-loopback-pinentry" > ~/.gnupg/gpg-agent.conf
    - echo $PGP_PASSPHRASE | gpg --no-tty --batch --import $GPG_USER_KEY
  script:
    - sbt -Dquill.macro.log=false clean test
    - sbt -Dquill.macro.log=false scalafmtCheck
    - sbt -Dquill.macro.log=false publishSigned
  artifacts:
    paths:
      - "sbt-cache"
      - "target"
      - "project/target"
      - "*/target"
    expire_in: 7 days
    reports:
      junit:
        - homeserver/target/test-reports/TEST-*.xml

package:native:
  stage: package
  before_script:
    - apt-get update -y
    - apt-get install apt-transport-https gnupg2 -y
    # Install SBT
    - echo "deb http://dl.bintray.com/sbt/debian /" | tee -a /etc/apt/sources.list.d/sbt.list
    - apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 642AC823
    - apt-get update -y
    - apt-get install sbt -y
  script:
    - sbt -Dquill.macro.log=false universal:packageZipTarball
  artifacts:
    paths:
      - "homeserver/target/universal"

package:dockerize:
  stage: package
  image: openjdk:11-jdk-buster
  tags:
    - rochefort
  before_script:
    - apt-get update -y
    - apt-get install apt-transport-https ca-certificates curl gnupg-agent gnupg2 software-properties-common -y
    - echo exit 0 > /usr/sbin/policy-rc.d
    # Install Docker
    - curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key add -
    - apt-key fingerprint 0EBFCD88
    - add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/debian $(lsb_release -cs) stable"
    - apt-get update -y
    - apt-get install docker-ce docker-ce-cli containerd.io -y
    - docker --version
    - docker run hello-world
    - docker version --format '{{.Server.APIVersion}}'
    # Install SBT
    - echo "deb http://dl.bintray.com/sbt/debian /" | tee -a /etc/apt/sources.list.d/sbt.list
    - apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 642AC823
    - apt-get update -y
    - apt-get install sbt -y
  script:
    - docker login -u $DOCKER_USER -p $DOCKER_TOKEN docker.io
    - sbt -Dquill.macro.log=false docker:publish
  artifacts:
    paths:
      - "homeserver/target/docker"

deploy:
  stage: deploy
  image: debian:buster-slim
  dependencies: []
  before_script:
    - apt-get update -y
    - apt-get install apt-transport-https ca-certificates curl gnupg-agent gnupg2 software-properties-common -y
    - 'which ssh-agent || ( apt-get update -y && apt-get install openssh-client -y )'
    - mkdir -p ~/.ssh
    - eval "$(ssh-agent -s)"
    - '[[ -f /.dockerenv ]] && echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config'
    - echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add -
    - chmod 600 ~/.ssh/config
    - ssh-keyscan -p 6791 snapshot.mascarene.org >> ~/.ssh/known_hosts
    - ssh-keyscan -p 6791 46.105.98.158 >> ~/.ssh/known_hosts
    - chmod 644 ~/.ssh/known_hosts

  script:
    - ssh -p 6791 mascarene@snapshot.mascarene.org "cd /home/mascarene/snapshot && docker-compose pull && docker-compose up -d"

sytest:
  stage: integration
  tags:
    - rochefort
  image: python:3.8-slim-buster
  dependencies: []
  before_script:
    - apt-get update -y
    - apt-get install apt-transport-https ca-certificates curl gnupg-agent gnupg2 software-properties-common -y
    - echo exit 0 > /usr/sbin/policy-rc.d
    # Install Docker
    - curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key add -
    - apt-key fingerprint 0EBFCD88
    - add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/debian $(lsb_release -cs) stable"
    - apt-get update -y
    - apt-get install docker-ce docker-ce-cli containerd.io -y
    - docker --version
    - docker run hello-world
    - docker version --format '{{.Server.APIVersion}}'

  script:
    - cd ./docker/sytest/
    - docker run mascarene/sytest:latest -I Manual -L https://snapshot.mascarene.org --server-name snapshot.mascarene.org -O tap > results.tap || true
    - python ./are-we-synapse-yet.py results.tap
  artifacts:
    paths:
    - ./docker/sytest/results.tap
