/*
Copyright 2018 Nicolas Jouanin

This file is part of Plasma.

Plasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License 3.

Plasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>
 */
import ReleaseTransformations._

ThisBuild / scalaVersion := "2.13.4"
ThisBuild / organization := "org.mascarene"
ThisBuild / organizationHomepage := Some(url("https://gitlab.com/mascarene/mascarene"))
ThisBuild / pomIncludeRepository := { _ => false }
ThisBuild / releaseProcess := Seq[ReleaseStep](
  checkSnapshotDependencies,
  inquireVersions,
  runClean,
  runTest,
  setReleaseVersion,
  commitReleaseVersion,
  tagRelease,
  releaseStepCommand("+publishSigned"),
  releaseStepCommand("sonatypeBundleRelease"),
  releaseStepCommand("docker:publish"),
  setNextVersion,
  commitNextVersion,
  pushChanges
)

lazy val root = (project in file("."))
  .aggregate(homeServer, matrixUtils)
  .settings(
    name := "mascarene"
  )

// Mascarene utilities
lazy val matrixUtils = (project in file("matrix-utils"))
  .settings(
    name := "matrix-utils",
    libraryDependencies := Dependencies.utilsDependencies
  )

// Mascarene SDK
lazy val matrixCoreApi = (project in file("matrix-core-api"))
  .settings(
    name := "matrix-core-api",
    libraryDependencies := Dependencies.commonDependencies ++ Dependencies.matrixApiDependencies
  )

// Matrix client API implementation
lazy val matrixClientApi = (project in file("matrix-client-api"))
  .settings(
    name := "matrix-client-api",
    libraryDependencies := Dependencies.commonDependencies
  )
  .dependsOn(matrixCoreApi, matrixClientApiModel)

// Matrix client API model
lazy val matrixClientApiModel = (project in file("matrix-client-api-model"))
  .settings(
    name := "matrix-client-api-model",
    libraryDependencies := Dependencies.commonDependencies
  )
  .dependsOn(matrixCoreApi)

// Matrix server API model
lazy val matrixServerApiModel = (project in file("matrix-server-api-model"))
  .settings(
    name := "matrix-server-api-model",
    libraryDependencies := Dependencies.commonDependencies
  )
  .dependsOn(matrixCoreApi)

// Matrix identity API client implementation
lazy val matrixIdentityApi = (project in file("matrix-identity-api"))
  .settings(
    name := "matrix-identity-api",
    libraryDependencies := Dependencies.commonDependencies
  )
  .dependsOn(matrixCoreApi, matrixIdentityApiModel)

// Matrix client API model
lazy val matrixIdentityApiModel = (project in file("matrix-identity-api-model"))
  .settings(
    name := "matrix-identity-api-model",
    libraryDependencies := Dependencies.commonDependencies
  )
  .dependsOn(matrixCoreApi)

// Mascarene Home Server
lazy val homeServer = (project in file("homeserver"))
  .enablePlugins(BuildInfoPlugin)
  .enablePlugins(JavaAppPackaging)
  .enablePlugins(DockerPlugin)
  .settings(
    name := "mascarene-homeserver",
    libraryDependencies ++= Dependencies.hsDependencies
  )
  .settings(
    buildInfoPackage := "org.mascarene.homeserver.version",
    buildInfoObject := "BuildInfo",
    buildInfoKeys := Seq[BuildInfoKey](name, version, "projectName" -> "mascarene-hs"),
    buildInfoOptions += BuildInfoOption.BuildTime,
    parallelExecution in Test := false
  )
  .settings(
    mainClass := Some("org.mascarene.homeserver.MascareneMain"),
    dockerBaseImage := "openjdk:11-jre-slim",
    dockerEntrypoint := Seq("/opt/docker/bin/mascarene-main"),
    dockerExposedPorts := Seq(2551, 8080),
    dockerUsername := Some("mascarene"),
    dockerUpdateLatest := true,
    bashScriptExtraDefines += """addJava "-Dconfig.file=${app_home}/../conf/mascarene.conf"""",
    batScriptExtraDefines += """call :add_java "-Dconfig.file=%APP_HOME%\conf\mascarene.conf""""
  )
  .dependsOn(
    matrixUtils,
    matrixCoreApi,
    matrixClientApiModel,
    matrixClientApi,
    matrixServerApiModel
  )
