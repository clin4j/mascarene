## Mascarene cluster sample configuration

This samples provides a complete solution for running a Mascarene cluster. 
It runs 2 mascarene nodes behind a Nginx load balancing proxy.  

### Configuration

Before running it, you need to create a database user:

```
docker-compose exec db psql -U postgres postgres
postgres=# CREATE USER mascarene WITH ENCRYPTED PASSWORD 'some_password';
postgres=# CREATE DATABASE mascarene OWNER mascarene;
```

You can also check `mascarene_node_1.conf`, `mascarene_node_2.conf` and `nginx.conf` to adapt the configuration
to your network environment.

Finally, you can download and extract [Riot](https://github.com/vector-im/riot-web) in the `riot/` folder. 
Make sure you set up `config.json` to point to the proxy API:

```
"default_server_config": {
        "m.homeserver": {
            "base_url": "https://192.168.1.16/",  <-- set to nginx proxy exposed address/port
            "server_name": "localhost"
        },
...
    },
```

### Running

Once done, you can simply run the configuration:

```
docker-compose up -d
```

If needed, more cluster nodes can be deployed by duplicating an existing node configuration.

