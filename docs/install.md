# Installing Mascarene

## Installing from fat JAR

This installation consists in installing and runnning the JAR provided by mascarene and which containts the application 
compiled code with all its dependencies. 

This installation requires you to provide the following requirements:
 * POSIX-compliant system
 * PostgreSQL 11 or above
 
Before installing Mascarene, you need to prepare the database environment. Depending on your choice you can create a new 
database or create a specific schema into an existing database. Whatever you choose, we recommend you to create a 
database user specifically for mascarene.

```
$ psql
postgres=# CREATE USER mascarene WITH ENCRYPTED PASSWORD 'some_password';
postgres=# CREATE DATABASE mascarene OWNER mascarene;
```   

Once the database is up and running, we recommend creating a system user for installing and running mascarene. Let's say 
you've created a `mascarene` user for this purpose. 
 * Download mascarene latest release package from *TODO: provide a place to download native packages*
 * un-archive the downloaded archive into the installation directory
 
 ```
$ mkdir -p ~/mascarene
$ wget https://gitlab.com/mascarene/mascarene/-/jobs/564910990/artifacts/raw/homeserver/target/universal/mascarene-homeserver-0.0.1-SNAPSHOT.tgz?inline=false
$ tar xzvf mascarene-homeserver-0.0.1-SNAPSHOT.tgz
$ cd mascarene-homeserver-0.0.1-SNAPSHOT
```  

You can run mascarene with the provided script `./bin/mascarene-main`, but before **you must configure it**.


## Installing from Docker image

Mascarene is available as a [docker image](https://hub.docker.com/repository/docker/mascarene/mascarene-homeserver). You
can run it directly by providing an external configuration file :

```
docker run -v /some_path/conf/mascarene.conf:/opt/docker/conf/mascarene.conf mascarene/mascarene-homeserver:latest
```

For using deploying mascarene with `docker-compose` see configuration examples available in the project `docker/samples`
directory.


## Configuration

Below is the annotated configuration file, called `mascarene.conf`. The file location is given to mascarene as a 
command-line argument (`-Dconfig.file=/some/path/mascarene.conf`).


```
# Sample file for mascarene configuration

mascarene {
  db {
    #Database connection URL. Use the following scheme (ex: "jdbc:postgresql://localhost:5432/postgres")
    url="jdbc:postgresql://localhost:5432/postgres"
    #Database connection credentials
    username="postgres"
    password="postgres"
  }
  clustering {
    # When using clustering, specifiy the hostname and port used for akka cluster communications
    bind-hostname = "localhost"
    bind-port = 2551
    # seed-nodes must contain the current cluster member and at least one other cluster member.
    # Don't change the actorSystem name
    seed-nodes = [
      "akka://MascareneHS@localhost:2551"]
  }
  server {
    # Domain to use for matrix identifier domain part (ex: user@domain-name)
    domain-name = "localhost"
    # URL to use as base for building server destination URLs
    base-url = "https://localhost"
    federation {
      delegate-server = "localhost:8448"
    }
    client-api {
      # Address/port on which the webserver should listen for client API calls
      bind-address = "localhost:8080"
    }
    federation-api {
      # Address/port on which the webserver should listen for federation API calls
      bind-address = "localhost:8448"
    }
    secret {
      #Secret key for encoding JSON tokens.
      #DON'T USE THIS DEFAULT VALUE. Use one of the following methods
      # openssl rand -base64 32
      # head -c 32 /dev/urandom | base64
      key = "CHANGE_ME"
    }
  }
  matrix {
      interactive-auth = {
        register = {
          "flows": [
            {
              "stages": [ "m.login.dummy" ]
            },
            {
              "stages": [ "example.type.foo", "example.type.baz" ]
            }
          ],
          "params": {
            "example.type.baz": {
              "example_key": "foobar"
            }
          }
        }
        login = {
          "flows": [
            {
              "type": "m.login.password"
            }
          ]
        }
        deactivate = {
          "flows": [
            {
              "type": "m.login.password"
            }
          ]
        }
      }
  }
}
```
