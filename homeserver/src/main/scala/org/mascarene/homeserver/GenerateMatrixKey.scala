package org.mascarene.homeserver

import java.io.{File, FileWriter, OutputStreamWriter}
import java.security.{SecureRandom, Security}

import com.typesafe.scalalogging.LazyLogging
import org.bouncycastle.crypto.generators.Ed25519KeyPairGenerator
import org.bouncycastle.crypto.params.{Ed25519KeyGenerationParameters, Ed25519PrivateKeyParameters}
import org.bouncycastle.jce.provider.BouncyCastleProvider
import org.bouncycastle.util.io.pem.{PemHeader, PemObject, PemWriter}
import org.mascarene.homeserver.version.BuildInfo
import org.mascarene.utils.Codecs
import scopt.OParser

import scala.jdk.CollectionConverters._

case class GeneratorConfig(outputFile: Option[File] = None)

object GenerateMatrixKey extends LazyLogging {
  def main(args: Array[String]): Unit = {
    logger.debug(s"Mascarene MatrixKey generator (${BuildInfo.version})")
    Security.addProvider(new BouncyCastleProvider())
    val provider = Security.getProvider(BouncyCastleProvider.PROVIDER_NAME)
    logger.debug(s"Using ${provider.getInfo}")
    val secRandom = new SecureRandom()

    //Generate private key
    val keyPairGenerator = new Ed25519KeyPairGenerator
    keyPairGenerator.init(new Ed25519KeyGenerationParameters(secRandom))
    val asymmetricCipherKeyPair = keyPairGenerator.generateKeyPair
    val privateKey              = asymmetricCipherKeyPair.getPrivate.asInstanceOf[Ed25519PrivateKeyParameters]

    //Generate key ID
    val keyBytes = new Array[Byte](5)
    secRandom.nextBytes(keyBytes)
    val keyId = "ed25519:" + Codecs.toBase64(keyBytes, unpadded = true)

    OParser.parse(cmdLineParser(), args, GeneratorConfig()).foreach { generatorConfig =>
      val writer = generatorConfig.outputFile match {
        case None           => new OutputStreamWriter(System.out)
        case Some(fileName) => new FileWriter(fileName)
      }
      val pemKey = new PemObject(s"PRIVATE KEY", List(new PemHeader("keyId", keyId)).asJava, privateKey.getEncoded)
      val pw     = new PemWriter(writer)
      pw.writeObject(pemKey)
      pw.flush()
      pw.close()
    }
    logger.debug("Done")
  }

  def cmdLineParser() = {
    val builder = OParser.builder[GeneratorConfig]
    import builder._
    OParser.sequence(
      programName("Mascarene MatrixKey generator"),
      head("GenerateMatrixKey", BuildInfo.version),
      opt[File]('f', "output")
        .valueName("<file>")
        .action((f, config) => config.copy(outputFile = Some(f)))
        .text("key .pem output file"),
      help("help").text("prints this usage text")
    )
  }
}
