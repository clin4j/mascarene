/*
 * Mascarene
 * Copyright (C) 2020  mascarene.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.mascarene.homeserver

import java.io.Closeable
import akka.actor.typed.ActorSystem
import com.typesafe.config.{Config, ConfigFactory, ConfigValueFactory}
import com.typesafe.scalalogging.LazyLogging
import de.mkammerer.argon2.{Argon2Factory, Argon2Helper}
import io.getquill.{PostgresJdbcContext, SnakeCase}
import org.mascarene.homeserver.version.BuildInfo
import org.flywaydb.core.Flyway

import javax.sql.DataSource
import org.mascarene.homeserver.MascareneGuard.Stop
import org.mascarene.homeserver.internal.repository.DbContext
import org.mascarene.utils.LogUtils.time

import scala.util.{Failure, Success, Try}

object MascareneMain extends LazyLogging {
  def main(args: Array[String]): Unit = {
    logger.info(s"Mascarene (${BuildInfo.version}) is starting up ...")

    val status = for {
      config           <- time(logger, "Configuration load") { loadMascareneConfig() }
      configWithArgon2 <- time(logger, "Argon2 tuning") { Try { initArgon2Parameters(config) } }
      dbContext        <- initDbContext(configWithArgon2)
    } yield (configWithArgon2, dbContext)

    status match {
      case Success((config, dbContext)) =>
        logger.info("Mascarene init completed")
        val trySystem = startMascareneGuard(config, dbContext)
        trySystem match {
          case Success(_) => logger.info("Mascarene started successfully.")
          case Failure(f) =>
            logger.warn(s"Mascarene failed to start with failure: ${f.getMessage}")
            logger.debug("failure details: ", f)
        }

        val mainThread = Thread.currentThread()
        Runtime.getRuntime.addShutdownHook(new Thread() {
          override def run(): Unit = {
            trySystem.map(system => system ! Stop)
            mainThread.join()
          }
        })
      case Failure(f) =>
        logger.error(s"Mascarene startup failed due to error: ${f.getMessage}")
        logger.debug("error details", f)
        System.exit(-1)
    }
  }

  private def startMascareneGuard(config: Config, dbContext: DbContext): Try[ActorSystem[MascareneGuard.Message]] =
    Try {
      ActorSystem[MascareneGuard.Message](MascareneGuard(config, dbContext), s"MascareneHS", config)
    }

  def initDbContext(config: Config): Try[DbContext] = {
    for {
      dataSource <- createDataSource(config)
      _          <- migrateDB(dataSource)
    } yield (new PostgresJdbcContext(SnakeCase, dataSource))
  }

  def loadMascareneConfig(): Try[Config] =
    Try {
      ConfigFactory
        .load()
        .withValue("akka.http.server.server-header", ConfigValueFactory.fromAnyRef(s"mascarene/${BuildInfo.version}"))
        .withValue(
          "akka.http.client.user-agent-header",
          ConfigValueFactory.fromAnyRef(s"mascarene/${BuildInfo.version}")
        )
    }

  def initArgon2Parameters(config: Config): Config = {
    val argonMemCost     = config.getInt("mascarene.server.auth.hash.mem-cost")
    val argonParallelism = config.getInt("mascarene.server.auth.hash.parallelism")
    val argonTimeCost    = config.getDuration("mascarene.server.auth.hash.time-cost").toMillis
    logger.info(s"Tuning Argon2 hash parameters ...")
    val argon2 = Argon2Factory.create(
      config.getInt("mascarene.server.auth.hash.salt-length"),
      config.getInt("mascarene.server.auth.hash.hash-length")
    )
    val argonIterations = Argon2Helper.findIterations(argon2, argonTimeCost, argonMemCost, argonParallelism)
    logger.debug(
      s"Argon2 hash parameters: memCost=$argonMemCost, parallelism=$argonParallelism, timeCost=$argonTimeCost, iterations=$argonIterations"
    )
    config.withValue("mascarene.server.auth.hash.iterations", ConfigValueFactory.fromAnyRef(argonIterations))
  }

  def createDataSource(config: Config): Try[DataSource with Closeable] = {
    import com.zaxxer.hikari.HikariDataSource
    Try {
      val dbConfig = config.getConfig("mascarene.db")
      val ds       = new HikariDataSource()
      ds.setDataSourceClassName(dbConfig.getString("datasource-class-name"))
      ds.addDataSourceProperty("url", dbConfig.getString("url"))
      ds.addDataSourceProperty("user", dbConfig.getString("username"))
      ds.addDataSourceProperty("password", dbConfig.getString("password"))
      ds
    }
  }

  private def migrateDB(dataSource: DataSource): Try[Unit] = {
    time(logger, "database migration") {
      val flyway = Flyway.configure.dataSource(dataSource).load
      Try { flyway.migrate() }
    }
  }
}
