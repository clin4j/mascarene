package org.mascarene.homeserver

import akka.actor.typed.scaladsl.Behaviors
import akka.actor.typed.{Behavior, PostStop}
import akka.http.scaladsl.Http
import akka.http.scaladsl.Http.ServerBinding
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.model.StatusCodes.{NotFound, Unauthorized}
import akka.http.scaladsl.server.Directives.{extractRequest, _}
import akka.http.scaladsl.server.directives.RouteDirectives.complete
import akka.http.scaladsl.server.{
  MalformedRequestContentRejection,
  RejectionHandler,
  UnsupportedRequestContentTypeRejection
}
import akka.http.scaladsl.settings.ServerSettings
import ch.megard.akka.http.cors.scaladsl.CorsDirectives.cors
import ch.megard.akka.http.cors.scaladsl.CorsRejection
import com.google.common.util.concurrent.RateLimiter
import com.typesafe.scalalogging.LazyLogging
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport
import io.circe.generic.auto._
import org.mascarene.homeserver.internal.rooms.RoomClusterCommand
import org.mascarene.homeserver.matrix.server.ApiErrorRejection
import org.mascarene.homeserver.matrix.server.client.{
  AccountDataApiRoutes,
  ClientApiRoutes,
  KeyManagementApiRoutes,
  PresenceApiRoutes,
  PushRulesApiRoutes,
  ThirdPartyNetworksApiRoutes
}
import org.mascarene.homeserver.matrix.server.client.auth.AuthApiRoutes
import org.mascarene.homeserver.matrix.server.client.filtering.FilteringApiRoutes
import org.mascarene.homeserver.matrix.server.client.rooms.{RoomApiRoutes, RoomEventsApiRoutes, RoomMembershipApiRoutes}
import org.mascarene.homeserver.matrix.server.client.syncing.SyncingApiRoutes
import org.mascarene.homeserver.matrix.server.client.user.UserDataApiRoutes
import org.mascarene.homeserver.matrix.server.federation.{KeyQueryApiRoutes, ServerDiscoveryApiRoutes}
import org.mascarene.sdk.matrix.core.ApiErrors

import scala.util.{Failure, Success}

object MascareneServer extends FailFastCirceSupport with LazyLogging {

  sealed trait Message

  private final case class StartFailed(cause: Throwable) extends Message

  private final case class Started(binding: ServerBinding) extends Message

  case object Stop extends Message

  def apply(host: String, port: Int, roles: List[String])(implicit runtimeContext: RuntimeContext): Behavior[Message] =
    Behaviors.setup { ctx =>
      implicit val system = ctx.system

      def apiRejectionHandler: RejectionHandler =
        RejectionHandler
          .newBuilder()
          .handle {
            case rejection: UnsupportedRequestContentTypeRejection =>
              complete(
                StatusCodes.BadRequest,
                ApiErrors.Unrecognized(Some(s"Unsupported request content type: $rejection"))
              )
          }
          .handle {
            case rejection: MalformedRequestContentRejection =>
              complete(StatusCodes.BadRequest, ApiErrors.BadJson(Some(s"Bad JSON: $rejection")))
          }
          .handle {
            case rejection: ApiErrorRejection =>
              logger.warn(s"Api rejection occurred: ${rejection.apiError}")
              complete(Unauthorized, rejection.apiError)
          }
          .handleAll[CorsRejection] { rejections =>
            val causes = rejections.map(_.cause.description).mkString(", ")
            complete(StatusCodes.BadRequest, ApiErrors.ForbiddenError(Some(s"CORS: $causes")))
          }
          .handleNotFound {
            extractUnmatchedPath { unmatchedPath =>
              logger.warn(s"API call unmatched path: $unmatchedPath")
              extractRequest { request =>
                logger.debug(s"Unmatched request: $request")
                complete(
                  StatusCodes.NotFound,
                  ApiErrors.NotFound(Some(s"No handler found for api call $unmatchedPath"))
                )
              }
            }
          }
          .result()

      val clientRoutes = if (roles.contains("client")) {
        val clientCallRate    = runtimeContext.config.getLong("mascarene.server.client-api.max-call-per-second")
        val clientRateLimiter = RateLimiter.create(clientCallRate)

        val roomCluster = runtimeContext.actorRegistry("RoomCluster").narrow[RoomClusterCommand]

        val clientApiRoutes             = new ClientApiRoutes(ctx.system, clientRateLimiter)
        val authApiRoutes               = new AuthApiRoutes(ctx.system, clientRateLimiter)
        val accountDataApiRoutes        = new AccountDataApiRoutes(ctx.system)
        val pushRulesApiRoutes          = new PushRulesApiRoutes(ctx.system)
        val filteringApiRoutes          = new FilteringApiRoutes(ctx.system)
        val syncingApiRoutes            = new SyncingApiRoutes(ctx.system)
        val roomApiRoutes               = new RoomApiRoutes(ctx.system, roomCluster, clientRateLimiter)
        val roomMembershipApiRoutes     = new RoomMembershipApiRoutes(ctx.system, roomCluster, clientRateLimiter)
        val roomEventsApiRoutes         = new RoomEventsApiRoutes(ctx.system, roomCluster)
        val presenceApiRoutes           = new PresenceApiRoutes(ctx.system, clientRateLimiter)
        val userDataApiRoutes           = new UserDataApiRoutes(ctx.system, clientRateLimiter)
        val thirdPartyNetworksApiRoutes = new ThirdPartyNetworksApiRoutes(ctx.system)
        val keyManagementApiRoutes      = new KeyManagementApiRoutes(ctx.system)
        Some(cors() {
          clientApiRoutes.routes ~
            authApiRoutes.routes ~
            accountDataApiRoutes.routes ~
            pushRulesApiRoutes.routes ~
            filteringApiRoutes.routes ~
            syncingApiRoutes.routes ~
            roomApiRoutes.routes ~
            presenceApiRoutes.routes ~
            userDataApiRoutes.routes ~
            roomMembershipApiRoutes.routes ~
            roomEventsApiRoutes.routes ~
            thirdPartyNetworksApiRoutes.routes ~
            keyManagementApiRoutes.routes
        })
      } else None

      val federationRoutes = if (roles.contains("federation")) {
        val serverDiscoveryApiRoutes = new ServerDiscoveryApiRoutes(ctx.system)
        val keyQueryApiRoutes        = new KeyQueryApiRoutes(ctx.system)
        Some(serverDiscoveryApiRoutes.routes ~ keyQueryApiRoutes.routes)
      } else None

      val routes = handleRejections(apiRejectionHandler) {
        (clientRoutes, federationRoutes) match {
          case (None, None)                 => complete(NotFound)
          case (None, Some(routes))         => routes
          case (Some(routes), None)         => routes
          case (Some(route1), Some(route2)) => route1 ~ route2
        }
      }

      val serverSettings = ServerSettings(runtimeContext.config)
      val bindingFuture  = Http().newServerAt(interface = host, port = port).withSettings(serverSettings).bind(routes)
      ctx.pipeToSelf(bindingFuture) {
        case Success(binding) => Started(binding)
        case Failure(ex)      => StartFailed(ex)
      }

      def running(binding: ServerBinding): Behavior[Message] =
        Behaviors
          .receiveMessagePartial[Message] {
            case Stop =>
              ctx.log.info(
                "Stopping API server at http://{}:{}/",
                binding.localAddress.getHostString,
                binding.localAddress.getPort
              )
              Behaviors.stopped
          }
          .receiveSignal {
            case (_, PostStop) =>
              binding.unbind()
              Behaviors.same
          }

      def starting(wasStopped: Boolean): Behaviors.Receive[Message] =
        Behaviors.receiveMessage[Message] {
          case StartFailed(cause) =>
            throw new RuntimeException("API server failed to start", cause)
          case Started(binding) =>
            val roleStr = roles.mkString(",")
            ctx.log.info(
              s"API server with roles [$roleStr] ready to serve at http://{}:{}/",
              binding.localAddress.getHostString,
              binding.localAddress.getPort
            )
            if (wasStopped) ctx.self ! Stop
            running(binding)
          case Stop =>
            // we got a stop message but haven't completed starting yet,
            // we cannot stop until starting has completed
            starting(wasStopped = true)
        }

      starting(wasStopped = false)
    }
}
