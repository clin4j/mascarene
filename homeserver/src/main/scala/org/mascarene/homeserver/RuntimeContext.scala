/*
 * Mascarene
 * Copyright (C) 2020 mascarene.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.mascarene.homeserver

import java.util.UUID
import akka.actor.typed.{ActorRef, ActorSystem}
import com.github.blemale.scaffeine.{Cache, Scaffeine}
import com.typesafe.config.Config
import org.mascarene.homeserver.internal.model.{Account, AuthToken, Device, Event, StateSet, User}
import org.mascarene.homeserver.internal.repository.DbContext
import org.mascarene.homeserver.services.{
  AccountDataService,
  AccountService,
  ApiTransactionService,
  AuthService,
  EventService,
  FederationService,
  FilteringService,
  RoomService,
  StateSetService,
  UserService
}

import scala.collection.mutable
import scala.jdk.DurationConverters._

class RuntimeContext(
    val actorSystem: ActorSystem[Nothing],
    val config: Config,
    val dbContext: DbContext
) {
  val actorRegistry: mutable.Map[String, ActorRef[Any]] = mutable.Map.empty
  val usersCache: Cache[UUID, User] = Scaffeine()
    .recordStats()
    .expireAfterWrite(config.getDuration("mascarene.internal.repo.user-cache.ttl").toScala)
    .maximumSize(config.getLong("mascarene.internal.repo.user-cache.maximum-size"))
    .build[UUID, User]()

  val eventsCache: Cache[UUID, Event] = Scaffeine()
    .recordStats()
    .expireAfterWrite(config.getDuration("mascarene.internal.repo.event-cache.ttl").toScala)
    .maximumSize(config.getLong("mascarene.internal.repo.event-cache.maximum-size"))
    .build[UUID, Event]()

  val eventAuthEdgesCache: Cache[UUID, Set[Event]] = Scaffeine()
    .recordStats()
    .expireAfterWrite(config.getDuration("mascarene.internal.repo.event-cache.ttl").toScala)
    .maximumSize(config.getLong("mascarene.internal.repo.event-cache.maximum-size"))
    .build[UUID, Set[Event]]()

  val eventParentEdgesCache: Cache[UUID, Set[Event]] = Scaffeine()
    .recordStats()
    .expireAfterWrite(config.getDuration("mascarene.internal.repo.event-cache.ttl").toScala)
    .maximumSize(config.getLong("mascarene.internal.repo.event-cache.maximum-size"))
    .build[UUID, Set[Event]]()

  val eventStateSetCache: Cache[UUID, StateSet] = Scaffeine()
    .recordStats()
    .expireAfterWrite(config.getDuration("mascarene.internal.repo.stateset-cache.ttl").toScala)
    .maximumSize(config.getLong("mascarene.internal.repo.stateset-cache.maximum-size"))
    .build[UUID, StateSet]()

  val authTokensCache: Cache[UUID, (Account, User, Device, AuthToken)] = Scaffeine()
    .recordStats()
    .expireAfterWrite(config.getDuration("mascarene.internal.repo.auth-token-cache.ttl").toScala)
    .maximumSize(config.getLong("mascarene.internal.repo.auth-token-cache.maximum-size"))
    .build[UUID, (Account, User, Device, AuthToken)]()

  private implicit val runtimeContext: RuntimeContext = this

  def newUserService: UserService                     = new UserService()
  def newAuthService: AuthService                     = new AuthService
  def newAccountDataService: AccountDataService       = new AccountDataService()
  def newFilteringService: FilteringService           = new FilteringService()
  def newRoomService: RoomService                     = new RoomService
  def newEventService: EventService                   = new EventService
  def newApiTransactionService: ApiTransactionService = new ApiTransactionService
  def newStateSetService: StateSetService             = new StateSetService
  def newFederationService: FederationService         = new FederationService
  def newAccountService: AccountService               = new AccountService
}
