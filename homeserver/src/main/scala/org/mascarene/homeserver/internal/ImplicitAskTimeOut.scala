package org.mascarene.homeserver.internal

import akka.util.Timeout
import org.mascarene.homeserver.RuntimeContext

import scala.jdk.DurationConverters.JavaDurationOps

trait ImplicitAskTimeOut {
  def runtimeContext: RuntimeContext
  private val defaultAskTimeout           = runtimeContext.config.getDuration("mascarene.internal.default-ask-timeout").toScala
  protected implicit val timeout: Timeout = Timeout(defaultAskTimeout)
}
