package org.mascarene.homeserver.internal.auth

import akka.actor.typed.ActorRef
import com.github.benmanes.caffeine.cache.stats.CacheStats
import org.mascarene.matrix.client.r0.model.auth.AuthFlow

trait AuthSessionCacheCommand
final case class GetCacheStats(replyTo: ActorRef[CacheStats]) extends AuthSessionCacheCommand
final case class PutAuthSession(authFlow: AuthFlow, replyTo: ActorRef[AuthSessionStored])
    extends AuthSessionCacheCommand
final case class GetAuthSession(session: String, replyTo: ActorRef[AuthSessionResponse]) extends AuthSessionCacheCommand
final case class UpdateAuthSession(authFlow: AuthFlow)                                   extends AuthSessionCacheCommand
final case class InvalidateAuthSession(authFlow: AuthFlow)                               extends AuthSessionCacheCommand
