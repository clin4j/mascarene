package org.mascarene.homeserver.internal.auth

import org.mascarene.matrix.client.r0.model.auth.AuthFlow

sealed trait AuthSessionCacheResponse
sealed case class CacheStats(
    hitCount: Long,
    missCount: Long,
    loadSuccessCount: Long,
    loadFailureCount: Long,
    evictionCount: Long,
    evictionWeight: Long
)                                                                extends AuthSessionCacheResponse
final case class AuthSessionStored(authFlow: AuthFlow)           extends AuthSessionCacheResponse
final case class AuthSessionResponse(authFlow: Option[AuthFlow]) extends AuthSessionCacheResponse
