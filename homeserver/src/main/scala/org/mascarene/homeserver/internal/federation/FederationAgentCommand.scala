package org.mascarene.homeserver.internal.federation

import akka.actor.typed.ActorRef
import org.mascarene.homeserver.internal.federation.client.r0.api.FederationClient

sealed trait FederationAgentCommand
case class InitialState(federationClient: FederationClient, state: FederationAgentState) extends FederationAgentCommand
case class InitError(failure: Throwable)                                                 extends FederationAgentCommand
case class GetPublicRooms(
    limit: Option[Int] = None,
    since: Option[String] = None,
    includeAllNetworks: Option[Boolean],
    thirdPartyInstanceId: Option[String] = None,
    replyTo: ActorRef[PublicRoomResponse]
) extends FederationAgentCommand
case class PostPublicRooms(
    searchTerm: Option[String],
    limit: Option[Int] = None,
    since: Option[String] = None,
    includeAllNetworks: Option[Boolean],
    thirdPartyInstanceId: Option[String] = None,
    replyTo: ActorRef[PublicRoomResponse]
) extends FederationAgentCommand
