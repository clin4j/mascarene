package org.mascarene.homeserver.internal.federation

import akka.cluster.sharding.typed.scaladsl.EntityRef

sealed trait FederationClusterResponse
case class GotFederationAgent(serverRef: EntityRef[FederationAgentCommand]) extends FederationClusterResponse
