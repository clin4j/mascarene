package org.mascarene.homeserver.internal.federation

import akka.actor.typed.pubsub.Topic
import akka.actor.typed.Behavior
import akka.actor.typed.receptionist.ServiceKey
import akka.actor.typed.scaladsl.Behaviors
import com.typesafe.scalalogging.LazyLogging
import org.bouncycastle.crypto.params.Ed25519PrivateKeyParameters
import org.bouncycastle.crypto.signers.Ed25519Signer
import org.bouncycastle.util.io.pem.{PemHeader, PemReader}
import org.mascarene.homeserver.RuntimeContext
import org.mascarene.homeserver.internal.ImplicitAskTimeOut
import org.mascarene.homeserver.internal.federation.client.KeySpec
import org.mascarene.utils.Codecs

import java.io.FileReader
import java.time.Instant
import java.util.concurrent.TimeUnit
import scala.concurrent.duration.FiniteDuration
import scala.util.Try
import scala.jdk.CollectionConverters._

object KeyManager extends LazyLogging {
  val KeyManagerKey = ServiceKey[KeyManagerCommand]("KeyManager")

  def apply(implicit runtimeContext: RuntimeContext): Behavior[KeyManagerCommand] = {
    val privateKeyFile = runtimeContext.config.getString("mascarene.server.federation.server-key-file")
    val refreshInterval =
      runtimeContext.config.getDuration("mascarene.internal.federation.key-manager-refresh-interval")

    val federationService = runtimeContext.newFederationService

    //Exception may be thrown here
    val keySpec = loadKeySpec(privateKeyFile).get
    val serverKey = federationService
      .createOrRefreshKey(
        keySpec.keyId,
        keySpec.publicKey.getEncoded
      )
      .get
    federationService.refreshKey(serverKey, Instant.now().plusMillis(refreshInterval.toMillis))
    // to here. In that case the KeyManager should fail starting so that's why we let it throw

    val signer = new Ed25519Signer()
    signer.init(true, keySpec.privateKey)

    Behaviors.setup { context =>
      val keyManagerTopic = context.spawn(Topic[KeyManagerMessage]("keyManager-topic"), "KeyManagerTopic")
      keyManagerTopic ! Topic.subscribe(context.self)

      def signPayLoad(payload: Array[Byte]): Array[Byte] = {
        signer.reset()
        signer.update(payload, 0, payload.length)
        val signature: Array[Byte] = signer.generateSignature()
        signature
      }

      Behaviors.withTimers[KeyManagerCommand] { timers =>
        timers.startTimerWithFixedDelay(
          InternalRefreshTick,
          new FiniteDuration(refreshInterval.toMillis, TimeUnit.MILLISECONDS)
        )
        Behaviors.receiveMessage {
          case GetKeySpec(replyTo) =>
            replyTo ! GotKeySpec(keySpec)
            Behaviors.same
          case InternalRefreshTick =>
            federationService.refreshKey(serverKey, Instant.now().plusMillis(refreshInterval.toMillis))
            Behaviors.same
          case SignPayload(keyId, payLoad, replyTo) if keyId.equals(serverKey.keyId) =>
            //Signature asked for the keyId managed by this Manager
            //Sign it
            replyTo ! SignedPayLoad(keyId, signPayLoad(payLoad))
            Behaviors.same

          case s: SignPayload =>
            //Signature asked for a different key manager
            //publish it so the correct manager can handle it
            keyManagerTopic ! Topic.publish(s)
            Behaviors.same

        }
      }
    }
  }

  private def loadKeySpec(fileName: String): Try[KeySpec] =
    Try {
      val pemReader = new PemReader(new FileReader(fileName))
      val pemObject = pemReader.readPemObject()
      pemObject.getHeaders.asScala.map(_.asInstanceOf[PemHeader]).find(_.getName == "keyId") match {
        case None => throw new IllegalArgumentException(s"keyId header not found in $fileName PEM file")
        case Some(keyId) =>
          val privateKey = new Ed25519PrivateKeyParameters(pemObject.getContent, 0)
          val publicKey  = privateKey.generatePublicKey()
          logger.whenInfoEnabled {
            val encodedPublicKey = Codecs.toBase64(publicKey.getEncoded, unpadded = true)
            logger.info(s"Loaded keyId=${keyId.getValue} encodedPublicKey=$encodedPublicKey")
          }
          KeySpec(keyId.getValue, publicKey, privateKey)
      }
    }
}
