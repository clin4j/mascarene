package org.mascarene.homeserver.internal.federation

import akka.actor.typed.ActorRef
import akka.actor.typed.receptionist.Receptionist

sealed trait KeyManagerCommand
case class GetKeySpec(replyTo: ActorRef[GotKeySpec])                extends KeyManagerCommand
case class ListingKeyManagerResponse(listing: Receptionist.Listing) extends KeyManagerCommand
case object InternalRefreshTick                                     extends KeyManagerCommand

sealed trait KeyManagerMessage                                                                extends KeyManagerCommand
case class SignPayload(keyId: String, payload: Array[Byte], replyTo: ActorRef[SignedPayLoad]) extends KeyManagerMessage
