package org.mascarene.homeserver.internal.federation

import org.mascarene.homeserver.internal.federation.client.KeySpec

sealed trait KeyManagerResponse
case class GotKeySpec(spec: KeySpec)                            extends KeyManagerResponse
case class SignedPayLoad(keyId: String, signature: Array[Byte]) extends KeyManagerResponse
