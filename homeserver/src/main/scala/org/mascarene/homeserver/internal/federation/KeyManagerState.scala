package org.mascarene.homeserver.internal.federation

import org.mascarene.homeserver.internal.federation.client.KeySpec

case class KeyManagerState(keySpec: KeySpec)
