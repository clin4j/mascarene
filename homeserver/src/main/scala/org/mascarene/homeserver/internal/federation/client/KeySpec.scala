package org.mascarene.homeserver.internal.federation.client

import org.bouncycastle.crypto.params.{Ed25519PrivateKeyParameters, Ed25519PublicKeyParameters}

case class KeySpec(keyId: String, publicKey: Ed25519PublicKeyParameters, privateKey: Ed25519PrivateKeyParameters)
