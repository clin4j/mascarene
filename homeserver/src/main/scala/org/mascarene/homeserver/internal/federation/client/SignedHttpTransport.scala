/*
Copyright 2018 Nicolas Jouanin

This file is part of Plasma.

Plasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License 3.

Plasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>
 */
package org.mascarene.homeserver.internal.federation.client

import akka.NotUsed
import akka.actor.typed.ActorSystem
import akka.http.scaladsl.{Http, HttpsConnectionContext}
import akka.http.scaladsl.model._
import akka.http.scaladsl.model.headers.{Authorization, GenericHttpCredentials}
import akka.stream.scaladsl.{Flow, Keep, Sink, Source}
import akka.util.ByteString
import com.typesafe.scalalogging.LazyLogging
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport
import io.circe._
import io.circe.parser._
import io.circe.syntax._
import io.circe.generic.auto._
import org.mascarene.matrix.server.r0.model.RequestSignature
import org.mascarene.sdk.matrix.core.{ApiError, ApiFailure, HttpApiResponse}
import org.mascarene.homeserver.services.FederationService
import org.mascarene.utils.Codecs

import scala.concurrent.Future
import scala.util.{Failure, Success}

trait SignedHttpTransport extends Transport with LazyLogging with FailFastCirceSupport {
  implicit def actorSystem: ActorSystem[Nothing]

  private implicit val ec = actorSystem.executionContext
  private val httpClient =
    connectionContext.map(ctx => Http().superPool[HttpRequest](ctx)).getOrElse(Http().superPool[HttpRequest]())

  // Implementation must provide the localhost name
  protected def origin: String
  protected def keySpec: KeySpec
  protected def federationService: FederationService

  // connectionContext allow to setup SSL context
  // this is especially used for tests purposes where hostname verification should be disabled for localhost tests
  def connectionContext: Option[HttpsConnectionContext]

  implicit private val printer: Printer = Printer.noSpacesSortKeys.copy(dropNullValues = true)

  protected def doPut[Q, R](
      uri: Uri,
      request: Q,
      additionalHeaders: Seq[HttpHeader] = Seq.empty,
      queries: Map[String, String] = Map.empty,
      requireAuth: Boolean
  )(implicit encoder: Encoder[Q], decoder: Decoder[R]): Future[HttpApiResponse[R]] =
    Source
      .single(request)
//      .mapAsync(1)(entity => Marshal(entity).to[RequestEntity].map(_.withContentType(MediaTypes.`application/json`)))
      .map(entity => HttpEntity(entity.asJson.printWith(printer)).withContentType(MediaTypes.`application/json`))
      .map(jsonEntity =>
        HttpRequest(
          method = HttpMethods.PUT,
          uri = uri.withQuery(Uri.Query(queries)),
          headers = additionalHeaders,
          entity = jsonEntity
        )
      )
      .via(signRequest(Some(request), requireAuth))
      .via(doHttpCall)
      .toMat(Sink.head)(Keep.right)
      .run()

  protected def doPost[Q, R](
      uri: Uri,
      request: Q,
      additionalHeaders: Seq[HttpHeader] = Seq.empty,
      queries: Map[String, String] = Map.empty,
      requireAuth: Boolean
  )(implicit encoder: Encoder[Q], decoder: Decoder[R]): Future[HttpApiResponse[R]] =
    Source
      .single(request)
      //.mapAsync(1)(entity => Marshal(entity).to[RequestEntity].map(_.withContentType(MediaTypes.`application/json`)))
      .map(entity => HttpEntity(entity.asJson.printWith(printer)).withContentType(MediaTypes.`application/json`))
      .map(jsonEntity =>
        HttpRequest(
          method = HttpMethods.POST,
          uri = uri.withQuery(Uri.Query(queries)),
          headers = additionalHeaders,
          entity = jsonEntity
        )
      )
      .via(signRequest(Some(request), requireAuth))
      .via(doHttpCall)
      .toMat(Sink.head)(Keep.right)
      .run()

  /**
    * Perform an HTTP GET and returns either a server response object (Right)
    * or an error (Left) which can be a ServerError (server side) or a ClientError (client side)
    * @param uri
    * @param decoder
    * @tparam R Type of the object encoded returned json
    * @return
    */
  protected def doGet[R](
      uri: Uri,
      additionalHeaders: Seq[HttpHeader] = Seq.empty,
      queries: Map[String, String] = Map.empty,
      requireAuth: Boolean = false
  )(implicit decoder: Decoder[R]): Future[HttpApiResponse[R]] = {
    Source
      .single(
        HttpRequest(
          method = HttpMethods.GET,
          uri = uri.withQuery(Uri.Query(queries)),
          headers = additionalHeaders,
          entity = HttpEntity.empty(MediaTypes.`application/json`)
        )
      )
      .via(signRequest[Unit](None, requireAuth))
      .via(doHttpCall)
      .toMat(Sink.head)(Keep.right)
      .run()
  }

  protected def doDelete[R](
      uri: Uri,
      additionalHeaders: Seq[HttpHeader] = Seq.empty,
      queries: Map[String, String] = Map.empty,
      requireAuth: Boolean
  )(implicit decoder: Decoder[R]): Future[HttpApiResponse[R]] =
    Source
      .single(
        HttpRequest(
          method = HttpMethods.DELETE,
          uri = uri.withQuery(Uri.Query(queries)),
          headers = additionalHeaders,
          entity = HttpEntity.empty(MediaTypes.`application/json`)
        )
      )
      .via(signRequest[Unit](None, requireAuth))
      .via(doHttpCall)
      .toMat(Sink.head)(Keep.right)
      .run()

  private def doHttpCall[R](implicit decoder: Decoder[R]): Flow[HttpRequest, HttpApiResponse[R], NotUsed] = {
    Flow[HttpRequest]
      .map { request =>
        logRequest(request)
        (request, request)
      }
      .via(httpClient)
      .mapAsync(1) {
        case (Success(response), request) =>
          val decodeFuture = response.entity.dataBytes
            .runFold(ByteString(""))(_ ++ _)
            .map(body => HttpApiResponse(responseDecodeHandler(body.utf8String), response.headers))
          logger.whenDebugEnabled {
            decodeFuture
              .onComplete {
                case Success(payload) =>
                  logger.debug(
                    s"${request.method.value} ${request.uri} <- ${response.status} : $payload"
                  )
                case Failure(t) =>
                  logger.debug(
                    s"${request.method.value} ${request.uri} <- ${response.status} : ${t.getMessage}"
                  )
              }
          }
          decodeFuture
        case (Failure(t), request) =>
          logger.debug(s"${request.method.value} ${request.uri} <- ${t.getMessage}")
          Future.failed(t)
      }
  }

  private def logRequest(request: HttpRequest): Unit =
    logger.whenDebugEnabled {
      request.entity.dataBytes
        .runFold(ByteString(""))(_ ++ _)
        .map(_.utf8String)
        .onComplete {
          case Success(payload) =>
            logger.debug(s"${request.method.value} ${request.uri} ${request.headers} -> $payload")
          case _ => logger.debug(s"${request.method.value} ${request.uri}")
        }
    }

  private def responseDecodeHandler[R](entity: String)(implicit decoder: Decoder[R]): R = {
    import cats.syntax.show._
    decode[R](entity) match {
      case Left(_) =>
        decode[ApiError](entity) match {
          case Left(error) =>
            logger.debug(s"Unexpected response: $entity")
            logger.warn(s"Unexpected response (JSON decoding failed): '${error.show}'")
            throw new ApiFailure(
              "ORG.MASCARENE.DECODE_ERROR",
              "Failed to decode server response",
              Some(error.toString + entity)
            )
          case Right(apiError) =>
            throw new ApiFailure(s"${apiError.errcode}", apiError.error.getOrElse(""))
        }
      case Right(t) => t
    }
  }

  private def signRequest[Q](content: Option[Q], requireAuth: Boolean)(implicit
      encoder: Encoder[Q]
  ): Flow[HttpRequest, HttpRequest, NotUsed] = {
    Flow[HttpRequest].mapAsync(1) { request =>
      if (requireAuth) {
        val requestSignature = RequestSignature[Q](
          request.method.value,
          request.uri.toRelative.toString(),
          origin,
          request.uri.authority.toString(),
          content
        )
        federationService.signJsonWithLocalKeys(requestSignature).map { signatures =>
          val authHeaders = signatures.map {
            case (keyId, signature) =>
              val credentials = GenericHttpCredentials(
                "X-Matrix",
                Map("origin" -> origin, "key" -> keyId, "sig" -> signature)
              )
              logger.debug(s"Request authorization header credentials: $credentials")
              Authorization(credentials)
          }
          request.withHeaders(request.headers ++ authHeaders)
        }
        /*
        encodeRequestSignature[Q](signature).map { s =>
          val credentials = GenericHttpCredentials(
            "X-Matrix",
            Map("origin" -> origin, "key" -> keySpec.keyId, "sig" -> s)
          )
          logger.debug(s"Request authorization header: $credentials")
          request.withHeaders(request.headers ++ Seq(Authorization(credentials)))
        }

         */
      } else Future.successful(request)
    }
  }

  private def encodeRequestSignature[Q](request: RequestSignature[Q])(implicit
      encoder: Encoder[Q]
  ): Future[String] = {
    for {
      requestWithSignature <- federationService.signJsonWithLocalKeys(request).map { signatures =>
        val requestSignature = request
          .copy(signatures = Some(Map(origin -> signatures)))
        logger.debug(s"request signature: $requestSignature")
        requestSignature
      }
      signatureMap <- federationService.signJson(requestWithSignature, List(keySpec.keyId))
    } yield signatureMap.head._2
  }

}
