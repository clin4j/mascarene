package org.mascarene.homeserver.internal.model

case class AuthCredentials(account: Account, user: User, device: Device, authToken: AuthToken)
