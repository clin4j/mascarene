package org.mascarene.homeserver.internal.model

import java.util.UUID
import io.circe.Json

import java.time.Instant

case class DeviceKey(
    deviceId: UUID,
    accountId: UUID,
    keyJson: Json,
    createdAt: Instant,
    updatedAt: Option[Instant]
)
