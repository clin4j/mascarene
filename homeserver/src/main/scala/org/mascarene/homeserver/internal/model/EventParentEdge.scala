package org.mascarene.homeserver.internal.model

import java.util.UUID

case class EventParentEdge(eventId: UUID, parentEventId: UUID)
