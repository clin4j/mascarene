package org.mascarene.homeserver.internal.model

import java.time.Instant
import java.util.UUID

case class RoomAliasServer(
    aliasId: UUID,
    serverDomain: String,
    createdAt: Instant
)
