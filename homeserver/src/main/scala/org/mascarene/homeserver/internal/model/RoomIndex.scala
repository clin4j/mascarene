package org.mascarene.homeserver.internal.model

import java.time.Instant
import java.util.UUID

case class RoomIndex(roomId: UUID, eventType: String, indexTerm: String, createdAt: Instant, updatedAt: Option[Instant])
