/*
 * Mascarene
 * Copyright (C) 2020  mascarene.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.mascarene.homeserver.internal.model

import io.circe.generic.auto._
import io.circe.{Decoder, Encoder}

import scala.collection.immutable
import scala.util.Try

class StateSet(val stateMap: Map[String, Map[String, Event]]) {
  def updatedWith(event: Event): StateSet = {
    require(event.isStateEvent)
    updatedWith(event.eventType, event.stateKey.get, event)
  }

  def updatedWith(eventType: String, stateKey: String, event: Event) =
    new StateSet(
      stateMap + (eventType -> stateMap
        .getOrElse(eventType, Map.empty)
        .+(stateKey -> event))
    )

  def exists(eventType: String, stateKey: String): Boolean = keys.exists(_ == (eventType, stateKey))

  def getEvent(eventType: String, stateKey: String): Option[Event] = stateMap.get(eventType).flatMap(_.get(stateKey))
  def getEvent(key: (String, String)): Option[Event]               = stateMap.get(key._1).flatMap(_.get(key._2))

  def ++(another: StateSet): StateSet = {
    new StateSet((stateMap.keys ++ another.stateMap.keys).map { key =>
      key -> stateMap
        .getOrElse[Map[String, Event]](key, Map.empty[String, Event])
        .concat(
          another.stateMap
            .getOrElse[Map[String, Event]](key, Map.empty[String, Event])
        )
    }.toMap)
  }

  def map[B](f: (String, String, Event) => B): Iterable[B] =
    keys.map(key => (key, getEvent(key))).filter(_._2.isDefined).map {
      case ((eventType, stateKey), Some(event)) => f(eventType, stateKey, event)
    }

  def keys: Iterable[(String, String)] =
    stateMap.keys.flatMap(eventType => stateMap(eventType).keys.map(stateKey => (eventType, stateKey)))
  def keySet: Set[(String, String)] =
    stateMap.keys.flatMap(eventType => stateMap(eventType).keys.map(stateKey => (eventType, stateKey))).toSet
  def toIterable: immutable.Iterable[(String, String, Event)] =
    stateMap.flatMap {
      case (eventType, subMap) => subMap.map { case (stateKey, event) => (eventType, stateKey, event) }
    }
  def toSeq: Seq[(String, String, Event)]   = toIterable.toSeq
  def toSet: Set[(String, String, Event)]   = toIterable.toSet
  def toList: List[(String, String, Event)] = toIterable.toList
  def values: Iterable[Event]               = stateMap.flatMap { case (_, eventMap) => eventMap.values }
  def size: Int                             = keys.size
  def filter(pred: ((String, String), Event) => Boolean): StateSet =
    StateSet.fromIterable(toIterable.filter { case (eventType, stateKey, event) => pred((eventType, stateKey), event) })
  def isEmpty: Boolean = stateMap.isEmpty
  def diff(other: StateSet): StateSet = {
    StateSet.fromIterable(toIterable.toSet.diff(other.toIterable.toSet))
  }
}

object StateSet {
  def apply()                                          = new StateSet(Map.empty)
  def apply(stateMap: Map[String, Map[String, Event]]) = new StateSet(stateMap)
  def fromIterable(iterable: Iterable[(String, String, Event)]): StateSet =
    StateSet(iterable.groupBy(_._1).map { case (k, it) => k -> Map.from(it.map(t => t._2 -> t._3)) })

  implicit val encodeStateSet: Encoder[StateSet] =
    Encoder.encodeMap[String, Map[String, Event]].contramap(_.stateMap)
  implicit val decodeStateSet: Decoder[StateSet] =
    Decoder.decodeMap[String, Map[String, Event]].emapTry(stateMap => Try { new StateSet(stateMap) })
}
