package org.mascarene.homeserver.internal.model

import java.util.UUID

case class StateSetVersion(roomId: UUID, version: Long, stateEventId: UUID)
