package org.mascarene.homeserver.internal.model

import java.time.Instant
import java.util.UUID

case class User(
    userId: UUID,
    mxUserId: String,
    displayName: Option[String],
    avatarUrl: Option[String],
    createdAt: Instant,
    updatedAt: Option[Instant]
)
