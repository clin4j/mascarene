/*
 * Mascarene
 * Copyright (C) 2020  mascarene.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.mascarene.homeserver.internal.repository

import java.time.Instant
import java.util.UUID
import io.circe.Json
import org.mascarene.homeserver.RuntimeContext
import org.mascarene.homeserver.internal.model.{AccountData, Room, RoomAccountData}

import scala.util.Try

class AccountDataRepo(implicit val runtimeContext: RuntimeContext) extends JsonCodec with InstantCodec {
  import runtimeContext.dbContext._

  private val accountDatas      = quote(querySchema[AccountData]("account_data"))
  private val roomAaccountDatas = quote(querySchema[RoomAccountData]("room_account_data"))
  private val rooms             = quote(querySchema[Room]("Rooms"))

  def storeAccountData(accountId: UUID, eventType: String, eventContent: Json): Try[AccountData] =
    Try {
      val now    = Instant.now()
      val newRow = AccountData(accountId, eventType, eventContent, now, None)
      run {
        accountDatas
          .insert(lift(newRow))
          .onConflictUpdate(_.accountId, _.eventType)(
            (t, e) => t.eventContent -> lift(eventContent),
            (t, e) => t.updatedAt -> lift(Some(now): Option[Instant])
          )
      }
      newRow
    }

  def storeAccountData(accountId: UUID, roomId: UUID, eventType: String, eventContent: Json): Try[RoomAccountData] =
    Try {
      val now    = Instant.now()
      val newRow = RoomAccountData(accountId, roomId, eventType, eventContent, now, None)
      run {
        roomAaccountDatas
          .insert(lift(newRow))
          .onConflictUpdate(_.accountId, _.roomId, _.eventType)(
            (t, e) => t.eventContent -> lift(eventContent),
            (t, e) => t.updatedAt -> lift(Some(now): Option[Instant])
          )
      }
      newRow
    }

  def getAccountData(accountId: UUID, eventType: String): Try[Option[AccountData]] =
    Try {
      run(
        accountDatas
          .filter(_.accountId == lift(accountId))
          .filter(_.eventType == lift(eventType))
      ).headOption
    }

  def getAccountData(accountId: UUID): Try[List[AccountData]] =
    Try {
      run(
        accountDatas
          .filter(_.accountId == lift(accountId))
      )
    }

  def getRoomAccountData(accountId: UUID, roomId: UUID, eventType: String): Try[Option[RoomAccountData]] =
    Try {
      run(
        roomAaccountDatas
          .filter(_.accountId == lift(accountId))
          .filter(_.eventType == lift(eventType))
          .filter(_.roomId == lift(roomId))
      ).headOption
    }

  def getRoomAccountData(accountId: UUID, roomId: UUID): Try[List[RoomAccountData]] =
    Try {
      run(
        roomAaccountDatas
          .filter(_.accountId == lift(accountId))
          .filter(_.roomId == lift(roomId))
      )
    }

  def getRoomsAccountData(accountId: UUID): Try[Map[Room, RoomAccountData]] =
    Try {
      run(
        roomAaccountDatas
          .filter(_.accountId == lift(accountId))
          .join(rooms)
          .on((ad, r) => ad.roomId == r.roomId)
      ).map { case (ad, r) => r -> ad }.toMap
    }
}
