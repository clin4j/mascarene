package org.mascarene.homeserver.internal.repository

import java.time.Instant
import java.util.UUID
import io.circe.Json
import org.mascarene.homeserver.RuntimeContext
import org.mascarene.homeserver.internal.model.{ApiTransaction, AuthToken}

import scala.util.Try

class ApiTransactionRepo(implicit val runtimeContext: RuntimeContext) extends JsonCodec with InstantCodec {
  import runtimeContext.dbContext._

  private val apiTransactions = quote(querySchema[ApiTransaction]("api_transactions"))
  private val authTokens      = quote(querySchema[AuthToken]("auth_tokens"))

  def storeApiTransaction(path: String, txnId: String, authTokenId: UUID, content: Json): Try[ApiTransaction] =
    Try {
      val newApiTransaction = ApiTransaction(UUID.randomUUID(), path, txnId, authTokenId, content, Instant.now())
      run {
        apiTransactions.insert(lift(newApiTransaction))
      }
      newApiTransaction
    }

  def getApiTransaction(txnId: String): Try[Option[ApiTransaction]] =
    Try {
      run {
        apiTransactions.filter(_.txnId == lift(txnId))
      }.headOption
    }

  def getApiTransactionWithAuthToken(transationId: UUID): Try[Option[(ApiTransaction, AuthToken)]] =
    Try {
      run {
        apiTransactions
          .filter(_.transactionId == lift(transationId))
          .join(authTokens)
          .on(_.authTokenId == _.tokenId)
      }.headOption
    }

}
