/*
 * Mascarene
 * Copyright (C) 2020  mascarene.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.mascarene.homeserver.internal.repository

import org.mascarene.homeserver.RuntimeContext

import java.time.Instant
import java.util.UUID
import org.mascarene.homeserver.internal.model.{Account, AuthToken, Device, User}

import scala.util.Try

class AuthRepo(implicit val runtimeContext: RuntimeContext) extends InstantCodec with TextSearchOperator {
  import runtimeContext.dbContext._

  /* query schema initialization */
  private val accounts   = quote(querySchema[Account]("accounts"))
  private val users      = quote(querySchema[User]("users"))
  private val devices    = quote(querySchema[Device]("devices"))
  private val authTokens = quote(querySchema[AuthToken]("auth_tokens"))

  def clearUserCache(): Unit = runtimeContext.usersCache.invalidateAll()

  def createUser(mxUserId: String): Try[User] =
    Try {
      val newUser = User(UUID.randomUUID(), mxUserId, None, None, Instant.now(), None)
      run {
        users.insert(lift(newUser))
      }
      newUser
    }

  /**
    * Create a new user Account.
    * By default, accounts are created activated
    * @param userId
    * @param passwordHash
    * @param kind
    * @param deactivated
    * @return
    */
  def createAccount(
      userId: UUID,
      passwordHash: Option[String],
      kind: String,
      deactivated: Boolean = false
  ): Try[Account] =
    Try {
      val newAccount = Account(UUID.randomUUID(), userId, passwordHash, kind, deactivated, Instant.now(), None)
      run {
        accounts.insert(lift(newAccount))
      }
      newAccount
    }

  def createDevice(mxDeviceId: String, displayName: String, owner: Account): Try[Device] =
    Try {
      val newDevice = Device(UUID.randomUUID(), mxDeviceId, displayName, owner.accountId, None, Instant.now(), None)
      run {
        devices.insert(lift(newDevice))
      }
      newDevice
    }

  def addToken(account: Account, device: Device, encodedToken: Option[String]): Try[AuthToken] =
    Try {
      val newToken =
        AuthToken(UUID.randomUUID(), account.accountId, device.deviceId, encodedToken, None, Instant.now(), None)
      run {
        authTokens.insert(lift(newToken))
      }
      newToken
    }

  def addOrUpdateToken(account: Account, device: Device, encodedToken: Option[String]): Try[AuthToken] = {
    getToken(account, device) flatMap {
      case None => addToken(account, device, encodedToken)
      case Some(authToken) =>
        updateEncodedToken(authToken.tokenId, encodedToken).map(_ =>
          authToken.copy(encodedToken = encodedToken, updatedAt = Some(Instant.now()))
        )
    }
  }

  def updateEncodedToken(tokenId: UUID, encodedToken: Option[String]): Try[Long] =
    Try {
      val now: Option[Instant] = Some(Instant.now())
      run {
        authTokens
          .filter(_.tokenId == lift(tokenId))
          .update(_.updatedAt -> lift(now), _.encodedToken -> lift(encodedToken))
      }
    }

  def createAccountWithDevice(
      mxUserId: String,
      passwordHash: Option[String],
      kind: String,
      mxDeviceId: String,
      displayName: String
  ): Try[(User, Account, Device)] =
    transaction[Try[(User, Account, Device)]] {
      for {
        user    <- createUser(mxUserId)
        account <- createAccount(user.userId, passwordHash, kind)
        device  <- createDevice(mxDeviceId, displayName, account)
      } yield (user, account, device)
    }

  def getToken(account: Account, device: Device): Try[Option[AuthToken]] =
    Try {
      run(
        authTokens.filter(_.accountId == lift(account.accountId)).filter(_.deviceId == lift(device.deviceId))
      ).headOption
    }
  def getUserByMxId(mxid: String): Try[Option[User]] = Try { run(users.filter(_.mxUserId == lift(mxid))).headOption }

  def getAccountFromUserMxId(mxid: String): Try[Account] =
    Try {
      run(users.filter(_.mxUserId == lift(mxid)).join(accounts).on(_.userId == _.userId).map(_._2)).head
    }

  def searchAccountFromUserMxId(mxid: String): Try[Option[Account]] =
    Try {
      run(users.filter(_.mxUserId == lift(mxid)).join(accounts).on(_.userId == _.userId).map(_._2)).headOption
    }

  def getUserById(userId: UUID): Try[Option[User]] = Try { run(users.filter(_.userId == lift(userId))).headOption }

  def getUserByIdWithCache(userId: UUID): Try[Option[User]] =
    Try {
      runtimeContext.usersCache.getIfPresent(userId).orElse {
        getUserById(userId)
          .map {
            case Some(user) =>
              runtimeContext.usersCache.put(userId, user)
              Some(user)
            case _ => None
          }
          .getOrElse(None)
      }
    }

  def getDeviceByMxId(mxDeviceId: String): Try[Device] =
    Try {
      run(devices.filter(_.mxDeviceId == lift(mxDeviceId))).head
    }

  def searchDeviceByMxId(mxDeviceId: String): Try[Option[Device]] =
    Try {
      run(devices.filter(_.mxDeviceId == lift(mxDeviceId))).headOption
    }

  def getUserDevices(userId: UUID): Try[List[Device]] =
    Try {
      run {
        devices.filter(_.accountId == lift(userId))
      }
    }

  def getCredentials(tokenId: UUID): Try[Option[(Account, User, Device, AuthToken)]] =
    Try {
      run {
        for {
          authToken <- authTokens.filter(_.tokenId == lift(tokenId))
          device    <- devices.join(_.deviceId == authToken.deviceId)
          account   <- accounts.join(_.accountId == authToken.accountId)
          user      <- users.join(_.userId == account.userId)
        } yield (account, user, device, authToken)
      }.headOption
    }

  def getCredentialsWithCache(tokenId: UUID): Try[Option[(Account, User, Device, AuthToken)]] =
    Try {
      runtimeContext.authTokensCache
        .getIfPresent(tokenId)
        .orElse {
          getCredentials(tokenId).map { credentials =>
            credentials.foreach(cred => runtimeContext.authTokensCache.put(tokenId, cred))
            credentials
          }.get
        }
    }

  def deleteDevice(deviceId: UUID): Try[Unit] =
    Try {
      run(devices.filter(_.deviceId == lift(deviceId)).delete)
    }

  def searchUserDirectory(searchTerm: String): Try[List[String]] =
    Try {
      run {
        users
          .filter(u => toTsVector(u.mxUserId) @@ toTsQuery(lift(searchTerm)))
          .union(
            users
              .filter(_.displayName.exists(displayName => toTsVector(displayName) @@ toTsQuery(lift(searchTerm))))
          )
          .map(_.mxUserId)
          .distinct
      }
    }

  def updateUserDisplayName(userId: UUID, displayName: Option[String]): Try[Instant] =
    Try {
      val now: Option[Instant] = Some(Instant.now())
      run {
        users
          .filter(_.userId == lift(userId))
          .update(_.updatedAt -> lift(now), _.displayName -> lift(displayName))
      }
      now.get
    }

  def updateUserAvatarUrl(userId: UUID, avatarUrl: Option[String]): Try[Instant] =
    Try {
      val now: Option[Instant] = Some(Instant.now())
      run {
        users
          .filter(_.userId == lift(userId))
          .update(_.updatedAt -> lift(now), _.avatarUrl -> lift(avatarUrl))
      }
      now.get
    }

  def updateAccountDeactivate(accountId: UUID, deactivated: Boolean): Try[Instant] =
    Try {
      val now: Option[Instant] = Some(Instant.now())
      run {
        accounts
          .filter(_.userId == lift(accountId))
          .update(_.updatedAt -> lift(now), _.deactivated -> lift(deactivated))
      }
      now.get
    }
}
