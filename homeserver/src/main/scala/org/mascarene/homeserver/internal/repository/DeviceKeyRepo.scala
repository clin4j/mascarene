package org.mascarene.homeserver.internal.repository

import io.circe.Json
import org.mascarene.homeserver.RuntimeContext
import org.mascarene.homeserver.internal.model.{Device, DeviceKey}

import java.time.Instant
import java.util.UUID
import scala.util.Try

class DeviceKeyRepo(implicit val runtimeContext: RuntimeContext) extends JsonCodec with InstantCodec {
  import runtimeContext.dbContext._

  /* query schema initialization */
  private val deviceKeys = quote(querySchema[DeviceKey]("device_keys"))
  private val devices    = quote(querySchema[Device]("devices"))

  def storeDeviceKey(accountId: UUID, deviceId: UUID, keyJson: Json): Try[DeviceKey] =
    Try {
      val now          = Instant.now()
      val newDeviceKey = DeviceKey(deviceId, accountId, keyJson, now, None)
      run {
        deviceKeys
          .insert(lift(newDeviceKey))
          .onConflictUpdate(_.accountId, _.deviceId)(
            (t, e) => t.keyJson -> lift(keyJson),
            (t, _) => t.updatedAt -> lift(Some(now): Option[Instant])
          )
          .returning(entity => entity)
      }
    }

  def getAccountDeviceKeys(accountId: UUID): Try[List[(Device, DeviceKey)]] = {
    Try {
      run {
        devices.filter(_.accountId == lift(accountId)).join(deviceKeys).on(_.deviceId == _.deviceId)
      }
    }
  }
}
