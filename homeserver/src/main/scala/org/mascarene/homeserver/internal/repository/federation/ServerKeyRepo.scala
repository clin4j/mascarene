/*
 * Mascarene
 * Copyright (C) 2020  mascarene.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.mascarene.homeserver.internal.repository.federation

import org.mascarene.homeserver.RuntimeContext

import java.time.Instant
import java.util.UUID
import org.mascarene.homeserver.internal.model.federation.ServerKey
import org.mascarene.homeserver.internal.repository.InstantCodec

import scala.util.Try

class ServerKeyRepo(implicit val runtimeContext: RuntimeContext) extends InstantCodec {
  import runtimeContext.dbContext._

  private val serverKeys = quote(querySchema[ServerKey]("server_keys"))

  def createOrUpdateServerKey(
      serverName: String,
      keyId: String,
      publicKey: Array[Byte],
      validUntil: Instant,
      expiredAt: Option[Instant] = None,
      nextManagerRefresh: Option[Instant] = None
  ): Try[ServerKey] =
    Try {
      val now = Instant.now()
      val newRow =
        ServerKey(
          UUID.randomUUID(),
          serverName,
          keyId,
          publicKey,
          expiredAt,
          Some(validUntil),
          nextManagerRefresh,
          Instant.now(),
          None
        )
      run {
        serverKeys
          .insert(lift(newRow))
          .onConflictUpdate(_.serverName, _.keyId)(
            (t, e) => t.publicKey -> e.publicKey,
            (t, e) => t.expiredAt -> e.expiredAt,
            (t, e) => t.validUntil -> e.validUntil,
            (t, e) => t.nextManagerRefresh -> e.nextManagerRefresh,
            (t, _) => t.updatedAt -> lift(Some(now): Option[Instant])
          )
          .returning(entity => entity)
      }
    }

  def refreshKey(serverKeyId: UUID, validUntil: Instant, nextRefresh: Instant): Try[Unit] =
    Try {
      val updatedAt: Option[Instant] = Some(Instant.now())
      run {
        serverKeys
          .filter(_.serverKeyId == lift(serverKeyId))
          .update(
            _.nextManagerRefresh -> lift(Some(nextRefresh): Option[Instant]),
            _.updatedAt          -> lift(updatedAt),
            _.validUntil         -> lift(Some(validUntil): Option[Instant])
          )
      }
    }

  def expireAllServerKeys(serverName: String): Try[Unit] =
    Try {
      val updatedAt: Option[Instant] = Some(Instant.now())
      run {
        serverKeys
          .filter(_.serverName == lift(serverName))
          .update(_.expiredAt -> lift(updatedAt), _.updatedAt -> lift(updatedAt))
      }
    }

  def getKey(keyId: String): Try[Option[ServerKey]] =
    Try {
      run {
        serverKeys.filter(_.keyId == lift(keyId))
      }.headOption
    }

  def getKey(serverName: String, keyId: String): Try[Option[ServerKey]] =
    Try {
      run {
        serverKeys.filter(_.keyId == lift(keyId)).filter(_.serverName == lift(serverName))
      }.headOption
    }

  def getKeys(serverName: String): Try[List[ServerKey]] =
    Try {
      run {
        serverKeys.filter(_.serverName == lift(serverName))
      }
    }

}
