/*
 * Mascarene
 * Copyright (C) 2020  mascarene.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.mascarene.homeserver.internal.rooms

import akka.actor.typed.{ActorRef, Behavior, PostStop}
import akka.actor.typed.scaladsl.Behaviors
import akka.cluster.ddata.{GCounter, GCounterKey, SelfUniqueAddress}
import akka.cluster.ddata.typed.scaladsl.{DistributedData, Replicator}
import com.typesafe.config.Config
import io.circe.Json
import org.mascarene.homeserver.RuntimeContext
import org.mascarene.homeserver.internal.repository.{DbContext, DistributedDataRepo}

import scala.jdk.DurationConverters._
import scala.util.Success

object EventStreamSequence {
  sealed trait Response
  case class EventStreamSequenceValue(value: Long) extends Response
  sealed trait Command
  final case class NextVal(replyTo: ActorRef[EventStreamSequenceValue])                     extends Command
  private sealed trait InternalCommand                                                      extends Command
  private case class InternalSubscribeResponse(chg: Replicator.SubscribeResponse[GCounter]) extends InternalCommand
  private case class InternalUpdateResponse(rsp: Replicator.UpdateResponse[GCounter])       extends InternalCommand
  private case class InternalGetResponse(
      rsp: Replicator.GetResponse[GCounter],
      replyTo: ActorRef[EventStreamSequenceValue]
  ) extends InternalCommand

  private val key = GCounterKey.create("eventStreamSequence")

  def apply(implicit runtimeContext: RuntimeContext): Behavior[Command] =
    Behaviors.setup[Command] { context =>
      implicit val node: SelfUniqueAddress = DistributedData(context.system).selfUniqueAddress
      val defaultAskTimeout                = runtimeContext.config.getDuration("mascarene.internal.default-ask-timeout").toScala
      val distributedDataRepo              = new DistributedDataRepo

      DistributedData.withReplicatorMessageAdapter[Command, GCounter] { replicatorAdapter =>
        replicatorAdapter.subscribe(key, InternalSubscribeResponse.apply)

        def updatedSequence(initValue: Long): Behavior[Command] = {
          Behaviors
            .receiveMessage[Command] {
              case NextVal(replyTo) =>
                replicatorAdapter.askUpdate(
                  askReplyTo =>
                    Replicator
                      .Update(
                        key,
                        GCounter.empty :+ initValue,
                        Replicator.WriteMajority(defaultAskTimeout),
                        askReplyTo
                      )(
                        _ :+ 1
                      ),
                  InternalUpdateResponse.apply
                )
                replicatorAdapter.askGet(
                  askReplyTo => Replicator.Get(key, Replicator.ReadMajority(defaultAskTimeout), askReplyTo),
                  value => InternalGetResponse(value, replyTo)
                )
                Behaviors.same
              case internal: InternalCommand =>
                internal match {
                  case InternalUpdateResponse(_) =>
                    Behaviors.same // ok

                  case InternalGetResponse(rsp @ Replicator.GetSuccess(key), replyTo) =>
                    val value = rsp.get(key).value.longValue
                    replyTo ! EventStreamSequenceValue(value)
                    updatedSequence(value)
                  case InternalGetResponse(_, _) =>
                    Behaviors.unhandled // not dealing with failures
                  case InternalSubscribeResponse(chg @ Replicator.Changed(key)) =>
                    val value = chg.get(key).value.longValue
                    updatedSequence(value)

                  case InternalSubscribeResponse(Replicator.Deleted(_)) =>
                    Behaviors.unhandled // no deletes
                }
            }
            .receiveSignal {
              case (_, PostStop) =>
                distributedDataRepo.putValue("eventStreamSequence", Json.fromLong(initValue))
                Behaviors.same
            }
        }
        val initValue = distributedDataRepo.getValue("eventStreamSequence") match {
          case Success(Some(json)) => json.as[Long].toOption
          case _                   => Some(0L)
        }
        initValue.map(initialValue => updatedSequence(initialValue)).get
      }
    }
}
