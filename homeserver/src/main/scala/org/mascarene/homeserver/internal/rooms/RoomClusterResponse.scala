package org.mascarene.homeserver.internal.rooms

import akka.cluster.sharding.typed.scaladsl.EntityRef

sealed trait RoomClusterResponse
case class GotRoomAgent(serverRef: EntityRef[RoomCommand]) extends RoomClusterResponse
