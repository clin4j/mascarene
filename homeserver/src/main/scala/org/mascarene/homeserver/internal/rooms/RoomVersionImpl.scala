/*
 * Mascarene
 * Copyright (C) 2020  mascarene.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.mascarene.homeserver.internal.rooms

import org.mascarene.homeserver.RuntimeContext
import org.mascarene.homeserver.internal.model.{Event, StateSet}
import org.mascarene.homeserver.internal.repository.EventRepo

import scala.util.Try

abstract class RoomVersionImpl(implicit runtimeContext: RuntimeContext) {
  protected[this] val eventRepo = new EventRepo

  def resolve(resolvedEvent: Event): (Event, Try[StateSet])

}
