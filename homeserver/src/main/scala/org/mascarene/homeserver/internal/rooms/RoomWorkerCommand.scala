package org.mascarene.homeserver.internal.rooms

import org.mascarene.homeserver.internal.model.{Event, EventContent, Room}

sealed trait RoomWorkerCommand
case class CastStateEventEffects(room: Room, stateEvent: Event) extends RoomWorkerCommand
case class CastRoomMemberEffects(room: Room, roomMemberEvent: Event, eventContent: EventContent)
    extends RoomWorkerCommand
case class CastRoomAliasEffects(room: Room, roomAliasEvent: Event, eventContent: EventContent) extends RoomWorkerCommand
case class CastRoomNameEffects(room: Room, roomNameEvent: Event, eventContent: EventContent)   extends RoomWorkerCommand
case class CastRoomTopicEffects(room: Room, roomTopicEvent: Event, eventContent: EventContent) extends RoomWorkerCommand
case class CastRoomCanonicalAliasEffects(room: Room, roomCanonicalAliasEvent: Event, eventContent: EventContent)
    extends RoomWorkerCommand
case class UpdateEventProcessed(event: Event)                             extends RoomWorkerCommand
case class CastRoomIndexTerm(room: Room, eventType: String, term: String) extends RoomWorkerCommand
