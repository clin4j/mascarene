package org.mascarene.homeserver.matrix.server

import akka.http.scaladsl.server.Rejection
import org.mascarene.sdk.matrix.core.ApiError

final case class ApiErrorRejection(apiError: ApiError) extends Rejection
