package org.mascarene.homeserver.matrix.server.client

import akka.actor.typed.ActorSystem
import akka.http.scaladsl.server.Directives.pathPrefix
import akka.http.scaladsl.server.Route
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport
import io.circe.Printer
import io.circe.generic.auto._
import org.mascarene.homeserver.RuntimeContext
import org.mascarene.homeserver.internal.ImplicitAskTimeOut
import akka.http.scaladsl.server.Directives._
import org.mascarene.homeserver.matrix.server
import org.mascarene.homeserver.matrix.server.client.auth.AuthDirectives
import org.mascarene.matrix.client.r0.model.keymanagement.{
  DeviceKeys,
  QueryDeviceKeyRequest,
  QueryDeviceKeyResponse,
  UploadDeviceKeyResponse,
  UploadDeviceKeysRequest
}
import org.mascarene.sdk.matrix.core.ApiErrors
import org.mascarene.utils.UserIdentifierUtils

import scala.util.{Success, Try}

class KeyManagementApiRoutes(system: ActorSystem[Nothing])(implicit val runtimeContext: RuntimeContext)
    extends FailFastCirceSupport
    with AuthDirectives
    with ImplicitAskTimeOut {
  implicit val printer: Printer = Printer.noSpaces.copy(dropNullValues = true)

  private val accountService = runtimeContext.newAccountService
  protected val authService  = runtimeContext.newAuthService

  private val matrixServerName = runtimeContext.config.getString("mascarene.server.domain-name")

  lazy val routes: Route = pathPrefix("_matrix" / "client" / "r0" / "keys") {
    requireAuth { _ =>
      path("upload") {
        post {
          entity(as[UploadDeviceKeysRequest]) {
            case UploadDeviceKeysRequest(Some(device_keys)) =>
              accountService
                .addDeviceKey(device_keys.user_id, device_keys.device_id, device_keys)
                .map(_ => complete(UploadDeviceKeyResponse(Map.empty)))
                .recover(failure => reject(server.ApiErrorRejection(ApiErrors.InternalError(Some(failure.getMessage)))))
                .get
            case _ => complete(UploadDeviceKeyResponse(Map.empty))
          }
        }
      } ~
        path("query") {
          post {
            entity(as[QueryDeviceKeyRequest]) { request =>
              val responseDeviceKeys = request.device_keys
                .map {
                  //Step through request device keys
                  case (uId, deviceList) =>
                    UserIdentifierUtils.parse(uId).flatMap { userIdentifier =>
                      // Foreach user, we look for device keys locally
                      // or remotelly
                      if (matrixServerName.equals(userIdentifier.domain)) {
                        //Local user : get DeviceKeys and filter device list if needed
                        val userDeviceKeys = for {
                          deviceKeysList <- accountService.getAccountDeviceKeys(userIdentifier.toString)
                          deviceKeyMap <- Try {
                            deviceKeysList
                              .filter {
                                case (device, _) => deviceList.isEmpty || deviceList.contains(device)
                              }
                              .map {
                                case (device, deviceKey) =>
                                  device.mxDeviceId -> deviceKey.keyJson.as[DeviceKeys].toTry.get
                              }
                              .toMap
                          }
                        } yield userIdentifier.toString -> deviceKeyMap
                        userDeviceKeys.map(Map(_))
                      } else {
                        // Get key for remote user is not yet implemented
                        Success(Map(userIdentifier.toString -> Map.empty[String, DeviceKeys]))
                      }
                    }
                }
                .filter(_.isSuccess) //Keep success search
                .map(_.get)
                .toList
                .reduce(_ ++ _) // joint results in the same map
              complete(QueryDeviceKeyResponse(Map.empty, responseDeviceKeys))
            }
          }
        }
    }
  }
}
