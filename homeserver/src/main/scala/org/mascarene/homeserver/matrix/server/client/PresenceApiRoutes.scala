/*
 * Mascarene
 * Copyright (C) 2020  mascarene.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.mascarene.homeserver.matrix.server.client

import akka.actor.typed.ActorSystem
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import com.google.common.util.concurrent.RateLimiter
import com.typesafe.scalalogging.LazyLogging
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport
import io.circe.Printer
import org.mascarene.homeserver.RuntimeContext
import org.mascarene.homeserver.internal.ImplicitAskTimeOut
import org.mascarene.homeserver.matrix.server.client.auth.AuthDirectives
import org.mascarene.homeserver.services.AuthService

import scala.jdk.DurationConverters._

class PresenceApiRoutes(system: ActorSystem[Nothing], clientApiRateLimiter: RateLimiter)(implicit
    val runtimeContext: RuntimeContext
) extends FailFastCirceSupport
    with AuthDirectives
    with ImplicitAskTimeOut
    with ThrottleDirective
    with LazyLogging {
  implicit val printer: Printer = Printer.noSpaces.copy(dropNullValues = true)
  private val throttleWaitTime =
    runtimeContext.config.getDuration("mascarene.server.client-api.throttle-wait-time").toScala

  protected val authService: AuthService = runtimeContext.newAuthService

  lazy val routes: Route = pathPrefix("_matrix" / "client" / "r0" / "presence" / Segment / "status") { userMxId =>
    requireAuth { credentials =>
      get {
        //TODO: implement https://matrix.org/docs/spec/client_server/r0.6.0#get-matrix-client-r0-presence-userid-status
        complete()
      } ~
        put {
          withThrottle(clientApiRateLimiter, throttleWaitTime) {
            //TODO: implement https://matrix.org/docs/spec/client_server/r0.6.0#put-matrix-client-r0-presence-userid-status
            complete()
          }
        }
    }
  }

}
