package org.mascarene.homeserver.matrix.server.client

import java.util.concurrent.TimeUnit

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directive0
import akka.http.scaladsl.server.Directives._
import com.google.common.util.concurrent.RateLimiter
import com.typesafe.scalalogging.LazyLogging
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport
import io.circe.generic.auto._
import org.mascarene.sdk.matrix.core.ApiError

import scala.concurrent.duration.Duration

trait ThrottleDirective { this: FailFastCirceSupport with LazyLogging =>
  def withThrottle(rateLimiter: RateLimiter, throttleWaitTime: Duration): Directive0 =
    extractMatchedPath.flatMap { matchedPath =>
      if (rateLimiter.tryAcquire(1, throttleWaitTime.toSeconds, TimeUnit.SECONDS)) {
        pass
      } else {
        logger.warn(
          s"API call to $matchedPath has been rate limited after $throttleWaitTime wait time. Consider tuning rate parameters"
        )
        complete(
          StatusCodes.TooManyRequests,
          ApiError("M_LIMIT_EXCEEDED", Some(s"Too many request. retry_after_ms: ${throttleWaitTime.toMillis}"))
        )
      }
    }
}
