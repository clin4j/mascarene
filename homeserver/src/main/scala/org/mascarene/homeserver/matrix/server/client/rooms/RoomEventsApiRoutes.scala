/*
 * Mascarene
 * Copyright (C) 2020 mascarene.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.mascarene.homeserver.matrix.server.client.rooms

import akka.actor.typed.{ActorRef, ActorSystem}
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import com.typesafe.scalalogging.LazyLogging
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport
import io.circe.generic.auto._
import io.circe.{Json, Printer}
import org.mascarene.homeserver.RuntimeContext
import org.mascarene.homeserver.internal.ImplicitAskTimeOut
import org.mascarene.homeserver.internal.model.{
  AuthCredentials,
  Event,
  EventContent,
  EventTransaction,
  RoomMembership,
  StateSet,
  User
}
import org.mascarene.homeserver.internal.rooms.RoomClusterCommand
import org.mascarene.homeserver.matrix.server
import org.mascarene.homeserver.matrix.server.{ApiErrorRejection, client}
import org.mascarene.homeserver.matrix.server.client.auth.AuthDirectives
import org.mascarene.homeserver.services.AuthService
import org.mascarene.matrix.client.r0.model.events.{RoomEvent, RoomMember}
import org.mascarene.sdk.matrix.core.ApiErrors
import org.mascarene.utils.EventUtils

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.chaining._
import scala.util.{Failure, Success, Try}

class RoomEventsApiRoutes(system: ActorSystem[Nothing], val roomCluster: ActorRef[RoomClusterCommand])(implicit
    val runtimeContext: RuntimeContext
) extends FailFastCirceSupport
    with AuthDirectives
    with client.ThrottleDirective
    with ImplicitAskTimeOut
    with LazyLogging {
  implicit private val printer: Printer  = Printer.noSpaces.copy(dropNullValues = true)
  implicit val sys                       = system
  protected val authService: AuthService = runtimeContext.newAuthService
  private val roomService                = runtimeContext.newRoomService
  private val eventService               = runtimeContext.newEventService
  private val userService                = runtimeContext.newUserService
  private val apiTransactionService      = runtimeContext.newApiTransactionService
  private val stateSetService            = runtimeContext.newStateSetService

  lazy val routes: Route = pathPrefix("_matrix" / "client" / "r0" / "rooms" / Segment) { roomMxId =>
    path("event" / Segment) { eventMxId =>
      get {
        requireAuth { credentials =>
          logger.info(s"API CALL - getRoomEvent: room=$roomMxId, eventId=$eventMxId")
          getRoomEvent(roomMxId, eventMxId, credentials)
        }
      }
    } ~
      path("state" / Segment ~ Slash.? ~ Segment.?) { (eventType, stateKey) =>
        get {
          requireAuth { credentials => getRoomStateEvent(roomMxId, eventType, stateKey.getOrElse(""), credentials) }
        }
      } ~
      path("state") { get { requireAuth { credentials => getRoomState(roomMxId, credentials) } } } ~
      path("joined_members") { get { requireAuth { credentials => getJoinedMembers(roomMxId, credentials) } } }
  }

  private def getJoinedMembers(roomMxId: String, credentials: AuthCredentials): Route = {
    val membersTry = for {
      mayBeRoom <- roomService.getRoomByMxId(roomMxId)
      tryMembers <- mayBeRoom match {
        case Some(room) => roomService.getRoomMembers(room, "join")
        case _          => Success(List.empty)
      }
    } yield tryMembers
    membersTry
      .map { members =>
        if (!members.map(_._1).contains(credentials.user)) {
          logger.debug(s"requesting user is not a member of room $roomMxId")
          complete(
            StatusCodes.Unauthorized,
            ApiErrors.UnAuthorized(Some("You aren't a member of the room."))
          )
        } else {
          val joinedMembers = members.map {
            case (user, _) => user.mxUserId -> RoomMember(user.displayName, user.avatarUrl)
          }.toMap
          complete(Map("joined" -> joinedMembers))
        }
      }
      .recover { f =>
        logger.warn(s"Failed to get room joined members: ${f.getMessage}")
        logger.debug("details:", f)
        reject(ApiErrorRejection(ApiErrors.InternalError(Some(s"Failed to get room joined members: ${f.getMessage}"))))
      }
      .get
  }

  private def getRoomState(roomMxId: String, credentials: AuthCredentials): Route = {
    val stateSetTry: Try[(Option[RoomMembership], Option[StateSet])] = for {
      mayBeRoom <- roomService.getRoomByMxId(roomMxId)
      membership <- mayBeRoom match {
        case Some(room) => userService.getRoomMembership(room, credentials.user)
        case _          => Success(None)
      }
      lastState <- mayBeRoom match {
        case Some(room) => roomService.getRoomLastState(room).map(stateSet => Some(stateSet))
        case _          => Success(None)
      }
    } yield (membership, lastState)
    stateSetTry
      .map {
        case (_, None) =>
          logger.debug("Room not found of with no state.")
          complete(
            StatusCodes.NotFound,
            ApiErrors.NotFound(Some("Room not found of with no state"))
          )
        case (None, _) =>
          logger.debug("Room not found of with no state.")
          complete(
            StatusCodes.Unauthorized,
            ApiErrors.UnAuthorized(Some("You aren't a member of the room and weren't previously a member of the room."))
          )
        case (_, Some(stateSet)) =>
          onComplete(stateSetService.stateSetToApiModel(stateSet, Some(roomMxId))) {
            case Success(stateEventList) => complete(stateEventList)
          }
      }
      .recover { f =>
        logger.warn(s"Failed to get room state: ${f.getMessage}")
        logger.debug("details:", f)
        reject(server.ApiErrorRejection(ApiErrors.InternalError(Some(s"Failed to get room state: ${f.getMessage}"))))

      }
      .get
  }

  private def getRoomStateEvent(
      roomMxId: String,
      eventType: String,
      stateKey: String,
      credentials: AuthCredentials
  ): Route = {
    // Get user room membership
    val membershipTry = for {
      maybeRoom <- roomService.getRoomByMxId(roomMxId)
      membershipWithEvent <- maybeRoom match {
        case Some(room) => userService.getRoomMembershipWithMemberEvent(room, credentials.user)
        case _          => Success(None)
      }
    } yield (maybeRoom, membershipWithEvent)
    membershipTry
      .map {
        case (Some(room), Some((membership, membershipEvent))) =>
          val contentTry = for {
            stateSet <-
              if (membership.membership == "join") roomService.getRoomLastState(room)
              else roomService.getRoomStateAtEvent(room, membershipEvent)
            stateEvent <- Try { stateSet.getEvent(eventType, stateKey) }
            evContent <- stateEvent match {
              case Some(event) => eventService.getEventContent(event)
              case _           => Success(None)
            }
          } yield evContent
          contentTry.map {
            case Some(evContent) =>
              val jsonContent: Json = evContent.content.getOrElse(Json.Null)
              logger.debug(s"get state event content: $jsonContent")
              complete(jsonContent)
            case None =>
              logger.debug(s"The room has no state with the given type '$eventType'or key '$stateKey'.")
              complete(
                StatusCodes.NotFound,
                ApiErrors.NotFound(Some(s"The room has no state with the given type '$eventType'or key '$stateKey'."))
              )
          }.get
        case (Some(_), None) =>
          complete(
            StatusCodes.Unauthorized,
            ApiErrors.UnAuthorized(Some("You aren't a member of the room and weren't previously a member of the room."))
          )
        case _ => complete(StatusCodes.NotFound, ApiErrors.NotFound(Some("Room not found")))
      }
      .recover { f =>
        logger.warn(s"Failed to get room state: ${f.getMessage}")
        logger.debug("details:", f)
        reject(server.ApiErrorRejection(ApiErrors.InternalError(Some(s"Failed to get room state: ${f.getMessage}"))))

      }
      .get
  }

  private def getRoomEvent(roomMxId: String, eventMxId: String, credentials: AuthCredentials) = {
    // concurrently get event details
    val eventFuture: Future[(Option[Event], Option[EventContent], Option[User], Option[EventTransaction])] = for {
      event <- Future.fromTry(eventService.getEventsByMxId(Set(eventMxId)).map(m => m.get(eventMxId)))
      eventContent <- event match {
        case Some(event) => Future.fromTry(eventService.getEventContent(event))
        case None        => Future.successful(None)
      }
      eventSender <- event match {
        case Some(event) => Future.fromTry(eventService.getEventSender(event))
        case None        => Future.successful(None)
      }
      eventTransaction <- event match {
        case Some(event) => Future.fromTry(eventService.getEventTransaction(event))
        case None        => Future.successful(None)
      }
    } yield (event, eventContent, eventSender, eventTransaction)

    // Get user room membership
    val membershipTry = for {
      maybeRoom <- roomService.getRoomByMxId(roomMxId)
      membership <- maybeRoom match {
        case Some(room) => userService.getRoomMembership(room, credentials.user)
        case _          => Success(None)
      }
    } yield (maybeRoom, membership)
    membershipTry
      .map {
        case (Some(room), Some(roomMembership)) if roomMembership.membership.equals("join") =>
          onComplete(eventFuture) {
            case Success((Some(event), Some(eventContent), Some(sender), eventTransaction)) =>
              val txnId: Option[String] = eventTransaction.map(_.transactionId).flatMap { transactionId =>
                apiTransactionService.getApiTransactionWithAuthToken(transactionId) match {
                  case Success(Some((apiTransaction, authToken)))
                      if authToken.tokenId == credentials.authToken.tokenId =>
                    Some(apiTransaction.txnId)
                  case Success(_) => None
                  case Failure(f) =>
                    logger.warn(s"Failed to get api transaction with id $transactionId: ${f.getMessage}")
                    None
                }
              }
              val unsigned =
                event.unsigned
                  .getOrElse(Json.Null)
                  .pipe(EventUtils.addAge(event.originServerTs))
                  .pipe(EventUtils.addTxnId(txnId))

              complete(
                RoomEvent(
                  eventContent.content.getOrElse(Json.Null),
                  event.eventType,
                  event.mxEventId,
                  room.mxRoomId,
                  sender.mxUserId,
                  event.originServerTs.toEpochMilli,
                  unsigned
                )
              )
            case _ => complete(StatusCodes.NotFound, ApiErrors.NotFound(Some("Event not found")))
          }
        case _ => complete(StatusCodes.NotFound, ApiErrors.NotFound(Some("Room or event not found")))
      }
      .recover { f =>
        logger.warn(s"Failed to get event: ${f.getMessage}")
        logger.debug("details:", f)
        reject(
          server.ApiErrorRejection(ApiErrors.InternalError(Some(s"Failed to get event: ${f.getMessage}")))
        )
      }
      .get
  }
}
