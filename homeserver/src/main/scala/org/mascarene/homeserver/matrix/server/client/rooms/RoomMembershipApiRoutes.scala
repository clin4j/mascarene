/*
 * Mascarene
 * Copyright (C) 2020 mascarene.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.mascarene.homeserver.matrix.server.client.rooms

import akka.NotUsed
import akka.actor.typed.{ActorRef, ActorSystem}
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.stream.scaladsl.{Flow, Sink, Source}
import com.google.common.util.concurrent.RateLimiter
import com.typesafe.scalalogging.LazyLogging
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport
import io.circe.Printer
import io.circe.generic.auto._
import org.mascarene.homeserver.RuntimeContext
import org.mascarene.homeserver.internal.ImplicitAskTimeOut
import org.mascarene.homeserver.internal.model.{Event, Room, User}
import org.mascarene.homeserver.internal.rooms.RoomClusterCommand
import org.mascarene.homeserver.matrix.server
import org.mascarene.homeserver.matrix.server.{ApiErrorRejection, client}
import org.mascarene.homeserver.matrix.server.client.auth.AuthDirectives
import org.mascarene.matrix.client.r0.model.rooms.{PublicRoomsChunk, _}
import org.mascarene.sdk.matrix.core.ApiErrors
import org.mascarene.utils.Codecs

import java.util.UUID
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.jdk.DurationConverters._
import scala.util.{Failure, Success, Try}

class RoomMembershipApiRoutes(
    system: ActorSystem[Nothing],
    val roomCluster: ActorRef[RoomClusterCommand],
    clientApiRateLimiter: RateLimiter
)(implicit
    val runtimeContext: RuntimeContext
) extends FailFastCirceSupport
    with AuthDirectives
    with client.ThrottleDirective
    with ImplicitAskTimeOut
    with LazyLogging {
  implicit private val printer: Printer = Printer.noSpaces.copy(dropNullValues = true)
  private val throttleWaitTime =
    runtimeContext.config.getDuration("mascarene.server.client-api.throttle-wait-time").toScala
  implicit val sys              = system
  protected val authService     = runtimeContext.newAuthService
  protected val roomService     = runtimeContext.newRoomService
  protected val userService     = runtimeContext.newUserService
  protected val stateSetService = runtimeContext.newStateSetService

  lazy val routes: Route = pathPrefix("_matrix" / "client" / "r0") {
    path("joined_rooms") {
      get {
        requireAuth { credentials =>
          onComplete(userService.joinedRoom(credentials.user)) {
            case Success(joinedRooms) => complete(JoinedRoomsResponse(joinedRooms.map(_.mxRoomId).toList))
            case Failure(f) =>
              logger.warn(s"Couldn't get user profile: ${f.getMessage}", f)
              reject(
                ApiErrorRejection(
                  ApiErrors.InternalError(Some(s"Couldn't get user joined rooms: ${f.getMessage}"))
                )
              )
          }
        }
      }
    } ~
      path("rooms" / Segment / "invite") { mxRoomId =>
        post {
          withThrottle(clientApiRateLimiter, throttleWaitTime) {
            requireAuth { credentials =>
              entity(as[InviteRoomRequest]) { request =>
                roomService
                  .getRoomByMxId(mxRoomId)
                  .map(_.get)
                  .map { room =>
                    val postEventFuture = for {
                      memberUser <- Future.fromTry {
                        // Invited user may not exist locally, so create it if needed
                        userService.getOrCreateUserByMxId(request.user_id)
                      }
                      event <- postRoomMemberEvent(room, credentials.user, memberUser, "invite")
                    } yield event
                    onComplete(postEventFuture) { tryEvent =>
                      tryEvent
                        .map { event =>
                          if (!event.rejected) complete(())
                          else reject(server.ApiErrorRejection(ApiErrors.ForbiddenError(event.rejectionCause)))
                        }
                        .recover { failure =>
                          reject(server.ApiErrorRejection(ApiErrors.InternalError(Some(failure.getMessage))))
                        }
                        .get
                    }
                  }
                  .recover { failure =>
                    reject(
                      server.ApiErrorRejection(
                        ApiErrors.Unrecognized(Some(s"Invalid request: ${failure.getMessage}"))
                      )
                    )
                  }
                  .get
              }
            }
          }
        }
      } ~
      path("rooms" / Segment / "join") { mxRoomId =>
        post {
          withThrottle(clientApiRateLimiter, throttleWaitTime) {
            requireAuth { credentials =>
              entity(as[JoinRoomRequest]) { request =>
                doPostMembership(mxRoomId, credentials.user, credentials.user, "join")
              }
            }
          }
        }
      } ~
      path("rooms" / Segment / "leave") { mxRoomId =>
        post {
          withThrottle(clientApiRateLimiter, throttleWaitTime) {
            requireAuth { credentials =>
              entity(as[JoinRoomRequest]) { request =>
                doPostMembership(mxRoomId, credentials.user, credentials.user, "leave")
              }
            }
          }
        }
      } ~
      path("rooms" / Segment / "kick") { mxRoomId =>
        post {
          withThrottle(clientApiRateLimiter, throttleWaitTime) {
            requireAuth { credentials =>
              entity(as[KickUserRequest]) { request =>
                //TODO: implement https://matrix.org/docs/spec/client_server/r0.6.0#post-matrix-client-r0-rooms-roomid-kick
                complete(())
              }
            }
          }
        }
      } ~
      path("rooms" / Segment / "ban") { mxRoomId =>
        post {
          withThrottle(clientApiRateLimiter, throttleWaitTime) {
            requireAuth { credentials =>
              entity(as[BanUserRequest]) { request =>
                //TODO: implement https://matrix.org/docs/spec/client_server/r0.6.0#post-matrix-client-r0-rooms-roomid-ban
                complete(())
              }
            }
          }
        }
      } ~
      path("rooms" / Segment / "unban") { mxRoomId =>
        post {
          withThrottle(clientApiRateLimiter, throttleWaitTime) {
            requireAuth { credentials =>
              entity(as[UnBanUserRequest]) { request =>
                //TODO: implement https://matrix.org/docs/spec/client_server/r0.6.0#post-matrix-client-r0-rooms-roomid-unban
                complete(())
              }
            }
          }
        }
      } ~
      path("rooms" / Segment / "forget") { mxRoomId =>
        post {
          withThrottle(clientApiRateLimiter, throttleWaitTime) {
            requireAuth { credentials =>
              entity(as[JoinRoomRequest]) { request =>
                val membershipTry = for {
                  maybeRoom <- roomService.getRoomByMxId(mxRoomId)
                  membership <- maybeRoom match {
                    case Some(room) => userService.getRoomMembership(room, credentials.user)
                    case None       => Success(None)
                  }
                } yield (maybeRoom, membership)
                membershipTry
                  .map {
                    case (Some(_), None) =>
                      complete(
                        StatusCodes.NotFound,
                        ApiErrors.NotFound(Some(s"no membership found user in room $mxRoomId"))
                      )
                    case (None, Some(_)) =>
                      complete(
                        StatusCodes.NotFound,
                        ApiErrors.NotFound(Some(s"no room $mxRoomId"))
                      )
                    case (None, None) =>
                      complete(
                        StatusCodes.NotFound,
                        ApiErrors.NotFound(Some(s"no room $mxRoomId and no membership found"))
                      )
                    case (Some(room), Some(membership)) =>
                      if (membership.membership.equals("leave")) {
                        userService.forgetRoom(room, credentials.user)
                        complete(())
                      } else
                        complete(
                          StatusCodes.BadRequest,
                          ApiErrors.Unknown(Some(s"User ${credentials.user.mxUserId} is in room $mxRoomId"))
                        )

                  }
                  .recover { failure =>
                    reject(
                      server.ApiErrorRejection(
                        ApiErrors.Unrecognized(Some(s"Invalid request: ${failure.getMessage}"))
                      )
                    )
                  }
                  .get
              }
            }
          }
        }
      } ~
      path("join" / Segment) { roomIdOrAlias =>
        post {
          withThrottle(clientApiRateLimiter, throttleWaitTime) {
            requireAuth { credentials =>
              parameter("server_name".as[String].*) { serverNames: Iterable[String] =>
                entity(as[JoinRoomRequest]) { request =>
                  // Concurrently find room by alias or matrix ID
                  val futureById    = Future.fromTry(roomService.getRoomByMxId(roomIdOrAlias))
                  val futureByAlias = Future.fromTry(roomService.getRoomByAlias(roomIdOrAlias))
                  val postEventFuture: Future[Event] = for {
                    roomById    <- futureById
                    roomByAlias <- futureByAlias
                    room <-
                      if (roomById.isDefined) Future.successful(roomById.get)
                      else Future.successful(roomByAlias.get)
                    event <- postRoomMemberEvent(room, credentials.user, credentials.user, "join")
                  } yield event
                  onComplete(postEventFuture) { tryEvent =>
                    tryEvent
                      .map { event =>
                        if (!event.rejected) complete(())
                        else reject(server.ApiErrorRejection(ApiErrors.ForbiddenError(event.rejectionCause)))
                      }
                      .recover { failure =>
                        reject(server.ApiErrorRejection(ApiErrors.InternalError(Some(failure.getMessage))))
                      }
                      .get
                  }
                }
              }
            }
          }
        }
      } ~
      path("directory" / "list" / "room" / Segment) { roomMxId =>
        get {
          roomService.getRoomByMxId(roomMxId) match {
            case Success(Some(room)) => complete(GetRoomVisibility(room.visibility))
            case Success(None)       => reject(server.ApiErrorRejection(ApiErrors.NotFound(Some("Room not found"))))
            case Failure(f) =>
              logger.warn(s"Couldn't list room directory: ${f.getMessage}", f)
              reject(
                server.ApiErrorRejection(
                  ApiErrors.InternalError(Some(s"Couldn't list room directory: ${f.getMessage}"))
                )
              )
          }
        } ~ post {
          requireAuth { credentials =>
            entity(as[SetRoomVisibility]) { request =>
              roomService
                .getRoomByMxId(roomMxId)
                .map {
                  case None => reject(server.ApiErrorRejection(ApiErrors.NotFound(Some("Room not found"))))
                  case Some(room) =>
                    roomService.setRoomVisibility(room, credentials.user, request.visibility).map(_ => complete(())).get
                }
                .recover { failure =>
                  reject(
                    server.ApiErrorRejection(
                      ApiErrors.Unrecognized(Some(s"Invalid request: ${failure.getMessage}"))
                    )
                  )
                }
                .get
            }
          }
        }
      }
  }

  private def doPostMembership(mxRoomId: String, sender: User, membershipUser: User, membership: String) = {
    roomService
      .getRoomByMxId(mxRoomId)
      .map(_.get)
      .map { room =>
        val postEventFuture = for {
          event <- postRoomMemberEvent(room, sender, membershipUser, membership)
        } yield event
        onComplete(postEventFuture) { tryEvent =>
          tryEvent
            .map { event =>
              if (!event.rejected) complete(())
              else reject(server.ApiErrorRejection(ApiErrors.ForbiddenError(event.rejectionCause)))
            }
            .recover { failure => reject(server.ApiErrorRejection(ApiErrors.InternalError(Some(failure.getMessage)))) }
            .get
        }
      }
      .recover { failure =>
        reject(
          server.ApiErrorRejection(
            ApiErrors.Unrecognized(Some(s"Invalid request: ${failure.getMessage}"))
          )
        )
      }
      .get
  }

  private def postRoomMemberEvent(room: Room, sender: User, memberUser: User, membership: String) = {
    roomService.postRoomMemberEvent(room, sender, memberUser, membership)
  }
}
