/*
 * Mascarene
 * Copyright (C) 2020  mascarene.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.mascarene.homeserver.matrix.server.client.syncing

import akka.actor.typed.ActorSystem
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import com.typesafe.scalalogging.LazyLogging
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport
import io.circe.generic.auto._
import io.circe.{Json, Printer}
import org.mascarene.homeserver.RuntimeContext
import org.mascarene.homeserver.internal.ImplicitAskTimeOut
import org.mascarene.homeserver.internal.model.{AccountData, AuthCredentials, Event, Room, RoomAccountData, StateSet}
import org.mascarene.homeserver.matrix.server
import org.mascarene.homeserver.matrix.server.ApiErrorRejection
import org.mascarene.homeserver.matrix.server.client.auth.AuthDirectives
import org.mascarene.homeserver.services.AuthService
import org.mascarene.matrix.client.r0.model.events.{
  AccountDataEvent,
  InviteState,
  InvitedRoom,
  JoinedRoom,
  LeftRoom,
  Rooms,
  State,
  StrippedEvent,
  SyncResponse,
  Timeline,
  AccountData => ApiAccountData
}
import org.mascarene.matrix.client.r0.model.filter.FilterDefinition
import org.mascarene.sdk.matrix.core.ApiErrors
import org.mascarene.utils.EventUtils

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future}
import scala.jdk.DurationConverters._
import scala.util.chaining._
import scala.util.{Failure, Success}

class SyncingApiRoutes(system: ActorSystem[Nothing])(implicit val runtimeContext: RuntimeContext)
    extends FailFastCirceSupport
    with AuthDirectives
    with ImplicitAskTimeOut
    with LazyLogging {
  implicit val printer: Printer = Printer.noSpaces.copy(dropNullValues = true)

  protected val authService: AuthService = runtimeContext.newAuthService
  private val filteringService           = runtimeContext.newFilteringService
  private val accountService             = runtimeContext.newAccountDataService
  private val userService                = runtimeContext.newUserService
  private val roomService                = runtimeContext.newRoomService
  private val eventService               = runtimeContext.newEventService
  private val apiTransactionService      = runtimeContext.newApiTransactionService
  private val statesetService            = runtimeContext.newStateSetService

  private val syncMaxTimeout = runtimeContext.config.getDuration("mascarene.server.client-api.sync-max-timeout").toScala

  lazy val routes: Route = pathPrefix("_matrix" / "client" / "r0") {
    path("sync") {
      get {
        requireAuth { credentials =>
          withRequestTimeout(syncMaxTimeout) {
            parameters(
              "filter".as[String].?,
              "since".as[Long].?,
              "full_state".as[Boolean].?,
              "set_presence".as[String].?,
              "timeout".as[Long].?
            ) {
              case (
                    mayBeFilter: Option[String],
                    mayBeSince: Option[Long],
                    mayBeFullState: Option[Boolean],
                    mayBeSetPresence: Option[String],
                    mayBeTimeout: Option[Long]
                  ) =>
                logger.info(
                  s"API CALL - get sync with :filter=$mayBeFilter, since=$mayBeSince, full_state=$mayBeFullState, set_presence=$mayBeSetPresence, timeout=$mayBeTimeout"
                )
                val fullState     = mayBeFullState.getOrElse(false)
                val timeout: Long = mayBeTimeout.getOrElse(0L)
                val timeoutFuture = Future { Thread.sleep(timeout) }
                val since         = mayBeSince.getOrElse(Long.MinValue)

                val filterDefinition =
                  filteringService.parseOrGetFilterDefinition(mayBeFilter.getOrElse(Json.Null.noSpaces))
                val setPresence = mayBeSetPresence.getOrElse("online")
                if (filterDefinition.isFailure)
                  reject(
                    ApiErrorRejection(
                      ApiErrors.BadJson(Some("Filter definition can't be parsed from JSON or is an invalid filter ID"))
                    )
                  )
                else if (!Seq("offline", "online", "unavailable").contains(setPresence)) {
                  reject(
                    server.ApiErrorRejection(
                      ApiErrors.Unrecognized(Some(s"Unrecognized set_presence value '$setPresence'"))
                    )
                  )
                } else {
                  //All parameters checked !
                  val accountDataFuture = Future.fromTry {
                    accountService.getAccountData(credentials.account).map { list =>
                      if (list.isEmpty) None else Some(accountDataToApiModel(list))
                    }
                  }
                  val syncRoomFuture = syncRooms(credentials, filterDefinition.get, fullState, mayBeSince)

                  onComplete(Future.sequence(List(accountDataFuture, syncRoomFuture))) { _ =>
                    val accountDataEvents           = Await.result(accountDataFuture, Duration.MinusInf)
                    val (rooms, syncMaxStreamOrder) = Await.result(syncRoomFuture, Duration.MinusInf)
                    val nextBatch                   = if (syncMaxStreamOrder <= since) since else syncMaxStreamOrder
                    var response                    = SyncResponse(nextBatch.toString)

                    if (nextBatch > since) {
                      response = response.copy(account_data = accountDataEvents, rooms = Some(rooms))
                      complete(response)
                    } else {
                      complete(timeoutFuture.map(_ => response.copy(account_data = accountDataEvents)))
                    }
                  }
                }
              case _ =>
                reject(
                  server.ApiErrorRejection(
                    ApiErrors.Unrecognized(Some(s"Invalid query parameters"))
                  )
                )
            }
          }
        }
      }
    }
  }

  /**
    * Compute /sync API rooms updates
    *
    * @param credentials      current user credentials
    * @param filterDefinition filter definition used to filter results
    * @param fullState        requires full state N
    * @param mayBeSince       state update beginning (as stream order position)
    * @return
    */
  private def syncRooms(
      credentials: AuthCredentials,
      filterDefinition: FilterDefinition,
      fullState: Boolean,
      mayBeSince: Option[Long] = None
  ): Future[(Rooms, Long)] = {
    val futureResults = for {
      roomMemberships <- userService.getRoomMemberships(credentials.user)
      roomLastEvents  <- getRoomsLastEvent(roomMemberships.map(_._1).toList)
      roomStateSets   <- getRoomsStateSetAtEvent(roomLastEvents)
      nextBatch <- Future.successful(
        roomLastEvents.values.filter(_.isDefined).map(_.get.streamOrder).maxOption.getOrElse(Long.MinValue)
      )
      diffs <- diffRoomStateSets(roomMemberships.map(_._1).toList, mayBeSince.getOrElse(Long.MinValue), nextBatch)
    } yield (roomMemberships, roomStateSets, nextBatch, diffs)

    futureResults.flatMap {
      case (roomMemberships, roomStateSets, nextBatch, diffs) =>
        val since       = mayBeSince.getOrElse(Long.MinValue)
        val joinRooms   = roomMemberships.filter(_._2 == "join").map(_._1)
        val inviteRooms = roomMemberships.filter(_._2 == "invite").map(_._1)
        val leftRooms   = roomMemberships.filter(_._2 == "leave").map(_._1)

        // Fill JoinedRoom
        val joinedRoomsIterableFutures: Iterable[Future[(String, JoinedRoom)]] = joinRooms.map { room =>
          val roomSummaryFuture =
            roomService.getRoomSummary(room, nextBatch, Some(credentials.user.mxUserId))
          val modelApiStateSetFuture = stateSetToApiModel {
            if (fullState)
              roomStateSets.getOrElse(room, StateSet())
            else
              diffs.getOrElse(room, StateSet())
          }
          val eventsTimelineFuture   = eventsTimeline(room, since, nextBatch, credentials)
          val roomsAccountDataFuture = Future.fromTry(accountService.getRoomAccountData(credentials.account, room))
          //ephemeral and unread_notifications are note yet implemented

          val joinRoomFuture = for {
            roomSummary      <- roomSummaryFuture
            modelApiStateSet <- modelApiStateSetFuture
            eventsTimeline   <- eventsTimelineFuture
            roomAccountData  <- roomsAccountDataFuture
          } yield (roomSummary, modelApiStateSet, eventsTimeline, roomAccountData)

          joinRoomFuture.map {
            case (roomSummary, modelApiStateSet, eventsTimeline, roomAccountData) =>
              val accountData = if (roomAccountData.isEmpty) None else Some(roomAccountDataToApiModel(roomAccountData))
              room.mxRoomId -> JoinedRoom(
                Some(roomSummary),
                Some(modelApiStateSet),
                Some(eventsTimeline),
                None,
                accountData,
                None
              )
          }
        }
        val joinedRoomsFutures =
          Future.foldLeft(joinedRoomsIterableFutures.toList)(Map.empty[String, JoinedRoom])((acc, elem) =>
            acc.updated(elem._1, elem._2)
          )

        //Fill InvitedRoom
        val inviteRoomIterableFutures = inviteRooms.map { room =>
          val memberShipEvent = roomMemberships.find(_._1 == room).map(_._3).get
          for {
            stateSet    <- Future.fromTry(roomService.getRoomStateAtEvent(room, memberShipEvent))
            inviteState <- stateSetToInvitedRoomApiModel(stateSet)
          } yield room.mxRoomId -> InvitedRoom(inviteState)
        }
        val invitedRoomsFutures =
          Future.foldLeft(inviteRoomIterableFutures.toList)(Map.empty[String, InvitedRoom])((acc, elem) =>
            acc.updated(elem._1, elem._2)
          )

        //Fill LeftRoom
        val leftRoomIterableFutures = leftRooms.map { room =>
          val modelApiStateSetFuture = stateSetToApiModel {
            if (fullState)
              roomStateSets.getOrElse(room, StateSet())
            else
              diffs.getOrElse(room, StateSet())
          }
          val eventsTimelineFuture   = eventsTimeline(room, since, nextBatch, credentials)
          val roomsAccountDataFuture = Future.fromTry(accountService.getRoomAccountData(credentials.account, room))
          //ephemeral and unread_notifications are note yet implemented

          val leftRoomFuture = for {
            modelApiStateSet <- modelApiStateSetFuture
            eventsTimeline   <- eventsTimelineFuture
            roomAccountData  <- roomsAccountDataFuture
          } yield (modelApiStateSet, eventsTimeline, roomAccountData)

          leftRoomFuture.map {
            case (modelApiStateSet, eventsTimeline, roomAccountData) =>
              val accountData = if (roomAccountData.isEmpty) None else Some(roomAccountDataToApiModel(roomAccountData))
              room.mxRoomId -> LeftRoom(modelApiStateSet, eventsTimeline, accountData)
          }
        }
        val leftRoomsFutures =
          Future.foldLeft(leftRoomIterableFutures.toList)(Map.empty[String, LeftRoom])((acc, elem) =>
            acc.updated(elem._1, elem._2)
          )

        val roomsFuture = for {
          joinedRoom  <- joinedRoomsFutures
          invitedRoom <- invitedRoomsFutures
          leftRoom    <- leftRoomsFutures
        } yield (joinedRoom, invitedRoom, leftRoom)

        roomsFuture.map {
          case (join, invite, left) =>
            (Rooms(join = join, invite = invite, left), nextBatch)
        }
    }
  }

  private def getRoomsLastEvent(rooms: List[Room]): Future[Map[Room, Option[Event]]] =
    Future.foldLeft {
      rooms.map(room => Future.fromTry(roomService.getRoomLastEvent(room).map(lastEvent => (room, lastEvent))))
    }(
      Map.empty[Room, Option[Event]]
    )((acc, elem) => acc.updated(elem._1, elem._2))

  private def getRoomsStateSetAtEvent(roomsMap: Map[Room, Option[Event]]): Future[Map[Room, StateSet]] = {
    Future
      .sequence {
        roomsMap.map {
          case (room, lastEvent) =>
            lastEvent
              .map(e =>
                Future
                  .fromTry(roomService.getRoomStateAtEvent(room, e))
              )
              .getOrElse(Future.successful(StateSet()))
              .map(stateSet => room -> stateSet)
        }
      }
      .map(_.toMap)
  }

  private def diffRoomStateSets(rooms: List[Room], since: Long, to: Long): Future[Map[Room, StateSet]] =
    Future {
      rooms.map { room => room -> roomService.diffRoomStateSet(room, since, to).get }.toMap
    }

  private def stateSetToApiModel(stateSet: StateSet): Future[State] =
    statesetService.stateSetToApiModel(stateSet).map(State.apply)

  private def roomAccountDataToApiModel(accountDataList: List[RoomAccountData]): ApiAccountData = {
    accountDataList.map { ac => AccountDataEvent(ac.eventType, ac.eventContent) }.pipe(ApiAccountData.apply)
  }

  private def accountDataToApiModel(accountDataList: List[AccountData]): ApiAccountData = {
    accountDataList.map { ac => AccountDataEvent(ac.eventType, ac.eventContent) }.pipe(ApiAccountData.apply)
  }

  private def stateSetToInvitedRoomApiModel(stateSet: StateSet): Future[InviteState] = {
    val eventList: List[Event] = stateSet.toList.map(_._3)
    val contentsFuture         = Future.fromTry(eventService.getEventsContents(eventList))
    val sendersFuture          = Future.fromTry(eventService.getEventsSenders(eventList))
    val apiFuture = for {
      contents <- contentsFuture
      senders  <- sendersFuture
    } yield (contents, senders)

    apiFuture.map {
      case (contents, senders) =>
        eventList
          .map { event =>
            val content = contents(event)
            val sender  = senders(event)
            StrippedEvent(content.content.getOrElse(Json.Null), event.stateKey.get, event.eventType, sender.mxUserId)
          }
          .pipe(InviteState.apply)
    }
  }

  private def eventsTimeline(
      room: Room,
      minStreamOder: Long,
      maxStreamOrder: Long,
      credentials: AuthCredentials
  ): Future[Timeline] = {
    val apiFuture = for {
      events            <- Future.fromTry(roomService.getRoomEvents(room, minStreamOder, maxStreamOrder))
      contents          <- Future.fromTry(eventService.getEventsContents(events))
      senders           <- Future.fromTry(eventService.getEventsSenders(events))
      eventTransactions <- Future.fromTry(eventService.getEventsTransactions(events))
    } yield (events, contents, senders, eventTransactions)
    apiFuture.map {
      case (events, contents, senders, eventTransactions) =>
        events
          .map { event =>
            val content = contents(event)
            val sender  = senders(event)
            // Check if event is linked to a transation. If yes, get transation details with auth token
            // Check if the transaction auth token is the same as the current client auth token.
            // if Yes, the transaction id is send back into unsigned data as we know that this event
            // is coming from this client.
            val txnId: Option[String] = eventTransactions.get(event).map(_.transactionId).flatMap { transactionId =>
              apiTransactionService.getApiTransactionWithAuthToken(transactionId) match {
                case Success(Some((apiTransaction, authToken))) if authToken.tokenId == credentials.authToken.tokenId =>
                  Some(apiTransaction.txnId)
                case Success(_) => None
                case Failure(f) =>
                  logger.warn(s"Failed to get api transaction with id $transactionId: ${f.getMessage}")
                  None
              }
            }
            val unsigned =
              event.unsigned
                .getOrElse(Json.Null)
                .pipe(EventUtils.addAge(event.originServerTs))
                .pipe(EventUtils.addTxnId(txnId))
            org.mascarene.matrix.client.r0.model.events.TimeLineEvent(
              content.content.getOrElse(Json.Null),
              event.eventType,
              event.mxEventId,
              sender.mxUserId,
              event.originServerTs.toEpochMilli,
              Some(unsigned)
            )
          }
          .pipe(list => Timeline.apply(list, None, None))
    }
  }

}
