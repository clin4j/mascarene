package org.mascarene.homeserver.matrix.server.client.user

import akka.actor.typed.ActorSystem
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import com.google.common.util.concurrent.RateLimiter
import com.typesafe.scalalogging.LazyLogging
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport
import io.circe.Printer
import io.circe.generic.auto._
import org.mascarene.homeserver.RuntimeContext
import org.mascarene.homeserver.internal.ImplicitAskTimeOut
import org.mascarene.homeserver.matrix.server
import org.mascarene.homeserver.matrix.server.{ApiErrorRejection, client}
import org.mascarene.homeserver.matrix.server.client.auth.AuthDirectives
import org.mascarene.matrix.client.r0.model.profiles._
import org.mascarene.sdk.matrix.core.ApiErrors

import scala.jdk.DurationConverters._
import scala.util.{Failure, Success}

class UserDataApiRoutes(system: ActorSystem[Nothing], clientApiRateLimiter: RateLimiter)(implicit
    val runtimeContext: RuntimeContext
) extends FailFastCirceSupport
    with AuthDirectives
    with ImplicitAskTimeOut
    with client.ThrottleDirective
    with LazyLogging {
  implicit private val printer: Printer = Printer.noSpaces.copy(dropNullValues = true)
  protected val authService             = runtimeContext.newAuthService
  protected val userService             = runtimeContext.newUserService
  private val throttleWaitTime =
    runtimeContext.config.getDuration("mascarene.server.client-api.throttle-wait-time").toScala

  lazy val routes: Route = pathPrefix("_matrix" / "client" / "r0") {
    path("user_directory" / "search") {
      post {
        withThrottle(clientApiRateLimiter, throttleWaitTime) {
          requireAuth { credentials =>
            entity(as[UserDirectorySearchRequest]) { request =>
              userService.searchUserDirectory(request.search_term, request.limit) match {
                case Success((response, limited)) =>
                  val responseUsers = response
                    .filter(_ != credentials.user.mxUserId)
                    .map(mxUserId => UserDirectorySearchResponseUser(mxUserId))
                  complete(UserDirectorySearchResponse(responseUsers, limited))
                case Failure(f) =>
                  logger.warn(s"Couldn't search user directory: ${f.getMessage}", f)
                  reject(
                    ApiErrorRejection(
                      ApiErrors.InternalError(Some(s"Couldn't search user directory: ${f.getMessage}"))
                    )
                  )
              }
            }
          }
        }
      }
    } ~
      pathPrefix("profile" / Segment) { mxUserId =>
        pathEndOrSingleSlash {
          get {
            userService.getUserByMxId(mxUserId) match {
              case Success(Some(user)) if user.avatarUrl.isDefined || user.displayName.isDefined =>
                complete(GetProfileResponse(user.avatarUrl, user.displayName))
              case Success(_) => complete(StatusCodes.NotFound)
              case Failure(f) =>
                logger.warn(s"Couldn't get user profile: ${f.getMessage}", f)
                reject(
                  server.ApiErrorRejection(
                    ApiErrors.InternalError(Some(s"Couldn't get user profile: ${f.getMessage}"))
                  )
                )
            }
          }
        } ~
          requireAuth { credentials =>
            if (credentials.user.mxUserId == mxUserId) {
              path("displayname") {
                get {
                  userService.getUserByMxId(mxUserId) match {
                    case Success(Some(user)) => complete(GetDisplayNameResponse(user.displayName))
                    case Success(None)       => complete(StatusCodes.NotFound)
                    case Failure(f) =>
                      logger.warn(s"Couldn't get user display name: ${f.getMessage}", f)
                      reject(
                        server.ApiErrorRejection(
                          ApiErrors.InternalError(Some(s"Couldn't get user display name: ${f.getMessage}"))
                        )
                      )
                  }
                } ~ put {
                  withThrottle(clientApiRateLimiter, throttleWaitTime) {
                    entity(as[SetDisplayNameRequest]) { request =>
                      userService.setUserDisplayName(credentials.user, request.displayname) match {
                        case Success(_) => complete(())
                        case Failure(f) =>
                          logger.warn(s"Couldn't set user display name: ${f.getMessage}", f)
                          reject(
                            server.ApiErrorRejection(
                              ApiErrors.InternalError(Some(s"Couldn't get user display name: ${f.getMessage}"))
                            )
                          )
                      }
                    }
                  }
                }
              } ~
                path("avatar_url") {
                  get {
                    userService.getUserByMxId(mxUserId) match {
                      case Success(Some(user)) => complete(GetAvatarUrlResponse(user.avatarUrl))
                      case Success(None)       => complete(StatusCodes.NotFound)
                      case Failure(f) =>
                        logger.warn(s"Couldn't get user avatar URL: ${f.getMessage}", f)
                        reject(
                          server.ApiErrorRejection(
                            ApiErrors.InternalError(Some(s"Couldn't get user avatar URL: ${f.getMessage}"))
                          )
                        )
                    }
                  } ~ put {
                    withThrottle(clientApiRateLimiter, throttleWaitTime) {
                      entity(as[SetAvatarUrlRequest]) { request =>
                        userService.setUserAvatarUrl(credentials.user, request.avatar_url) match {
                          case Success(_) => complete(())
                          case Failure(f) =>
                            logger.warn(s"Couldn't set user avatar URL: ${f.getMessage}", f)
                            reject(
                              server.ApiErrorRejection(
                                ApiErrors.InternalError(Some(s"Couldn't get user avatar URL: ${f.getMessage}"))
                              )
                            )
                        }
                      }
                    }
                  }
                }
            } else {
              reject(
                server.ApiErrorRejection(
                  ApiErrors.ForbiddenError(Some(s"API path userId '$mxUserId' doesn't match authorized user id"))
                )
              )
            }
          }
      }
  }
}
