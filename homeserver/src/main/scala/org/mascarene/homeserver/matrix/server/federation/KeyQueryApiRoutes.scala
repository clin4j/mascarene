package org.mascarene.homeserver.matrix.server.federation

import akka.actor.typed.ActorSystem
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import com.typesafe.scalalogging.LazyLogging
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport
import io.circe.Printer
import org.mascarene.homeserver.RuntimeContext
import io.circe.syntax._
import io.circe.generic.auto._
import org.mascarene.homeserver.matrix.server
import org.mascarene.homeserver.matrix.server.ApiErrorRejection
import org.mascarene.matrix.server.r0.model.{OldVerifyKey, ServerKeysResponse, VerifyKey}
import org.mascarene.sdk.matrix.core.ApiErrors
import org.mascarene.utils.Codecs

import java.time.Instant
import scala.util.{Failure, Success}

class KeyQueryApiRoutes(system: ActorSystem[Nothing])(implicit val runtimeContext: RuntimeContext)
    extends FailFastCirceSupport
    with LazyLogging {
  implicit val printer: Printer = Printer.noSpaces.copy(dropNullValues = true)
  private val matrixServerName  = runtimeContext.config.getString("mascarene.server.domain-name")
  private val validUntilTs      = runtimeContext.config.getDuration("mascarene.server.federation.key-valid-interval")
  private val federationService = runtimeContext.newFederationService

  lazy val routes: Route = pathPrefix("_matrix" / "key" / "v2" / "server") {
    path(Segment.?) { _ =>
      get {
        logger.debug("GET /_matrix/key/v2/server")
        val now = Instant.now()
        federationService.getLocalKeys
          .map { serverKeys =>
            val validKeys = serverKeys
              .filter(key => key.expiredAt.isEmpty && key.validUntil.isDefined && key.validUntil.get.isAfter(now))
            val verifyKeys = validKeys
              .map(key => key.keyId -> VerifyKey(Codecs.toBase64(key.publicKey, unpadded = true)))
              .toMap
            val validUntil: Option[Instant] = validKeys.map(_.validUntil).min
            val oldKeys = serverKeys
              .filter(key => key.expiredAt.isDefined)
              .map(key => key.keyId -> OldVerifyKey(key.expiredAt.get.toEpochMilli, key.keyId))
              .toMap

            val response = ServerKeysResponse(
              matrixServerName,
              verifyKeys,
              oldKeys,
              None,
              validUntil.map(_.toEpochMilli)
            )

            onComplete(federationService.signJson(response.asJson, validKeys.map(_.keyId))) {
              case Success(signatures) =>
                val signedResponse =
                  response.copy(signatures = Some(Map(matrixServerName -> signatures)))
                complete(signedResponse)
              case Failure(f) => reject(server.ApiErrorRejection(ApiErrors.InternalError(Some(f.getMessage))))
            }
          }
          .recover(failure => reject(ApiErrorRejection(ApiErrors.InternalError(Some(failure.getMessage)))))
          .get
      }
    }
  }
}
