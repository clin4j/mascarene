package org.mascarene.homeserver.services

import com.typesafe.scalalogging.LazyLogging
import org.mascarene.homeserver.RuntimeContext
import org.mascarene.homeserver.internal.ImplicitAskTimeOut
import org.mascarene.homeserver.internal.model.{Account, Device, DeviceKey}
import org.mascarene.homeserver.internal.repository.{AuthRepo, DeviceKeyRepo}
import org.mascarene.matrix.client.r0.model.keymanagement.DeviceKeys
import io.circe._
import io.circe.parser._
import io.circe.syntax._
import io.circe.generic.auto._

import java.time.Instant
import scala.util.Try

class AccountService(implicit val runtimeContext: RuntimeContext) extends LazyLogging with ImplicitAskTimeOut {

  implicit val sys = runtimeContext.actorSystem

  private val authRepo      = new AuthRepo
  private val deviceKeyRepo = new DeviceKeyRepo()

  def deactivateAccount(account: Account): Try[Instant] =
    authRepo.updateAccountDeactivate(account.accountId, deactivated = true)
  def activateAccount(account: Account): Try[Instant] =
    authRepo.updateAccountDeactivate(account.accountId, deactivated = false)

  def getAccountByMxId(mxId: String): Try[Account] = authRepo.getAccountFromUserMxId(mxId)
  def getDevice(mxDeviceId: String): Try[Device]   = authRepo.getDeviceByMxId(mxDeviceId)

  def addDeviceKey(accountMxId: String, mxDeviceId: String, deviceKey: DeviceKeys): Try[DeviceKey] = {
    for {
      account   <- getAccountByMxId(accountMxId)
      device    <- getDevice(mxDeviceId)
      deviceKey <- deviceKeyRepo.storeDeviceKey(account.accountId, device.deviceId, deviceKey.asJson)
    } yield deviceKey
  }

  def getAccountDeviceKeys(accountMxId: String): Try[List[(Device, DeviceKey)]] = {
    for {
      account         <- getAccountByMxId(accountMxId)
      deviceTupleList <- deviceKeyRepo.getAccountDeviceKeys(account.accountId)
    } yield deviceTupleList
  }

}
