/*
 * Mascarene
 * Copyright (C) 2020  mascarene.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.mascarene.homeserver.services

import java.time.Instant
import java.util.UUID
import akka.actor.typed.scaladsl.AskPattern._
import com.typesafe.scalalogging.LazyLogging
import de.mkammerer.argon2.Argon2Factory
import org.mascarene.homeserver.RuntimeContext
import org.mascarene.homeserver.internal.ImplicitAskTimeOut
import org.mascarene.homeserver.internal.auth.{
  AuthSessionCacheCommand,
  AuthSessionResponse,
  AuthSessionStored,
  GetAuthSession,
  InvalidateAuthSession,
  PutAuthSession,
  UpdateAuthSession
}
import org.mascarene.homeserver.internal.model.{Account, AuthCredentials, AuthToken, Device, User}
import org.mascarene.homeserver.internal.repository.AuthRepo
import org.mascarene.homeserver.version.BuildInfo
import org.mascarene.matrix.client.r0.model.auth.{AuthFlow, HasAuthData, RegisterRequest, RegisterResponse}
import org.mascarene.sdk.matrix.core.ApiFailure
import org.mascarene.utils.{Codecs, UserIdentifier, UserIdentifierUtils}
import org.uaparser.scala.Client
import pdi.jwt.{JwtAlgorithm, JwtCirce, JwtClaim}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.{Failure, Success, Try}

class AuthService(implicit val runtimeContext: RuntimeContext) extends LazyLogging with ImplicitAskTimeOut {

  implicit val sys = runtimeContext.actorSystem

  private val argon2 = Argon2Factory.create(
    runtimeContext.config.getInt("mascarene.server.auth.hash.salt-length"),
    runtimeContext.config.getInt("mascarene.server.auth.hash.hash-length")
  )
  private val argonMemCost     = runtimeContext.config.getInt("mascarene.server.auth.hash.mem-cost")
  private val argonParallelism = runtimeContext.config.getInt("mascarene.server.auth.hash.parallelism")
  private val argonIterations  = runtimeContext.config.getInt("mascarene.server.auth.hash.iterations")

  private val matrixServerName = runtimeContext.config.getString("mascarene.server.domain-name")
  private val serverSecretKey  = runtimeContext.config.getString("mascarene.server.secret.key")
  private val tokenExpiration  = runtimeContext.config.getDuration("mascarene.server.auth.token.expiration")

  private val authRepo = new AuthRepo

  private val authSessionCache = runtimeContext.actorRegistry("AuthSessionCache").narrow[AuthSessionCacheCommand]

  def checkAuthFlow[T <: HasAuthData](endpoint: String, request: T): Future[Either[T, AuthFlow]] = {
    if (request.auth.isEmpty || request.auth.get.isEmpty) {
      // No auth data provided initialize interactive auth flow
      replyNewAuthFlow[T](endpoint)
    } else
      checkCompleted[T](endpoint, request)
  }

  private def replyNewAuthFlow[T <: HasAuthData](endpointConfigKey: String): Future[Either[T, AuthFlow]] = {
    import pureconfig._
    import pureconfig.generic.auto._
    Try {
      val configPath = s"mascarene.matrix.interactive-auth.$endpointConfigKey"
      ConfigSource
        .fromConfig(runtimeContext.config.getConfig(configPath))
        .loadOrThrow[AuthFlow]
    } match {
      case Success(initFlow) =>
        authSessionCache
          .ask[AuthSessionStored](PutAuthSession(initFlow, _))
          .map(response => Right(response.authFlow))
      case Failure(failure) => Future.failed(failure)
    }
  }

  private def checkCompleted[T <: HasAuthData](endpointConfigKey: String, request: T): Future[Either[T, AuthFlow]] = {
    request.auth.get.get("session") match {
      case None                             => replyNewAuthFlow(endpointConfigKey)
      case Some(session) if session.isEmpty => replyNewAuthFlow(endpointConfigKey)
      case Some(session) =>
        authSessionCache
          .ask[AuthSessionResponse](GetAuthSession(session, _))
          .flatMap(session => challenge(request, session.authFlow))
    }
  }

  /**
    * Challenge auth data sent with a request with an existing session
    */
  private def challenge[T <: HasAuthData](request: T, authSession: Option[AuthFlow]): Future[Either[T, AuthFlow]] =
    Future {
      authSession match {
        //reply error to the sender if the session couldn't be retrieved
        case None                  => throw new IllegalArgumentException(s"Session unknown or expired")
        case Some(authSessionFlow) =>
          //We have a auth session auth flow from the session cache.
          //Check that auth data type given with the request is a valid type
          validateAuthType(request.auth.get).map { authType =>
            //Given auth type is valid but marked as already completed in the session
            //reply with error
            if (authSessionFlow.completed.isDefined && authSessionFlow.completed.get.contains(authType)) {
              Right(
                authSessionFlow.copy(
                  errcode = Some("IO.MASCARENE.AUTH.ALREADY_COMPLETED"),
                  error = Some(s"'$authType' is already completed")
                )
              )
            } else {
              val stages = nextExpectedStages(authSessionFlow)
              if (!stages.filter(_.isDefined).map(_.get).contains(authType)) {
                throw new IllegalArgumentException(
                  s"Unexpected auth type '$authType'. Expecting one of ${stages.mkString(",")}"
                )
              } else {
                //authenticate depending on auth type
                //successful authentication should return Left(request:T), request must be unchanged
                //unsucceful authentication should return Right(AuthFlow) with authFlow errcode filled with appropriate
                //errcode and messages
                val authResult: Either[T, AuthFlow] = authType match {
                  case "m.login.password" =>
                    Right(authSessionFlow.copy(errcode = Some("IO.MASCARENE.NOT_IMPLEMENTED")))
                  case "m.login.dummy" => Left(request)
                  case "m.login.recaptcha" =>
                    Right(authSessionFlow.copy(errcode = Some("IO.MASCARENE.NOT_IMPLEMENTED")))
                  case "m.login.oauth2" =>
                    Right(authSessionFlow.copy(errcode = Some("IO.MASCARENE.NOT_IMPLEMENTED")))
                  case "m.login.email.identity" =>
                    Right(authSessionFlow.copy(errcode = Some("IO.MASCARENE.NOT_IMPLEMENTED")))
                  case "m.login.token" =>
                    Right(authSessionFlow.copy(errcode = Some("IO.MASCARENE.NOT_IMPLEMENTED")))
                }
                authResult match {
                  case Right(errAuthFlow) => Right(errAuthFlow)
                  case Left(_)            =>
                    // add auth type to the completed auth stages
                    val nextFlow =
                      authSessionFlow.copy(completed =
                        Some(authSessionFlow.completed.getOrElse(List.empty) ++ List(authType))
                      )
                    authSessionCache ! UpdateAuthSession(nextFlow)
                    nextExpectedStages(nextFlow).filter(_.isEmpty) match {
                      case List() =>
                        Right(nextFlow) //No empty flow among remaining => more auth steps are needed
                      case _ =>
                        authSessionCache ! InvalidateAuthSession(nextFlow) //Force session, timeout
                        Left(request)                                      //at least one flow has no more steps to complete, to auth is completed
                    }
                }

              }
            }
          }.get
      }
    }

  /**
    * Check that authData map contains a `type` key and that its value is among know auth types.
    * @param authData
    * @return
    */
  private def validateAuthType(authData: Map[String, String]): Try[String] = {
    val auth_types = List(
      "m.login.password",
      "m.login.recaptcha",
      "m.login.oauth2",
      "m.login.email.identity",
      "m.login.token",
      "m.login.dummy"
    )
    authData.get("type") match {
      case None                              => Failure(new IllegalArgumentException("Auth type missing"))
      case Some(session) if session.isEmpty  => Failure(new IllegalArgumentException("Auth type empty"))
      case Some(t) if auth_types.contains(t) => Success(t)
      case Some(t)                           => Failure(new IllegalArgumentException(s"Unexpected auth type '$t'"))
    }
  }

  /**
    * Return a list of expected flow stages according to available flows and already completed stages
    * @param authSessionFlow
    * @return
    */
  private def nextExpectedStages(authSessionFlow: AuthFlow) = {
    val completedFlow = authSessionFlow.completed.getOrElse(List.empty)
    authSessionFlow.flows
      .map { flow => flow.stages.diff(completedFlow).headOption }
  }

  private def buildDisplayName(userAgent: Client) =
    s"${userAgent.userAgent.family} on ${userAgent.os.family}/${userAgent.device.family}"

  def registerUser(kind: Option[String], request: RegisterRequest, userAgent: Client): Try[RegisterResponse] = {
    val actions = for {
      userIdentifier <- validateUserName(request.username)
      password       <- Success(request.password.map(hash_password))
      (user, account, device) <- authRepo.createAccountWithDevice(
        userIdentifier.toString,
        password,
        kind.getOrElse("user"),
        request.device_id.getOrElse(Codecs.genDeviceId()),
        request.initial_device_display_name.getOrElse(buildDisplayName(userAgent))
      )
      authToken    <- authRepo.addToken(account, device, None)
      encodedToken <- Try { encodeAuthToken(authToken) }
      _updated     <- authRepo.updateEncodedToken(authToken.tokenId, Some(encodedToken))
    } yield (user, device, authToken.copy(encodedToken = Some(encodedToken)))
    actions match {
      case Success((user, device, authToken)) =>
        if (request.inhibit_login.isDefined && request.inhibit_login.get)
          Success(RegisterResponse(user.mxUserId, None, device.mxDeviceId))
        else
          Success(RegisterResponse(user.mxUserId, authToken.encodedToken, device.mxDeviceId))
      case Failure(f) => Failure(f)
    }
  }

  private def validateUserName(requestedUserName: Option[String]): Try[UserIdentifier] = {
    val userIdentifier = requestedUserName
      .map(UserIdentifierUtils.build(_, matrixServerName))                //check if the requested username format is valid
      .getOrElse(Success(UserIdentifierUtils.generate(matrixServerName))) //if no requested username, generate if
    userIdentifier match {
      case Failure(_) => Failure(new ApiFailure("M_INVALID_USERNAME", s"Invalid user name '$requestedUserName'"))
      case Success(validIdentifier) =>
        authRepo.getUserByMxId(validIdentifier.toString) match {
          case Success(None)    => Success(validIdentifier)
          case Success(Some(_)) => Failure(new ApiFailure("M_USER_IN_USE", s"'$validIdentifier' is already used"))
        }
    }
  }

  private def hash_password(password: String): String = {
    val bytes = password.getBytes
    val hash  = argon2.hash(argonIterations, argonMemCost, argonParallelism, bytes)
    argon2.wipeArray(bytes)
    hash
  }

  private def encodeAuthToken(authToken: AuthToken): String = {
    val claim = JwtClaim(
      expiration = Some(Instant.now.plusMillis(tokenExpiration.toMillis).getEpochSecond),
      issuedAt = Some(Instant.now.getEpochSecond),
      issuer = Some(s"Mascarene/${BuildInfo.version}"),
      subject = Some(authToken.tokenId.toString)
    )
    JwtCirce.encode(claim, serverSecretKey, JwtAlgorithm.HS256)
  }

  def decodeAuthToken(token: String): Try[AuthCredentials] = {
    JwtCirce.decode(token, serverSecretKey, Seq(JwtAlgorithm.HS256)).flatMap { claim =>
      if (claim.subject.isDefined) {
        val tokenId = UUID.fromString(claim.subject.get)
        authRepo.getCredentialsWithCache(tokenId)
      } else
        Failure(new IllegalArgumentException("Invalid auth token"))
    } match {
      case Success(Some((account, user, device, authToken))) =>
        Success(AuthCredentials(account, user, device, authToken))
      case Success(None)      => Failure(new ApiFailure("M_UNKNOWN_TOKEN"))
      case Failure(exception) => Failure(exception)
    }
  }

  def loginAuthenticate(
      userLocalPart: String,
      password: String,
      deviceId: Option[String],
      initialDisplayName: Option[String],
      userAgent: Client
  ): Try[(Account, User, Device, AuthToken)] = {
    authRepo
      .searchAccountFromUserMxId(UserIdentifier(userLocalPart, matrixServerName).toString)
      .flatMap {
        case None =>
          Failure(new ApiFailure("M_FORBIDDEN", s"unknown user $userLocalPart"))
        case Some(account) =>
          val bytes = password.getBytes
          if (!argon2.verify(account.passwordHash.getOrElse(""), bytes)) {
            argon2.wipeArray(bytes)
            Failure(new ApiFailure("M_FORBIDDEN", "The provided authentication data was incorrect"))
          } else {
            argon2.wipeArray(bytes)
            val deviceTry = deviceId match {
              case Some(id) => authRepo.searchDeviceByMxId(id)
              case None =>
                authRepo
                  .createDevice(
                    Codecs.genDeviceId(),
                    initialDisplayName.getOrElse(buildDisplayName(userAgent)),
                    account
                  )
                  .map(Some(_))
            }
            val actions = for {
              device       <- deviceTry
              authToken    <- authRepo.addOrUpdateToken(account, device.get, None)
              user         <- authRepo.getUserById(account.userId)
              encodedToken <- Try { encodeAuthToken(authToken) }
              _updated     <- authRepo.updateEncodedToken(authToken.tokenId, Some(encodedToken))
            } yield (device, user, authToken.copy(encodedToken = Some(encodedToken)))
            actions match {
              case Success((device, user, authToken)) => Success((account, user.get, device.get, authToken))
              case Failure(f)                         => Failure(f)
            }
          }
      }
  }

  def logout(deviceId: UUID): Try[Unit] = authRepo.deleteDevice(deviceId)
  def logoutAll(userId: UUID): Try[Unit] =
    authRepo.getUserDevices(userId).map { devices =>
      for (device <- devices) {
        authRepo.deleteDevice(device.deviceId)
      }
    }

  def isUserNameAvailable(userName: String): Try[Boolean] = authRepo.searchAccountFromUserMxId(userName).map(_.isEmpty)

  def getOrCreateUser(userMxId: String): Try[User] = {
    authRepo.getUserByMxId(userMxId).flatMap {
      case None               => authRepo.createUser(userMxId)
      case Some(existingUser) => Success(existingUser)
    }
  }
  def getUser(userId: UUID): Try[Option[User]] = authRepo.getUserById(userId)

}
