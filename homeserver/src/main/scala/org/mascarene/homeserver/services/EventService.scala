/*
 * Mascarene
 * Copyright (C) 2020  mascarene.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.mascarene.homeserver.services

import java.time.Instant
import java.util.UUID
import com.typesafe.scalalogging.LazyLogging
import io.circe.Json
import io.circe.generic.auto._
import io.circe.syntax._
import org.mascarene.homeserver.RuntimeContext
import org.mascarene.homeserver.internal.ImplicitAskTimeOut
import org.mascarene.homeserver.internal.model.{Event, EventContent, EventTransaction, Room, User}
import org.mascarene.homeserver.matrix.server.PDU
import org.mascarene.homeserver.internal.repository.EventRepo
import org.mascarene.sdk.matrix.core.ApiFailure
import org.mascarene.utils.Codecs

import scala.util.{Success, Try}

class EventService(implicit val runtimeContext: RuntimeContext) extends LazyLogging with ImplicitAskTimeOut {

  private[this] val eventRepo = new EventRepo

  private[this] val matrixServerName = runtimeContext.config.getString("mascarene.server.domain-name")

  def getEventsContents(events: List[Event]): Try[Map[Event, EventContent]] =
    eventRepo.getEventsContent(events.map(_.eventId))
  def getEventContent(event: Event): Try[Option[EventContent]] =
    getEventsContents(List(event)).map(m => m.values.headOption)

  def getEventsSenders(events: List[Event]): Try[Map[Event, User]] =
    eventRepo.getEventsSenders(events.map(_.eventId))
  def getEventSender(event: Event): Try[Option[User]] = getEventsSenders(List(event)).map(m => m.get(event))

  def getEventsTransactions(events: List[Event]): Try[Map[Event, EventTransaction]] =
    eventRepo.getEventsTransactions(events.map(_.eventId))
  def getEventTransaction(event: Event): Try[Option[EventTransaction]] =
    getEventsTransactions(List(event)).map(m => m.get(event))

  def getEventsByMxId(mxIds: Set[String]): Try[Map[String, Event]] =
    eventRepo.getEventsByMxIds(mxIds).map { events => events.map(event => event.mxEventId -> event).toMap }

  def createEvent(
      room: Room,
      eventType: String,
      stateKey: Option[String],
      sender: User,
      content: Option[Json],
      streamOrderNextVal: Long,
      parentsId: Set[UUID] = Set.empty,
      authsEventId: Set[UUID] = Set.empty
  ): Try[Event] = {
    val now = Instant.now()
    for {
      senderMxId  <- Success(sender.mxUserId)
      parentsMxId <- eventRepo.getEventsByIds(parentsId).map(events => events.map(_.mxEventId))
      authsMxId   <- eventRepo.getEventsByIds(authsEventId).map(events => events.map(_.mxEventId))
      eventMxId <- Try {
        generateMxEventId(
          room,
          now,
          eventType,
          stateKey,
          senderMxId,
          content.getOrElse(Json.Null),
          parentsMxId,
          authsMxId
        )
      }
      event <- eventRepo.insertEvent(
        roomId = room.roomId,
        senderId = sender.userId,
        eventType = eventType,
        stateKey = stateKey,
        content = content,
        parentsId = parentsId,
        authsEventId = authsEventId,
        originServerTs = Instant.now(),
        mxEventId = eventMxId,
        streamOrder = streamOrderNextVal
      )
    } yield event
  }

  private def generateMxEventId(
      room: Room,
      originServerTs: Instant,
      eventType: String,
      stateKey: Option[String],
      senderMxId: String,
      content: Json,
      prevEventsMxId: Set[String],
      authEventsMxId: Set[String],
      redacts: Option[String] = None
  ): String = {

    room.version match {
      case "1" | "2" =>
        s"$$${Codecs.genId()}:$matrixServerName"
      case "3" | "4" | "5" =>
        val eventHash = computeEventHash(
          room.mxRoomId,
          originServerTs,
          eventType,
          stateKey,
          senderMxId,
          content,
          prevEventsMxId,
          authEventsMxId,
          redacts
        )
        s"$$${Codecs.toBase64(eventHash, unpadded = true)}"
      case _ => throw new ApiFailure("M_UNSUPPORTED_ROOM_VERSION", s"Unsupported room version '${room.version}'")
    }
  }

  private def computeEventHash(
      roomMxId: String,
      originServerTs: Instant,
      eventType: String,
      stateKey: Option[String],
      senderMxId: String,
      content: Json,
      prevEventsMxId: Set[String],
      authEventsMxId: Set[String],
      redacts: Option[String] = None
  ): Array[Byte] = {
    val pdu =
      PDU(
        roomMxId,
        senderMxId,
        matrixServerName,
        originServerTs.toEpochMilli,
        eventType,
        stateKey,
        content,
        prevEventsMxId,
        0,
        authEventsMxId,
        redacts,
        None,
        Map.empty,
        Map.empty
      )
    val modifiedJson =
      pdu.asJson.hcursor.downField("unsigned").delete.downField("signatures").delete.downField("hashes").delete.top.get
    val jsonString = modifiedJson.dropNullValues.noSpacesSortKeys
    Codecs.sha256(jsonString)
  }

  def updateEventProcessed(eventId: UUID, processed: Boolean): Try[Unit] =
    eventRepo
      .updateEventProcessed(eventId, processed)
      .map { _ =>
        logger.debug(s"Event $eventId updated processed=$processed")
        ()
      }
      .recover { failure =>
        logger.warn("Failed to update event", failure)
      }

  def updateEventResolved(eventId: UUID, resolved: Boolean): Try[Unit] =
    eventRepo
      .updateEventResolved(eventId, resolved)
      .map { _ =>
        logger.debug(s"Event $eventId updated resolved=$resolved")
        ()
      }
      .recover { failure =>
        logger.warn("Failed to update event", failure)
      }
}
