package org.mascarene.homeserver.services

import akka.actor.typed.scaladsl.AskPattern._
import akka.cluster.sharding.typed.scaladsl.EntityRef
import com.typesafe.scalalogging.LazyLogging
import io.circe._
import io.circe.parser._
import io.circe.syntax._
import io.circe.generic.auto._
import org.mascarene.homeserver.RuntimeContext
import org.mascarene.homeserver.internal.ImplicitAskTimeOut
import org.mascarene.homeserver.internal.federation.{
  FederationAgentCommand,
  FederationClusterCommand,
  GetFederationAgent,
  GetPublicRooms,
  GotFederationAgent,
  KeyManagerCommand,
  KeyManagerMessage,
  PostPublicRooms,
  PublicRoomResponse,
  SignPayload,
  SignedPayLoad
}
import org.mascarene.homeserver.internal.model.federation.ServerKey
import org.mascarene.homeserver.internal.repository.federation.ServerKeyRepo
import org.mascarene.matrix.server.r0.model.ListPublicRoomsResponse
import org.mascarene.utils.Codecs

import java.time.Instant
import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.Try

class FederationService(implicit val runtimeContext: RuntimeContext) extends ImplicitAskTimeOut with LazyLogging {
  implicit val sys                      = runtimeContext.actorSystem
  implicit private val printer: Printer = Printer.noSpacesSortKeys.copy(dropNullValues = true)

  private val matrixServerName    = runtimeContext.config.getString("mascarene.server.domain-name")
  private val keyValidInterval    = runtimeContext.config.getDuration("mascarene.server.federation.key-valid-interval")
  private[this] val serverKeyRepo = new ServerKeyRepo

  private def getFederationAgent(forAuthority: String): Future[EntityRef[FederationAgentCommand]] = {
    val federationCluster = runtimeContext.actorRegistry("FederationCluster").narrow[FederationClusterCommand]
    federationCluster.ask[GotFederationAgent](GetFederationAgent(forAuthority, _)).map(_.serverRef)
  }

  def createOrRefreshKey(keyId: String, publicKey: Array[Byte], nextRefresh: Option[Instant] = None): Try[ServerKey] = {
    val validUntil = Instant.now().plusMillis(keyValidInterval.toMillis)
    serverKeyRepo.createOrUpdateServerKey(matrixServerName, keyId, publicKey, validUntil, None, nextRefresh)
  }

  def refreshKey(serverKey: ServerKey, nextRefresh: Instant): Try[Unit] = {
    val validUntil = Instant.now().plusMillis(keyValidInterval.toMillis)
    logger.debug(s"Refreshing key ${serverKey.keyId}: now valid until $validUntil")
    serverKeyRepo.refreshKey(serverKey.serverKeyId, validUntil, nextRefresh)
  }

  def getLocalKey(keyId: String): Try[Option[ServerKey]] = serverKeyRepo.getKey(matrixServerName, keyId)

  def getLocalKeys: Try[List[ServerKey]] = serverKeyRepo.getKeys(matrixServerName)

  def getLocalActiveKeys: Try[List[ServerKey]] = {
    val now = Instant.now()
    getLocalKeys.map(
      _.filter(key => key.expiredAt.isEmpty && key.validUntil.isDefined && key.validUntil.get.isAfter(now))
    )
  }

  def signJson[Q](payLoad: Q, keyIds: List[String])(implicit
      encoder: Encoder[Q]
  ): Future[Map[String, String]] = {
    val keyManager           = runtimeContext.actorRegistry("KeyManager").narrow[KeyManagerCommand]
    val stringPayLoad        = payLoad.asJson.printWith(printer)
    val payload: Array[Byte] = stringPayLoad.getBytes("utf-8")
    logger.debug(s"payload to sign: $stringPayLoad")
    val signatureFutures = keyIds.map { keyId =>
      keyManager
        .ask[SignedPayLoad](SignPayload(keyId, payload, _))
        .map(signedPayLoad => keyId -> Codecs.toBase64(signedPayLoad.signature, unpadded = true))
    }
    Future.sequence(signatureFutures).map(_.toMap)
  }

  def signJsonWithLocalKeys[Q](payLoad: Q)(implicit
      encoder: Encoder[Q]
  ): Future[Map[String, String]] = {
    for {
      keys          <- Future.fromTry(getLocalActiveKeys)
      signedPayload <- signJson(payLoad, keys.map(_.keyId))
    } yield signedPayload
  }

  def getPublicRooms(
      serverName: String,
      limit: Option[Int],
      since: Option[String],
      includeAllNetworks: Option[Boolean],
      thirdPartyInstanceId: Option[String]
  ): Future[ListPublicRoomsResponse] = {
    for {
      agent <- getFederationAgent(serverName)
      agentResponse <-
        agent.ask[PublicRoomResponse](GetPublicRooms(limit, since, includeAllNetworks, thirdPartyInstanceId, _))
      response <- Future.fromTry(agentResponse.publicRoomResponse)
    } yield response
  }

  def postPublicRooms(
      serverName: String,
      searchTerm: Option[String],
      limit: Option[Int],
      since: Option[String],
      includeAllNetworks: Option[Boolean],
      thirdPartyInstanceId: Option[String]
  ): Future[ListPublicRoomsResponse] = {
    for {
      agent <- getFederationAgent(serverName)
      agentResponse <- agent.ask[PublicRoomResponse](
        PostPublicRooms(searchTerm, limit, since, includeAllNetworks, thirdPartyInstanceId, _)
      )
      response <- Future.fromTry(agentResponse.publicRoomResponse)
    } yield response
  }

}
