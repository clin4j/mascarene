/*
 * Mascarene
 * Copyright (C) 2020  mascarene.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.mascarene.homeserver.services

import java.util.UUID
import akka.actor.typed.scaladsl.AskPattern._
import akka.cluster.sharding.typed.scaladsl.EntityRef
import com.typesafe.scalalogging.LazyLogging
import io.circe.Json
import io.circe.generic.auto._
import io.circe.syntax._
import org.mascarene.homeserver.RuntimeContext
import org.mascarene.homeserver.internal.ImplicitAskTimeOut
import org.mascarene.homeserver.internal.model.{
  Event,
  EventTypes,
  Room,
  RoomAlias,
  RoomAliasServer,
  RoomIndex,
  RoomMembership,
  StateSet,
  User
}
import org.mascarene.homeserver.internal.repository._
import org.mascarene.homeserver.internal.rooms.{
  EventProcessed,
  EventStreamSequence,
  GetRoomAgent,
  GetRoomState,
  GotRoomAgent,
  ProcessEvent,
  RoomClusterCommand,
  RoomCommand,
  RoomState,
  RoomStateResponse
}
import org.mascarene.homeserver.internal.rooms.EventStreamSequence.{EventStreamSequenceValue, NextVal}
import org.mascarene.matrix.client.r0.model.ModelEventUtils
import org.mascarene.matrix.client.r0.model.events.{
  MemberEventContent,
  RoomAliasEventContent,
  RoomNameEventContent,
  RoomSummary
}
import org.mascarene.matrix.client.r0.model.rooms.{
  CreateRoomRequest,
  CreationContent,
  ListPublicRoomsResponse,
  PowerLevelEventContent,
  PublicRoomsChunk,
  StateEvent
}
import org.mascarene.sdk.matrix.core.ApiFailure
import org.mascarene.utils.{Codecs, RoomAliasIdentifierUtils, RoomIdentifierUtils}

import scala.collection.mutable
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.{Failure, Success, Try}

case class RoomAliasResponse(room: Room, alias: RoomAlias, servers: List[RoomAliasServer])

class RoomService(implicit val runtimeContext: RuntimeContext) extends LazyLogging with ImplicitAskTimeOut {

  private val matrixServerName      = runtimeContext.config.getString("mascarene.server.domain-name")
  private val defaultRoomVersion    = runtimeContext.config.getString("mascarene.matrix.default-room-version")
  private val supportedRoomVersions = runtimeContext.config.getStringList("mascarene.matrix.supported-room-versions")

  implicit val sys            = runtimeContext.actorSystem
  private val roomRepo        = new RoomRepo
  private val stateSetService = runtimeContext.newStateSetService
  private val eventService    = runtimeContext.newEventService

  private val eventStreamSequence =
    runtimeContext.actorRegistry("EventStreamSequence").narrow[EventStreamSequence.Command]
  private val roomCluster = runtimeContext.actorRegistry("RoomCluster").narrow[RoomClusterCommand]

  def createRoom(visibility: String, version: Option[String] = Some(defaultRoomVersion)): Try[Room] = {
    for {
      validatedVersion <- version match {
        case Some(v) if supportedRoomVersions.contains(v) => Success(v)
        case None                                         => Success(defaultRoomVersion)
        case _                                            => Failure(new ApiFailure("M_UNSUPPORTED_ROOM_VERSION"))
      }
      room <- roomRepo.createRoom(RoomIdentifierUtils.generate(matrixServerName).toString, visibility, validatedVersion)
    } yield room
  }

  def getRoomByMxId(mxId: String): Try[Option[Room]] = roomRepo.getRoomByMxId(mxId)

  def getRoomByAlias(aliasName: String): Try[Option[Room]] = roomRepo.getRoomByAlias(aliasName)

  def getRoomById(roomId: UUID): Try[Option[Room]] = roomRepo.getRoomById(roomId)

  def postEvent(
      room: Room,
      sender: User,
      eventType: String,
      content: Option[Json],
      parentsId: Set[UUID] = Set.empty,
      waitProcess: Boolean = true
  ): Future[Event] = {
    for {
      roomServer <- roomCluster.ask[GotRoomAgent](GetRoomAgent(room, _)).map(_.serverRef)
      event      <- internalPostEvent(roomServer, sender, eventType, None, content, parentsId, waitProcess)
    } yield event
  }

  def postStateEvent(
      room: Room,
      sender: User,
      eventType: String,
      stateKey: String,
      content: Option[Json],
      parentsId: Set[UUID] = Set.empty,
      waitProcess: Boolean = true
  ): Future[Event] = {
    for {
      roomServer <- roomCluster.ask[GotRoomAgent](GetRoomAgent(room, _)).map(_.serverRef)
      event      <- internalPostEvent(roomServer, sender, eventType, Some(stateKey), content, parentsId, waitProcess)
    } yield event
  }

  private def internalPostEvent(
      roomServer: EntityRef[RoomCommand],
      sender: User,
      eventType: String,
      stateKey: Option[String],
      content: Option[Json],
      parentsId: Set[UUID] = Set.empty,
      waitProcess: Boolean = false
  ): Future[Event] = {
    for {
      roomState <- roomServer.ask[RoomStateResponse](GetRoomState).map(_.roomState)
      sequence  <- eventStreamSequence.ask[EventStreamSequenceValue](NextVal.apply)
      parentEventsId <- Future.successful(if (parentsId.isEmpty) {
        roomState.lastResolvedEvent.map(lastEvent => Set(lastEvent.eventId)).getOrElse(Set.empty)
      } else {
        parentsId
      })
      authEventsId <- Future.successful(selectAuthEvents(eventType, stateKey, content, sender, roomState.resolvedState))
      event <- Future.fromTry(
        eventService.createEvent(
          roomState.room,
          eventType,
          stateKey,
          sender,
          content,
          sequence.value,
          parentEventsId,
          authEventsId
        )
      )
      _ <-
        if (waitProcess) {
          roomServer.ask[EventProcessed](replyTo => ProcessEvent(event, Some(replyTo)))
        } else {
          Future.successful(roomServer ! ProcessEvent(event, None))
        }
    } yield event
  }

  /**
    * Select events from a room state which identifies the authentify a new event being posted
    * See https://matrix.org/docs/spec/server_server/r0.1.4#auth-events-selection
    * @param eventType
    * @param sender
    * @param roomStateSet
    * @return
    */
  private def selectAuthEvents(
      eventType: String,
      stateKey: Option[String],
      eventContent: Option[Json],
      sender: User,
      roomStateSet: StateSet
  ): Set[UUID] = {
    if (eventType == EventTypes.M_ROOM_CREATE)
      Set.empty
    else {
      val eventSet = mutable.Set[Option[Event]]()
      eventSet.add(roomStateSet.getEvent(EventTypes.M_ROOM_CREATE, ""))
      eventSet.add(roomStateSet.getEvent(EventTypes.M_ROOM_POWER_LEVELS, ""))
      eventSet.add(roomStateSet.getEvent(EventTypes.M_ROOM_MEMBER, sender.mxUserId))
      if (eventType == EventTypes.M_ROOM_MEMBER) {
        ModelEventUtils.decodeMemberEventContent(eventContent.get).map { memberEventContent =>
          stateKey.map(target => eventSet.add(roomStateSet.getEvent(EventTypes.M_ROOM_MEMBER, target)))
          if (List("join", "invite").contains(memberEventContent.membership))
            eventSet.add(roomStateSet.getEvent(EventTypes.M_ROOM_JOIN_RULES, ""))
          if (memberEventContent.membership.equals("invite") && memberEventContent.third_party_invite.isDefined) {
            eventSet.add(
              roomStateSet
                .getEvent(EventTypes.M_ROOM_THIRD_PARTY_INVITE, memberEventContent.third_party_invite.get.signed.token)
            )
          }
        }
      }
      eventSet.filter(_.isDefined).map(_.get.eventId).toSet
    }
  }

  /**
    * Create all events necessary for a room creation
    *
    * @param roomServer RoomServer instance in charge of handling events
    * @param sender     User sender of the creation request
    * @param request    API creation request
    * @return
    */
  def postCreateRoomEvents(
      roomServer: EntityRef[RoomCommand],
      sender: User,
      request: CreateRoomRequest
  ): Future[RoomState] = {
    val creationContent =
      request.creation_content
        .getOrElse(CreationContent(creator = sender.mxUserId))
        .copy(creator = sender.mxUserId, room_version = Some(defaultRoomVersion))
    val defaultPowerLevels = PowerLevelEventContent(
      ban = 50,
      events = Map.empty,
      events_default = 0,
      invite = 50,
      kick = 50,
      redact = 50,
      state_default = 50,
      Map(sender.mxUserId -> 100),
      users_default = Some(0),
      notifications = Map.empty
    )
    val powerLevels = request.power_level_content_override match {
      case Some(powerOverride) =>
        defaultPowerLevels.copy(
          ban = powerOverride.ban,
          events = defaultPowerLevels.events ++ powerOverride.events,
          events_default = powerOverride.events_default,
          invite = powerOverride.invite,
          kick = powerOverride.kick,
          redact = powerOverride.redact,
          state_default = powerOverride.state_default,
          users = defaultPowerLevels.users ++ powerOverride.users,
          users_default = powerOverride.users_default,
          notifications = defaultPowerLevels.notifications ++ powerOverride.notifications
        )
      case None => defaultPowerLevels
    }

    //TODO: implement invitation events on room creation
    if (request.invite.isDefined && request.invite.get.nonEmpty)
      logger.warn("Invitations not yet supported on room creation")
    if (request.invite_3pid.isDefined && request.invite_3pid.get.nonEmpty)
      logger.warn("3Pid invitations not yet supported on room creation")

    for {
      createEventId <-
        internalPostEvent(roomServer, sender, EventTypes.M_ROOM_CREATE, Some(""), Some(creationContent.asJson))
          .map(_.eventId)
      memberEventId <- internalPostEvent(
        roomServer,
        sender,
        EventTypes.M_ROOM_MEMBER,
        Some(sender.mxUserId),
        Some(MemberEventContent(membership = "join", is_direct = request.is_direct).asJson)
      )
        .map(_.eventId)
      powerLevelsEventId <- internalPostEvent(
        roomServer,
        sender,
        EventTypes.M_ROOM_POWER_LEVELS,
        Some(""),
        Some(powerLevels.asJson)
      )
      (_, _, guestAccessEventId) <- createPresetEvents(
        request.preset,
        sender,
        roomServer
      )
      stateEvents <- createInitialStateEvents(
        request.initial_state.getOrElse(List.empty),
        sender,
        roomServer
      )
      _ <- createNameTopicAndAlias(
        request.name,
        request.topic,
        request.room_alias_name,
        sender,
        roomServer
      )
      roomStateResponse <- roomServer.ask[RoomStateResponse](replyTo => GetRoomState(replyTo))
    } yield roomStateResponse.roomState
  }

  private def createPresetEvents(
      preset: Option[String],
      sender: User,
      roomServer: EntityRef[RoomCommand]
  ): Future[(UUID, UUID, UUID)] = {
    val (joinRule, historyVisibility, guestAccess): (String, String, String) = preset match {
      case Some("private_chat")         => ("invite", "shared", "can_join")
      case Some("trusted_private_chat") => ("invite", "shared", "can_join")
      case Some("public_chat")          => ("public", "shared", "forbidden")
      case other                        => throw new ApiFailure("M_UNRECOGNIZED", s"Unrecognized preset value '$other'")
    }
    for {
      joinRulesEventId <- internalPostEvent(
        roomServer,
        sender,
        EventTypes.M_ROOM_JOIN_RULES,
        Some(""),
        Some(Map("join_rule" -> joinRule).asJson)
      )
        .map(_.eventId)
      historyVisibilityEventId <- internalPostEvent(
        roomServer,
        sender,
        EventTypes.M_ROOM_HISTORY_VISIBILITY,
        Some(""),
        Some(Map("history_visibility" -> historyVisibility).asJson)
      )
        .map(_.eventId)
      guestAccessEventId <- internalPostEvent(
        roomServer,
        sender,
        EventTypes.M_ROOM_GUEST_ACCESS,
        Some(""),
        Some(Map("guest_access" -> guestAccess).asJson)
      )
        .map(_.eventId)
    } yield (joinRulesEventId, historyVisibilityEventId, guestAccessEventId)
  }

  private def createInitialStateEvents(
      initialState: List[StateEvent],
      sender: User,
      roomServer: EntityRef[RoomCommand]
  ): Future[List[UUID]] = {
    Future.sequence {
      initialState.map { stateEvent =>
        internalPostEvent(
          roomServer,
          sender,
          stateEvent.`type`,
          Some(stateEvent.state_key),
          stateEvent.content
        ).map(_.eventId)
      }
    }
  }

  private def createNameTopicAndAlias(
      nameOpt: Option[String],
      topicOpt: Option[String],
      aliasOpt: Option[String],
      sender: User,
      roomServer: EntityRef[RoomCommand]
  ): Future[Seq[UUID]] = {
    val nameFuture = nameOpt.map { name =>
      internalPostEvent(
        roomServer,
        sender,
        EventTypes.M_ROOM_NAME,
        Some(""),
        Some(RoomNameEventContent(name).asJson)
      ).map(_.eventId)
    }
    val topicFuture = topicOpt.map { topic =>
      internalPostEvent(
        roomServer,
        sender,
        EventTypes.M_ROOM_TOPIC,
        Some(""),
        Some(Map("topic" -> topic).asJson)
      ).map(_.eventId)
    }
    val aliasFuture = aliasOpt.flatMap { aliasName =>
      RoomAliasIdentifierUtils.build(aliasName, matrixServerName).toOption.map { alias =>
        internalPostEvent(
          roomServer,
          sender,
          EventTypes.M_ROOM_ALIASES,
          Some(""),
          Some(RoomAliasEventContent(List(alias.toString)).asJson)
        ).map(_.eventId)
      }
    }

    val f = for {
      f1 <- nameFuture
      f2 <- topicFuture
      f3 <- aliasFuture
    } yield Future.sequence(Seq(f1, f2, f3))
    f.getOrElse(Future.successful(Seq.empty))
  }

  /**
    * Update a user membership into a room.
    * This doesn't create an event but references a membership (coming from a `m.room.member` event) into the
    * room_memberships table
    *
    * @param room            room for new membership
    * @param user            member
    * @param membershipEvent the `m.room.event` giving the membership
    * @param membership      event content
    * @return `RoomMembership` instance as created into room_membership table
    */
  def createOrUpdateMembership(
      room: Room,
      user: User,
      membershipEvent: Event,
      membership: String
  ): Try[RoomMembership] = {
    roomRepo.createOrUpdateMembership(room.roomId, user.userId, membershipEvent.eventId, membership)
  }

  /**
    * Compute room summary at given point in stream
    *
    * @param room                the room to compute summary for
    * @param streamOrderPosition point in the stream order as time reference
    * @return
    * TODO: Refactor to use streamOrderPosition parameter
    */
  def getRoomSummary(room: Room, streamOrderPosition: Long, clientUserId: Option[String]): Future[RoomSummary] = {
    val joinedMemberCountFuture  = Future.fromTry(roomRepo.getRoomMembers(room.roomId, "join").map(_.size))
    val invitedMemberCountFuture = Future.fromTry(roomRepo.getRoomMembers(room.roomId, "invite").map(_.size))
    val heroesFuture = Future.fromTry(
      getRoomStateAtStreamOrder(room, streamOrderPosition).map(stateSet =>
        roomHeroes(stateSet.getOrElse(StateSet()), clientUserId)
      )
    )
    val forFuture = for {
      joinedMemberCount  <- joinedMemberCountFuture
      invitedMemberCount <- invitedMemberCountFuture
      heroes             <- heroesFuture
    } yield (joinedMemberCount, invitedMemberCount, heroes)
    forFuture.map {
      case (joinedMemberCount, invitedMemberCount, heroes) =>
        RoomSummary(
          `m.joined_member_count` = joinedMemberCount,
          `m.invited_member_count` = invitedMemberCount,
          `m.heroes` = Some(heroes)
        )
    }
  }

  /**
    * The users which can be used to generate a room name if the room does not have one.
    *
    * This should be the first 5 members of the room, ordered by stream ordering, which are joined or invited.
    * The list must never include the client's own user ID. When no joined or invited members are available,
    * this should consist of the banned and left users. More than 5 members may be provided, however less
    * than 5 should only be provided when there are less than 5 members to represent.
    *
    * @param stateSet
    * @param clientUserId
    */
  def roomHeroes(stateSet: StateSet, clientUserId: Option[String]): List[String] = {
    val members =
      memberList(stateSet, membership => membership.membership == "join" || membership.membership == "invite")
        .filter(_ != clientUserId.getOrElse(""))
        .take(5)
    if (members.nonEmpty)
      members
    else
      memberList(stateSet, membership => membership.membership == "ban" || membership.membership == "leave")
        .filter(_ != clientUserId.getOrElse(""))
        .take(5)
  }

  def memberList(stateSet: StateSet, membershipPred: MemberEventContent => Boolean): List[String] = {
    stateSet.toList
      .filter(_._1 == EventTypes.M_ROOM_MEMBER)
      .sortBy(_._3.streamOrder)
      .map { case (eventType, stateKey, event) => stateKey -> stateSetService.getMembershipEventContent(event) }
      .filter(_._2.isSuccess)
      .map { case (member, membership) => (member, membership.get) }
      .filter {
        case (_, membership) => membershipPred(membership)
      }
      .map(_._1)
  }

  /**
    * Get the room state at a the closest (or equal) stream order position before the given position
    * if
    *
    * @param room
    * @param streamOrderPosition
    * @return
    */
  def getRoomStateAtStreamOrder(room: Room, streamOrderPosition: Long): Try[Option[StateSet]] = {
    roomRepo.getRoomLastEventBeforeStreamPosition(room.roomId, streamOrderPosition).flatMap {
      case Some(event) => stateSetService.loadStateSetForEvent(event).map(Some(_))
      case None        => Success(None)
    }
  }

  /**
    * Get the room state at the given event
    * if
    *
    * @param room
    * @param event
    * @return
    */
  def getRoomStateAtEvent(room: Room, event: Event): Try[StateSet] = {
    stateSetService.loadStateSetForEvent(event)
  }

  /**
    * Diff two room state set given their event stream Order position
    */
  def diffRoomStateSet(room: Room, oldStreamOrderPosition: Long, newStreamOrderPosition: Long): Try[StateSet] = {
    // logger.debug(s"Diff ${room.id} from $oldStreamOrderPosition to $newStreamOrderPosition")
    for {
      oldState <- getRoomStateAtStreamOrder(room, oldStreamOrderPosition)
      newState <- getRoomStateAtStreamOrder(room, newStreamOrderPosition)
    } yield newState.getOrElse(StateSet()).diff(oldState.getOrElse(StateSet()))
  }

  /**
    * get the last event of a room. Last event is the last processed event with the max streamOrderPosition
    * among room events
    *
    * @param room the room to query
    * @return the stream order of the most recent room event
    */
  def getRoomLastEvent(room: Room): Try[Option[Event]] = roomRepo.getRoomLastEvent(room.roomId)

  def getRoomEvents(
      room: Room,
      minStreamOder: Long = 0L,
      maxStreamOrder: Long = Long.MaxValue
  ): Try[List[Event]] = roomRepo.getRoomEvents(room.roomId, minStreamOder, maxStreamOrder)

  def createRoomAlias(
      room: Room,
      creatorId: User,
      alias: String,
      serverDomain: String
  ): Try[(RoomAlias, RoomAliasServer)] =
    roomRepo.createRoomAlias(room.roomId, creatorId.userId, alias, serverDomain)

  def getRoomAlias(alias: String): Try[Option[RoomAliasResponse]] = {
    roomRepo.getRoomAlias(alias).flatMap {
      case Some((alias, servers)) =>
        getRoomById(alias.roomId).map {
          case Some(room) => Some(RoomAliasResponse(room, alias, servers))
          case None       => None
        }
      case _ => Success(None)
    }
  }

  def getRoomKnownAliases(room: Room): Try[List[RoomAlias]] = { roomRepo.getKnownAliases(room.roomId) }

  /**
    * Add a new room alias.
    * This method get all known local alias and then post a new m.room.aliases event
    * which contains all alias plus the new one
    * @param room
    * @param alias
    * @return
    */
  def addRoomAliasPostEvent(
      room: Room,
      sender: User,
      alias: String
  ): Future[Event] = {
    Future.fromTry(roomRepo.getKnownAliases(room.roomId, matrixServerName)).flatMap { knownAliases =>
      val aliasEventContent = RoomAliasEventContent(knownAliases.map(_.alias).appended(alias))
      postStateEvent(
        room,
        sender,
        EventTypes.M_ROOM_ALIASES,
        matrixServerName,
        Some(aliasEventContent.asJson)
      )
    }
  }

  def postRoomMemberEvent(
      room: Room,
      sender: User,
      memberUser: User,
      membership: String
  ): Future[Event] = {
    require(List("invite", "join", "knock", "leave", "ban").contains(membership))
    postStateEvent(
      room,
      sender,
      EventTypes.M_ROOM_MEMBER,
      memberUser.mxUserId,
      Some(MemberEventContent(membership = membership).asJson)
    )
  }

  /**
    * Get a room state at its last event
    * @param room
    * @return
    */
  def getRoomLastState(room: Room): Try[StateSet] =
    for {
      lastEvent <- getRoomLastEvent(room)
      lastState <- getRoomStateAtEvent(room, lastEvent.get) if lastEvent.isDefined
    } yield lastState

  /**
    * Set a room visibility. Only its creator can change a room visibility
    * @param room
    * @return
    */
  def setRoomVisibility(room: Room, user: User, visibility: String): Try[Room] = {
    getRoomLastState(room).flatMap { stateSet =>
      if (stateSet.getEvent(EventTypes.M_ROOM_CREATE, "").get.senderId == user.userId) {
        roomRepo
          .updateRoomVisibility(room.roomId, visibility)
          .map(now => room.copy(visibility = visibility, updatedAt = Some(now)))
      } else
        Failure(new IllegalArgumentException("Only room creator can change room visibility"))
    }
  }

  def getRoomMembers(room: Room, membership: String = "join"): Try[List[(User, Event)]] =
    roomRepo.getRoomMembers(room.roomId, membership).map(_.toList)

  def indexRoomTerm(room: Room, eventType: String, indexTerm: String): Try[RoomIndex] =
    roomRepo.createOrUpdateRoomIndex(room.roomId, eventType, indexTerm)

  def searchPublicRooms(searchTerm: Option[String]): Try[Seq[Room]] = {
    roomRepo.searchPublicRoomIndex(searchTerm)
  }
}
