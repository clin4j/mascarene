/*
 * Mascarene
 * Copyright (C) 2020  mascarene.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.mascarene.homeserver.services

import org.mascarene.homeserver.RuntimeContext
import org.mascarene.homeserver.internal.model.{Event, Room, RoomMembership, User}
import org.mascarene.homeserver.internal.repository.{AuthRepo, RoomRepo}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.{Success, Try}

class UserService(implicit val runtimeContext: RuntimeContext) {
  val roomRepo = new RoomRepo
  val authRepo = new AuthRepo

  /**
    * Get a user room membership
    * @param user the user to find membership
    * @return a list of triplet containing for each membership : the membered room, the membership type
    *         ("join", "ban"...) and the state event stating this membership
    */
  def getRoomMemberships(user: User): Future[Iterable[(Room, String, Event)]] =
    Future.fromTry {
      roomRepo.getRoomMemberships(user.userId).map { it =>
        it.map { case (membership, room, event) => (room, membership.membership, event) }
      }
    }

  /**
    * Get a user membership status for a specific room
    * @param user the user to find membership
    * @param room
    * @return Some room membership if any.
    */
  def getRoomMembership(room: Room, user: User): Try[Option[RoomMembership]] =
    roomRepo.getRoomMembership(room.roomId, user.userId)

  def getRoomMembershipWithMemberEvent(room: Room, user: User): Try[Option[(RoomMembership, Event)]] =
    roomRepo.getRoomMembershipWithMemberEvent(room.roomId, user.userId)

  def forgetRoom(room: Room, user: User): Try[Unit] =
    roomRepo.forceMembership(room.roomId, user.userId, "forget").map(_ => ())

  def joinedRoom(user: User): Future[Iterable[Room]] =
    getRoomMemberships(user).map { it =>
      it.filter { case (_, membership, _) => membership == "join" }.map(_._1)
    }

  def searchUserDirectory(searchTerm: String, limit: Option[Int]): Try[(List[String], Boolean)] = {
    authRepo.searchUserDirectory(searchTerm).map { searchResult =>
      limit match {
        case Some(limit) => (searchResult.take(limit), searchResult.size > limit)
        case None        => (searchResult, false)
      }
    }
  }

  def getUserByMxId(mxId: String): Try[Option[User]] = authRepo.getUserByMxId(mxId)

  def getOrCreateUserByMxId(mxId: String): Try[User] =
    authRepo.getUserByMxId(mxId).flatMap {
      case Some(user) => Success(user)
      case None       => authRepo.createUser(mxId)
    }

  def setUserDisplayName(user: User, displayName: Option[String]): Try[User] =
    authRepo.updateUserDisplayName(user.userId, displayName).map { updatedAt =>
      user.copy(displayName = displayName, updatedAt = Some(updatedAt))
    }

  def setUserAvatarUrl(user: User, avatarUrl: Option[String]): Try[User] =
    authRepo.updateUserAvatarUrl(user.userId, avatarUrl).map { updatedAt =>
      user.copy(avatarUrl = avatarUrl, updatedAt = Some(updatedAt))
    }
}
