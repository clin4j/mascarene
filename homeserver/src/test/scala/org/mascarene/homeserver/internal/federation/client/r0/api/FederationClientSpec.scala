package org.mascarene.homeserver.internal.federation.client.r0.api

import akka.actor.testkit.typed.scaladsl.ActorTestKit
import akka.http.scaladsl.ConnectionContext
import akka.http.scaladsl.model.Uri
import akka.http.scaladsl.model.headers.Host
import org.bouncycastle.crypto.generators.Ed25519KeyPairGenerator
import org.bouncycastle.crypto.params.{Ed25519KeyGenerationParameters, Ed25519PrivateKeyParameters}
import org.mascarene.homeserver.TestEnv
import org.mascarene.homeserver.internal.federation.client.KeySpec
import org.mascarene.matrix.server.r0.model.{ListPublicRoomsResponse, ServerKeysResponse, ServerVersionResponse}
import org.mascarene.utils.Codecs
import org.scalatest.BeforeAndAfterAll
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AsyncWordSpecLike

import java.security.SecureRandom
import java.util.concurrent.TimeUnit
import javax.net.ssl.{SSLContext, SSLEngine}
import scala.concurrent.duration.FiniteDuration

class FederationClientSpec extends TestEnv with AsyncWordSpecLike with Matchers with BeforeAndAfterAll {

  def createInsecureSslEngine(host: String, port: Int): SSLEngine = {
    val engine = SSLContext.getDefault.createSSLEngine(host, port)
    engine.setUseClientMode(true)
    engine
  }
  val testCtx = Some(ConnectionContext.httpsClient(createInsecureSslEngine _))

  //Generate private key
  val secRandom        = new SecureRandom()
  val keyPairGenerator = new Ed25519KeyPairGenerator
  keyPairGenerator.init(new Ed25519KeyGenerationParameters(secRandom))
  val asymmetricCipherKeyPair = keyPairGenerator.generateKeyPair
  val privateKey              = asymmetricCipherKeyPair.getPrivate.asInstanceOf[Ed25519PrivateKeyParameters]

  //Generate key ID
  val keyBytes = new Array[Byte](5)
  secRandom.nextBytes(keyBytes)
  val keyId   = "ed25519:" + Codecs.toBase64(keyBytes, unpadded = true)
  val keySpec = KeySpec(keyId, privateKey.generatePublicKey(), privateKey)

  override def afterAll(): Unit = testKit.shutdownTestKit()

  "FederationClient" should {
    val matrixClient =
      new FederationClient(
        runtimeContext,
        keySpec,
        "localhost:8448",
        FiniteDuration(24, TimeUnit.HOURS),
        "localhost",
        testCtx
      )(
        testKit.internalSystem,
        testKit.timeout
      )

    "resolve IP server name" in {
      val federationClient =
        new FederationClient(runtimeContext, keySpec, "1.2.3.4", FiniteDuration(24, TimeUnit.HOURS), "localhost")(
          testKit.internalSystem,
          testKit.timeout
        )
      federationClient.resolveServerName.map { response =>
        response.apiRoot shouldEqual Uri("https://1.2.3.4:8448/")
        response.hostHeader shouldEqual Host("1.2.3.4")
      }
    }

    "resolve name server name" ignore {
      matrixClient.resolveServerName.map { response =>
        response.apiRoot shouldEqual Uri("https://localhost:8448/")
      }
    }

    "resolve name server name with cache" ignore {
      val future = for {
        _        <- matrixClient.resolveServerName
        response <- matrixClient.resolveServerName
      } yield response
      future.map { response =>
        response.apiRoot shouldEqual Uri("https://localhost:8448/")
      }
    }

    "get server version" ignore {
      matrixClient.getFederationVersion.map { response =>
        response shouldBe a[ServerVersionResponse]
      }
    }

    "get server keys" ignore {
      matrixClient.getServerKeys.map { response =>
        response shouldBe a[ServerKeysResponse]
      }
    }

    "get some other server keys" ignore {
      matrixClient.getServerKeys("beerfactory.org").map { response =>
        response shouldBe a[ServerKeysResponse]
      }
    }

    "get public rooms" ignore {
      matrixClient.getPublicRooms(Some(20), None, Some(false)).map { response =>
        response shouldBe a[ListPublicRoomsResponse]
      }
    }
  }
}
