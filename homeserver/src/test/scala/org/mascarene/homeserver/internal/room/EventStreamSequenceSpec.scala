/*
 * Mascarene
 * Copyright (C) 2020  mascarene.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.mascarene.homeserver.internal.room

import akka.actor.testkit.typed.scaladsl.ActorTestKit
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport
import org.mascarene.homeserver.TestEnv
import org.mascarene.homeserver.internal.rooms.EventStreamSequence
import org.mascarene.homeserver.internal.rooms.EventStreamSequence.{EventStreamSequenceValue, NextVal}
import org.scalatest.{BeforeAndAfter, BeforeAndAfterAll}
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpecLike

class EventStreamSequenceSpec
    extends AnyWordSpecLike
    with Matchers
    with FailFastCirceSupport
    with TestEnv
    with BeforeAndAfter
    with BeforeAndAfterAll {

  before {
    flyway.clean()
    flyway.migrate()
  }

  override def afterAll(): Unit = {
    testKit.shutdownTestKit()
  }

  "EventStreamSequence" should {
    val sequenceActor = testKit.spawn(EventStreamSequence.apply, "EventStreamSequence")

    "reply with nextval" in {
      val probe = testKit.createTestProbe[EventStreamSequence.EventStreamSequenceValue]()
      sequenceActor ! NextVal(probe.ref)
      probe.expectMessage(EventStreamSequenceValue(1))
      sequenceActor ! NextVal(probe.ref)
      probe.expectMessage(EventStreamSequenceValue(2))
      sequenceActor ! NextVal(probe.ref)
      sequenceActor ! NextVal(probe.ref)
      probe.expectMessage(EventStreamSequenceValue(3))
      probe.expectMessage(EventStreamSequenceValue(4))
    }
  }
}
