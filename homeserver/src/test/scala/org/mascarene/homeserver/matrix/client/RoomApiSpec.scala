/*
 * Mascarene
 * Copyright (C) 2020  mascarene.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.mascarene.homeserver.matrix.client

import java.net.URLEncoder
import java.util.UUID

import akka.http.scaladsl.model.StatusCodes
import akka.actor.typed.scaladsl.adapter._
import akka.http.scaladsl.model.headers.{Authorization, OAuth2BearerToken}
import akka.http.scaladsl.testkit.ScalatestRouteTest
import com.google.common.util.concurrent.RateLimiter
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport
import io.circe.Json
import io.circe.generic.auto._
import org.mascarene.homeserver.TestEnv
import org.mascarene.homeserver.internal.auth.AuthSessionCache
import org.mascarene.homeserver.internal.rooms.{EventStreamSequence, RoomCluster}
import org.mascarene.homeserver.matrix.server.client.auth.AuthApiRoutes
import org.mascarene.homeserver.matrix.server.client.rooms.{RoomApiRoutes, RoomEventsApiRoutes}
import org.mascarene.matrix.client.r0.model.auth._
import org.mascarene.matrix.client.r0.model.events.{RoomEvent, RoomMember, StateEvent}
import org.mascarene.matrix.client.r0.model.rooms.{CreateRoomRequest, CreateRoomResponse, CreationContent}
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpecLike
import org.scalatest.{BeforeAndAfter, BeforeAndAfterAll}

class RoomApiSpec
    extends AnyWordSpecLike
    with Matchers
    with FailFastCirceSupport
    with ScalatestRouteTest
    with TestEnv
    with BeforeAndAfter
    with BeforeAndAfterAll {

  implicit def typedSystem = testKit.system

  flyway.clean()
  flyway.migrate()

  override def afterAll(): Unit = {
    testKit.shutdownTestKit()
  }

  override def createActorSystem(): akka.actor.ActorSystem = testKit.system.toClassic

  runtimeContext.actorRegistry
    .put("AuthSessionCache", testKit.spawn(AuthSessionCache.apply, "AuthSessionCache").unsafeUpcast)
  private val eventStreamSequence = testKit.spawn(EventStreamSequence.apply, "EventStreamSequence")
  runtimeContext.actorRegistry.put("EventStreamSequence", eventStreamSequence.unsafeUpcast)
  private val roomCluster = testKit.spawn(RoomCluster.apply, "RoomCluster")
  runtimeContext.actorRegistry.put("RoomCluster", roomCluster.unsafeUpcast)
  private val rateLimiter = RateLimiter.create(clientCallRate)

  private val authApiRoutes       = new AuthApiRoutes(testKit.system, rateLimiter)
  private val roomApiRoutes       = new RoomApiRoutes(testKit.system, roomCluster, rateLimiter)
  private val roomEventsApiRoutes = new RoomEventsApiRoutes(testKit.system, roomCluster)

  "Room API" should {
    var accessToken: String = ""

    Post("/_matrix/client/r0/register", RegisterRequest()) ~> authApiRoutes.routes ~> check {
      status shouldEqual StatusCodes.Unauthorized
      val flow = responseAs[AuthFlow]
      val authRequest = RegisterRequest(
        auth = Some(Map("type" -> "m.login.dummy", "session" -> flow.session.get)),
        username = Some("someuser"),
        password = Some("password")
      )
      Post("/_matrix/client/r0/register", authRequest) ~> authApiRoutes.routes ~> check {
        status shouldEqual StatusCodes.OK
        val registerResponse = responseAs[RegisterResponse]
        registerResponse.user_id should not be empty
        registerResponse.device_id should not be empty
        registerResponse.access_token should not be empty

        val loginRequest =
          LoginRequest("m.login.password", Map("type" -> "user", "user" -> "someuser"), Some("password"))
        Post("/_matrix/client/r0/login", loginRequest) ~> authApiRoutes.routes ~> check {
          status shouldEqual StatusCodes.OK
          val loginResponse = responseAs[LoginResponse]
          accessToken = loginResponse.access_token
        }
      }
    }

    "create room" in {
      val createRoomRequest =
        CreateRoomRequest(name = Some("TestRoom"), topic = Some("RoomAPI test room"))
      Post("/_matrix/client/r0/createRoom", createRoomRequest).withHeaders(
        Seq(Authorization(OAuth2BearerToken(accessToken)))
      ) ~> roomApiRoutes.routes ~> check {
        status shouldEqual StatusCodes.OK
      }
    }

    "put message" in {
      val createRoomRequest =
        CreateRoomRequest(name = Some("putMessage"), topic = Some("RoomAPI test room"))
      Post("/_matrix/client/r0/createRoom", createRoomRequest).withHeaders(
        Seq(Authorization(OAuth2BearerToken(accessToken)))
      ) ~> roomApiRoutes.routes ~> check {
        status shouldEqual StatusCodes.OK
        val createRoomResponse = responseAs[CreateRoomResponse]
        val roomId             = createRoomResponse.room_id
        val encodedRoomId      = URLEncoder.encode(roomId, "UTF-8")
        Put(
          s"/_matrix/client/r0/rooms/$encodedRoomId/send/m.room.message/${UUID.randomUUID().toString}",
          Json.fromString("TEST")
        )
          .withHeaders(
            Seq(Authorization(OAuth2BearerToken(accessToken)))
          ) ~> roomApiRoutes.routes ~> check {
          status shouldEqual StatusCodes.OK
          val response = responseAs[Map[String, String]]
          response.get("event_id") shouldBe defined
        }
      }
    }

    "get message" in {
      val createRoomRequest =
        CreateRoomRequest(name = Some("getExistingMessage"), topic = Some("RoomAPI test room"))
      Post("/_matrix/client/r0/createRoom", createRoomRequest).withHeaders(
        Seq(Authorization(OAuth2BearerToken(accessToken)))
      ) ~> roomApiRoutes.routes ~> check {
        status shouldEqual StatusCodes.OK
        val createRoomResponse = responseAs[CreateRoomResponse]
        val roomId             = createRoomResponse.room_id
        val encodedRoomId      = URLEncoder.encode(roomId, "UTF-8")
        Put(
          s"/_matrix/client/r0/rooms/$encodedRoomId/send/m.room.message/${UUID.randomUUID().toString}",
          Json.fromString("TEST")
        )
          .withHeaders(
            Seq(Authorization(OAuth2BearerToken(accessToken)))
          ) ~> roomApiRoutes.routes ~> check {
          status shouldEqual StatusCodes.OK
          val response = responseAs[Map[String, String]]
          response.get("event_id") shouldBe defined
          val eventId        = response("event_id")
          val encodedEventId = URLEncoder.encode(eventId, "UTF-8")

          Get(s"/_matrix/client/r0/rooms/$encodedRoomId/event/$encodedEventId").withHeaders(
            Seq(Authorization(OAuth2BearerToken(accessToken)))
          ) ~> roomEventsApiRoutes.routes ~> check {
            status shouldEqual StatusCodes.OK
            val response = responseAs[RoomEvent]
            response.event_id shouldEqual eventId
          }
        }
      }
    }
    "get state event" in {
      val createRoomRequest =
        CreateRoomRequest(name = Some("TestRoom"), topic = Some("RoomAPI test room"))
      Post("/_matrix/client/r0/createRoom", createRoomRequest).withHeaders(
        Seq(Authorization(OAuth2BearerToken(accessToken)))
      ) ~> roomApiRoutes.routes ~> check {
        status shouldEqual StatusCodes.OK
        val createRoomResponse = responseAs[CreateRoomResponse]
        val roomId             = createRoomResponse.room_id
        val encodedRoomId      = URLEncoder.encode(roomId, "UTF-8")
        Get(s"/_matrix/client/r0/rooms/$encodedRoomId/state/m.room.create").withHeaders(
          Seq(Authorization(OAuth2BearerToken(accessToken)))
        ) ~> roomEventsApiRoutes.routes ~> check {
          status shouldEqual StatusCodes.OK
          val response = responseAs[CreationContent]
          response shouldBe a[CreationContent]
        }
      }
    }
    "get room state" in {
      val createRoomRequest =
        CreateRoomRequest(name = Some("getRoomState"), topic = Some("RoomAPI test room"))
      Post("/_matrix/client/r0/createRoom", createRoomRequest).withHeaders(
        Seq(Authorization(OAuth2BearerToken(accessToken)))
      ) ~> roomApiRoutes.routes ~> check {
        status shouldEqual StatusCodes.OK
        val createRoomResponse = responseAs[CreateRoomResponse]
        val roomId             = createRoomResponse.room_id
        val encodedRoomId      = URLEncoder.encode(roomId, "UTF-8")
        Get(s"/_matrix/client/r0/rooms/$encodedRoomId/state").withHeaders(
          Seq(Authorization(OAuth2BearerToken(accessToken)))
        ) ~> roomEventsApiRoutes.routes ~> check {
          status shouldEqual StatusCodes.OK
          val response = responseAs[List[StateEvent]]
        }
      }
    }
    "get room joined members" in {
      val createRoomRequest =
        CreateRoomRequest(name = Some("getRoomJoinedMembers"), topic = Some("RoomAPI test room"))
      Post("/_matrix/client/r0/createRoom", createRoomRequest).withHeaders(
        Seq(Authorization(OAuth2BearerToken(accessToken)))
      ) ~> roomApiRoutes.routes ~> check {
        status shouldEqual StatusCodes.OK
        val createRoomResponse = responseAs[CreateRoomResponse]
        val roomId             = createRoomResponse.room_id
        val encodedRoomId      = URLEncoder.encode(roomId, "UTF-8")
        Get(s"/_matrix/client/r0/rooms/$encodedRoomId/joined_members").withHeaders(
          Seq(Authorization(OAuth2BearerToken(accessToken)))
        ) ~> roomEventsApiRoutes.routes ~> check {
          status shouldEqual StatusCodes.OK
          val response = responseAs[Map[String, Map[String, RoomMember]]]
        }
      }
    }
  }
}
