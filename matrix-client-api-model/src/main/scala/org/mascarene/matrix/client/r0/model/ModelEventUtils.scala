package org.mascarene.matrix.client.r0.model

import io.circe.Json
import org.mascarene.matrix.client.r0.model.events.MemberEventContent
import io.circe._
import io.circe.generic.auto._

import scala.util.Try

object ModelEventUtils {
  def decodeMemberEventContent(content: Json): Try[MemberEventContent] = content.as[MemberEventContent].toTry

}
