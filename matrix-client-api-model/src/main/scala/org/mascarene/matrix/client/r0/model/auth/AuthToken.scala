package org.mascarene.matrix.client.r0.model.auth

case class AuthToken(value: String)
