/*
Copyright 2018 Nicolas Jouanin

This file is part of Plasma.

Plasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License 3.

Plasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>
 */
package org.mascarene.matrix.client.r0.model

import io.circe.Json

package object events {

  case class SyncResponse(
      next_batch: String,
      var rooms: Option[Rooms] = None,
      presence: Option[Presence] = None,
      account_data: Option[AccountData] = None,
      to_device: Option[ToDevice] = None,
      device_lists: Option[DeviceLists] = None
  )

  case class Rooms(
      var join: Map[String, JoinedRoom] = Map.empty,
      invite: Map[String, InvitedRoom] = Map.empty,
      leave: Map[String, LeftRoom] = Map.empty
  )
  case class JoinedRoom(
      summary: Option[RoomSummary] = None,
      state: Option[State] = None,
      timeline: Option[Timeline] = None,
      ephemeral: Option[Ephemeral] = None,
      account_data: Option[AccountData] = None,
      unread_notifications: Option[UnreadNotificationsCount] = None
  )
  case class RoomSummary(
      `m.heroes`: Option[List[String]] = None,
      `m.joined_member_count`: Int,
      `m.invited_member_count`: Int
  )
  case class InvitedRoom(invite_state: InviteState)
  case class LeftRoom(state: State, timeline: Timeline, account_data: Option[AccountData] = None)
  case class InviteState(events: List[StrippedEvent])
  case class StrippedEvent(content: Json, state_key: String, `type`: String, sender: String)
  case class State(events: List[StateEvent])
  case class Timeline(events: List[TimeLineEvent], limited: Option[Boolean] = None, prev_batch: Option[String] = None)
  case class Ephemeral(events: List[Event])
  case class UnreadNotificationsCount(higher_count: Option[Int], notification_count: Option[Int])

  case class Presence(events: List[Event] = List.empty)
  case class AccountData(events: List[AccountDataEvent] = List.empty)
  case class AccountDataEvent(`type`: String, content: Json)
  case class ToDevice(events: List[DeviceEvent])
  case class DeviceEvent(content: Json, sender: String, `type`: String)
  case class DeviceLists(changed: List[String])
  case class TimeLineEvent(
      content: Json,
      `type`: String,
      event_id: String,
      sender: String,
      origin_server_ts: Long,
      unsigned: Option[Json]
  )
  case class Event(
      content: Json,
      `type`: String,
      event_id: String,
      sender: String,
      origin_server_ts: Long,
      unsigned: Option[Json]
  )

  case class StateEvent(
      content: Json,
      `type`: String,
      event_id: String,
      sender: String,
      origin_server_ts: Long,
      unsigned: Option[Json],
      state_key: String,
      prev_content: Option[Json] = None,
      room_id: Option[String] = None
  )

  case class RoomMembersResponse(chunk: List[MemberEvent])
  case class MemberEvent(
      content: MemberEventContent,
      `type`: String,
      event_id: String,
      room_id: String,
      sender: String,
      origin_server_ts: String,
      unsigned: UnsignedData,
      prev_content: Option[MemberEventContent],
      state_key: String,
      invite_room_state: List[StrippedState]
  )
  case class UnsignedData(age: Int, related_because: Option[Event], transaction_id: String)
  case class MemberEventContent(
      avatar_url: Option[String] = None,
      displayname: Option[String] = None,
      membership: String,
      is_direct: Option[Boolean] = None,
      third_party_invite: Option[Invite] = None,
      unsigned: Option[UnsignedData] = None
  )
  case class StrippedState(content: MemberEventContent, state_key: String, `type`: String)
  case class Invite(display_name: String, signed: Signed)
  case class Signed(mxid: String, signatures: Map[String, Map[String, String]], token: String)

  case class JoinedMembersResponse(joined: List[RoomMember])
  case class RoomMember(display_name: Option[String], avatar_url: Option[String])

  case class RoomMessagesResponse(start: String, end: String, chunk: List[RoomEvent])
  case class RoomEvent(
      content: Json,
      `type`: String,
      event_id: String,
      room_id: String,
      sender: String,
      origin_server_ts: Long,
      unsigned: Json
  )

  case class SendRoomEventResponse(event_id: String)

  case class RedactEventRequest(reason: String)
  case class RedactEventResponse(event_id: String)
  case class RoomAliasEventContent(aliases: List[String] = List.empty)
  case class RoomNameEventContent(name: String)
  case class RoomTopicEventContent(topic: String)
  case class RoomCanonicalAliasEventContent(alias: String)
  case class RoomHistoryVisibilityEventContent(history_visibility: String)
  case class RoomGuestAccessEventContent(guest_access: String)
  case class RoomAvatarEventContent(url: String, info: Option[ImageInfo] = None)
  case class ImageInfo(
      h: Int,
      w: Int,
      mimetype: String,
      size: Int,
      thumbnail_url: String,
      thumbnail_file: String,
      thumbnail_info: String
  )
}
