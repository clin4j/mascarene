package org.mascarene.matrix.client.r0.model.keymanagement

case class QueryDeviceKeyRequest(timeout: Option[Int], device_keys: Map[String, List[String]], token: Option[String])
