package org.mascarene.matrix.client.r0.model.keymanagement

import io.circe.Json

case class QueryDeviceKeyResponse(failures: Map[String, Json], deviceKeys: Map[String, Map[String, DeviceKeys]])
