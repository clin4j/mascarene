package org.mascarene.matrix.client.r0.model.keymanagement

case class UploadDeviceKeyResponse(one_time_key_counts: Map[String, Int])
