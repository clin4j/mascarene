package org.mascarene.matrix.client.r0.model.keymanagement

case class UploadDeviceKeysRequest(device_keys: Option[DeviceKeys])

case class DeviceKeys(
    user_id: String,
    device_id: String,
    algorithms: List[String],
    keys: Map[String, String],
    signatures: Map[String, Map[String, String]]
)
