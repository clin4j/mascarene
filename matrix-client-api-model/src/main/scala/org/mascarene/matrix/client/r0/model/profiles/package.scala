/*
Copyright 2018 Nicolas Jouanin

This file is part of Plasma.

Plasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License 3.

Plasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>
 */
package org.mascarene.matrix.client.r0.model

package object profiles {
  case class UserDirectorySearchRequest(search_term: String, limit: Option[Int])
  case class UserDirectorySearchResponse(results: List[UserDirectorySearchResponseUser], limited: Boolean)
  case class UserDirectorySearchResponseUser(
      user_id: String,
      display_name: Option[String] = None,
      avatar_url: Option[String] = None
  )
  case class SetDisplayNameRequest(displayname: Option[String])
  case class GetDisplayNameResponse(displayname: Option[String])
  case class SetAvatarUrlRequest(avatar_url: Option[String])
  case class GetAvatarUrlResponse(avatar_url: Option[String])
  case class GetProfileResponse(avatar_url: Option[String], displayname: Option[String])
}
