package org.mascarene.matrix.client.r0

import akka.actor.typed.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.marshalling.Marshal
import akka.http.scaladsl.model.headers.{Authorization, OAuth2BearerToken}
import akka.http.scaladsl.model._
import akka.util.ByteString
import com.typesafe.scalalogging.LazyLogging
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport
import io.circe.{Decoder, Encoder, Printer}
import org.mascarene.matrix.client.r0.model.auth.AuthToken
import org.mascarene.sdk.matrix.core.HttpApiResponse

import scala.concurrent.Future
import scala.util.{Failure, Success}

trait HttpTransport extends Transport with LazyLogging with FailFastCirceSupport {
  implicit def actorSystem: ActorSystem[Nothing]

  private implicit val ec = actorSystem.executionContext

  private val jsonPrinter = Printer.noSpaces.copy(dropNullValues = true)
  private val httpClient  = Http()

  protected def responseDecodeHandler[R](entity: String)(implicit decoder: Decoder[R]): R

  protected def doPut[Q, R](
      uri: Uri,
      request: Q,
      additionalHeaders: Seq[HttpHeader] = Seq.empty,
      queries: Map[String, String] = Map.empty
  )(implicit encoder: Encoder[Q], decoder: Decoder[R], token: Option[AuthToken] = None): Future[HttpApiResponse[R]] =
    Marshal(request).to[RequestEntity].map(_.withContentType(MediaTypes.`application/json`)).flatMap { jsonEntity =>
      doHttpCall(
        HttpRequest(
          method = HttpMethods.PUT,
          uri = uri.withQuery(Uri.Query(queries)),
          headers = additionalHeaders ++ (token match {
            case Some(t) => Seq(Authorization(OAuth2BearerToken(t.value)))
            case None    => Seq.empty
          }),
          entity = jsonEntity
        )
      )
    }

  protected def doPost[Q, R](
      uri: Uri,
      request: Q,
      additionalHeaders: Seq[HttpHeader] = Seq.empty,
      queries: Map[String, String] = Map.empty
  )(implicit encoder: Encoder[Q], decoder: Decoder[R], token: Option[AuthToken] = None): Future[HttpApiResponse[R]] =
    Marshal(request).to[RequestEntity].map(_.withContentType(MediaTypes.`application/json`)).flatMap { jsonEntity =>
      doHttpCall(
        HttpRequest(
          method = HttpMethods.POST,
          uri = uri.withQuery(Uri.Query(queries)),
          headers = additionalHeaders ++ (token match {
            case Some(t) => Seq(Authorization(OAuth2BearerToken(t.value)))
            case None    => Seq.empty
          }),
          entity = jsonEntity
        )
      )
    }

  /**
    * Perform an HTTP GET and returns either a server response object (Right)
    * or an error (Left) which can be a ServerError (server side) or a ClientError (client side)
    * @param uri
    * @param decoder
    * @tparam R Type of the object encoded returned json
    * @return
    */
  protected def doGet[R](
      uri: Uri,
      additionalHeaders: Seq[HttpHeader] = Seq.empty,
      queries: Map[String, String] = Map.empty
  )(implicit decoder: Decoder[R], token: Option[AuthToken] = None): Future[HttpApiResponse[R]] =
    doHttpCall(
      HttpRequest(
        method = HttpMethods.GET,
        uri = uri.withQuery(Uri.Query(queries)),
        headers = additionalHeaders ++ (token match {
          case Some(t) => Seq(Authorization(OAuth2BearerToken(t.value)))
          case None    => Seq.empty
        }),
        entity = HttpEntity.empty(MediaTypes.`application/json`)
      )
    )

  protected def doDelete[R](
      uri: Uri,
      additionalHeaders: Seq[HttpHeader] = Seq.empty,
      queries: Map[String, String] = Map.empty
  )(implicit decoder: Decoder[R], token: Option[AuthToken] = None): Future[HttpApiResponse[R]] =
    doHttpCall(
      HttpRequest(
        method = HttpMethods.DELETE,
        uri = uri.withQuery(Uri.Query(queries)),
        headers = additionalHeaders ++ (token match {
          case Some(t) => Seq(Authorization(OAuth2BearerToken(t.value)))
          case None    => Seq.empty
        }),
        entity = HttpEntity.empty(MediaTypes.`application/json`)
      )
    )

  private def doHttpCall[R](
      request: HttpRequest
  )(implicit decoder: Decoder[R]): Future[HttpApiResponse[R]] = {
    logRequest(request)
    httpClient
      .singleRequest(request)
      .flatMap { response =>
        val decodeFuture = response.entity.dataBytes
          .runFold(ByteString(""))(_ ++ _)
          .map(body => HttpApiResponse(responseDecodeHandler(body.utf8String), response.headers))
        logger.whenDebugEnabled {
          decodeFuture
            .onComplete {
              case Success(payload) =>
                logger.debug(
                  s"${request.method.value} ${request.uri} <- ${response.status} : $payload"
                )
              case Failure(t) =>
                logger.debug(
                  s"${request.method.value} ${request.uri} <- ${response.status} : ${t.getMessage}"
                )
            }
        }
        decodeFuture
      }
  }

  private def logRequest(request: HttpRequest): Unit =
    logger.whenDebugEnabled {
      request.entity.dataBytes
        .runFold(ByteString(""))(_ ++ _)
        .map(_.utf8String)
        .onComplete {
          case Success(payload) =>
            logger.debug(s"${request.method.value} ${request.uri} ${request.headers} -> $payload")
          case _ => logger.debug(s"${request.method.value} ${request.uri}")
        }
    }
}
