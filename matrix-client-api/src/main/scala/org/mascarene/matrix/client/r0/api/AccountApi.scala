/*
Copyright 2018 Nicolas Jouanin

This file is part of Plasma.

Plasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License 3.

Plasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>
 */
package org.mascarene.matrix.client.r0.api

import akka.actor.typed.ActorSystem
import akka.http.scaladsl.model.Uri
import io.circe.generic.auto._
import org.mascarene.matrix.client.r0.Transport
import org.mascarene.matrix.client.r0.model.auth.{AuthToken, _}

import scala.concurrent.Future

trait AccountApi { self: Transport =>
  implicit def actorSystem: ActorSystem[Nothing]

  implicit private val ec = actorSystem.executionContext

  protected def apiRoot: Uri

  def register(kind: String, request: RegisterRequest): Future[RegisterResponse] =
    doPost[RegisterRequest, RegisterResponse](
      apiRoot.withPath(apiRoot.path + "/r0/register"),
      request,
      Seq.empty,
      Map("kind" -> kind)
    ).map(_.entity)

  def requestToken(request: RequestTokenRequest): Future[Unit] =
    doPost[RequestTokenRequest, Unit](apiRoot.withPath(apiRoot.path + "/r0/register/email/requestToken"), request)
      .map(_.entity)

  def requestTokenThreePid(request: RequestTokenRequest): Future[Unit] =
    doPost[RequestTokenRequest, Unit](apiRoot.withPath(apiRoot.path + "/r0/account/3pid/email/requestToken"), request)
      .map(_.entity)

  def requestToken(implicit token: AuthToken): Future[Unit] =
    doPost[Unit, Unit](apiRoot.withPath(apiRoot.path + "/r0/account/password/email/requestToken"), ()).map(_.entity)

  def changePassword(request: PasswordRequest): Future[Unit] =
    doPost[PasswordRequest, Unit](apiRoot.withPath(apiRoot.path + "/r0/account/password"), request).map(_.entity)

  def deactivate(request: DeactivateRequest): Future[Unit] =
    doPost[DeactivateRequest, Unit](apiRoot.withPath(apiRoot.path + "/r0/account/deactivate"), request).map(_.entity)

  def getThreePids(implicit token: AuthToken): Future[GetThreePidsResponse] =
    doGet[GetThreePidsResponse](apiRoot.withPath(apiRoot.path + "/account/3pids")).map(_.entity)

  def postThreePid(request: PostThreePidRequest)(implicit token: AuthToken): Future[Unit] =
    doPost[PostThreePidRequest, Unit](apiRoot.withPath(apiRoot.path + "/r0/account/3pid"), request).map(_.entity)

  def whoAmI(implicit token: AuthToken): Future[WhoAmIResponse] =
    doGet[WhoAmIResponse](apiRoot.withPath(apiRoot.path + "/r0/account/whoami")).map(_.entity)
}
