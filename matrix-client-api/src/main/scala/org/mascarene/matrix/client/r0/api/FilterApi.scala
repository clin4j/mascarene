/*
Copyright 2018 Nicolas Jouanin

This file is part of Plasma.

Plasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License 3.

Plasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>
 */
package org.mascarene.matrix.client.r0.api

import akka.actor.typed.ActorSystem
import akka.http.scaladsl.model.Uri
import io.circe.generic.auto._
import org.mascarene.matrix.client.r0.Transport
import org.mascarene.matrix.client.r0.model.auth.AuthToken
import org.mascarene.matrix.client.r0.model.filter._

import scala.concurrent.Future

trait FilterApi { self: Transport =>
  implicit def actorSystem: ActorSystem[Nothing]
  implicit private val ec = actorSystem.executionContext

  protected def apiRoot: Uri

  def uploadFilter(userId: String, request: FilterDefinition)(implicit token: AuthToken): Future[PostFilterResponse] =
    doPost[FilterDefinition, PostFilterResponse](apiRoot.withPath(apiRoot.path + s"/r0/user/$userId/filter"), request)
      .map(_.entity)

  def getFilter(userId: String, filterId: String)(implicit token: AuthToken): Future[GetFilterResponse] =
    doGet[GetFilterResponse](apiRoot.withPath(apiRoot.path + s"/r0/user/$userId/filter/$filterId")).map(_.entity)
}
