package org.mascarene.matrix.client.r0.validation.rooms

import cats.data._
import cats.data.Validated._
import cats.implicits._
import org.mascarene.matrix.client.r0.model.rooms.CreateRoomRequest
import org.mascarene.sdk.matrix.core.ApiError

sealed trait DomainValidation {
  def error: ApiError
}
case object InvalidVisibilityValue extends DomainValidation {
  override def error: ApiError = ApiError("M_UNRECOGNIZED", Some("visibility must be one of [\"public\",\"private\"]"))
}
case class UnsupportedRoomVersion(supportedRoomVersions: String) extends DomainValidation {
  override def error: ApiError =
    ApiError(
      "M_UNSUPPORTED_ROOM_VERSION",
      Some(s"Unsupported room version. Supported room versions are [$supportedRoomVersions]")
    )
}
case object InvalidPresetValue extends DomainValidation {
  override def error: ApiError =
    ApiError(
      "M_UNRECOGNIZED",
      Some("preset must be one of [\"private_chat\", \"public_chat\", \"trusted_private_chat\"]")
    )
}

object RoomsValidator {
  type ValidationResult[A] = ValidatedNec[DomainValidation, A]

  private def validateVisibility(visibility: Option[String]): ValidationResult[String] =
    visibility match {
      case None => "private".validNec
      case Some(v) =>
        if (Set("public", "private").contains(v))
          v.validNec
        else
          InvalidVisibilityValue.invalidNec
    }

  private def validatePreset(preset: Option[String]): ValidationResult[Option[String]] =
    if (preset.isDefined) {
      if (Set("private_chat", "public_chat", "trusted_private_chat").contains(preset.get))
        preset.validNec
      else InvalidPresetValue.invalidNec
    } else
      preset.validNec

  def validate(request: CreateRoomRequest): ValidationResult[CreateRoomRequest] = {
    (validateVisibility(request.visibility), validatePreset(request.preset)).mapN { (visibility, preset) =>
      request.copy(visibility = Some(visibility), preset = preset)
    }
  }
}
