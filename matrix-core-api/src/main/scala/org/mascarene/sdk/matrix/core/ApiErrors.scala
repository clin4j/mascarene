package org.mascarene.sdk.matrix.core

object ApiErrors {
  def InternalError(message: Option[String] = None): ApiError  = ApiError("ORG.MASCARENE.HS.INTERNAL_ERROR", message)
  def ForbiddenError(message: Option[String] = None): ApiError = ApiError("M_FORBIDDEN", message)
  def Unrecognized(message: Option[String] = None): ApiError   = ApiError("M_UNRECOGNIZED", message)
  def BadJson(message: Option[String] = None): ApiError        = ApiError("M_BAD_JSON", message)
  def NotFound(message: Option[String] = None): ApiError       = ApiError("M_NOT_FOUND", message)
  def Unknown(message: Option[String] = None): ApiError        = ApiError("M_UNKWOWN", message)
  def UnAuthorized(message: Option[String] = None): ApiError   = ApiError("M_UNAUTHORIZED", message)
}
