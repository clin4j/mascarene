/*
Copyright 2018 Nicolas Jouanin

This file is part of Plasma.

Plasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License 3.

Plasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>
 */
package org.mascarene.matrix.identity.model

package object association {
  case class LookupResponse(
      address: Option[String],
      medium: Option[String],
      mxid: Option[String],
      not_before: Option[Int],
      not_after: Option[Int],
      ts: Option[Int],
      signatures: Map[String, Map[String, String]]
  )
}
