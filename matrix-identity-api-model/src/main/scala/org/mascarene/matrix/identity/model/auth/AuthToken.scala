package org.mascarene.matrix.identity.model.auth

case class AuthToken(value: String)
