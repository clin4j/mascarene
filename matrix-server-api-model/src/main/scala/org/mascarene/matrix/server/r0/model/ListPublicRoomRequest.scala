package org.mascarene.matrix.server.r0.model

case class ListPublicRoomsRequest(
    limit: Option[Int] = None,
    since: Option[String] = None,
    filter: Option[Filter] = None,
    include_all_networks: Option[Boolean] = None,
    third_party_instance_id: Option[String] = None
)
case class Filter(generic_search_term: Option[String] = None)
