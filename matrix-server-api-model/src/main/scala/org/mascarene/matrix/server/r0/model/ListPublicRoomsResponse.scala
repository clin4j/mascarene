package org.mascarene.matrix.server.r0.model

case class ListPublicRoomsResponse(
    chunk: List[PublicRoomsChunk],
    next_batch: Option[String],
    prev_batch: Option[String],
    total_room_count_estimate: Option[Int]
)

case class PublicRoomsChunk(
    room_id: String,
    aliases: Option[List[String]] = None,
    canonical_alias: Option[String] = None,
    name: Option[String] = None,
    num_joined_members: Int = 0,
    topic: Option[String] = None,
    world_readable: Boolean = false,
    guest_can_join: Boolean = false,
    avatar_url: Option[String] = None
)
