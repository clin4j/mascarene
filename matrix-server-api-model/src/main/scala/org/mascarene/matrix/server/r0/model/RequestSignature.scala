package org.mascarene.matrix.server.r0.model

case class RequestSignature[T](
    method: String,
    uri: String,
    origin: String,
    destination: String,
    content: Option[T] = None,
    signatures: Option[Map[String, Map[String, String]]] = None
)
