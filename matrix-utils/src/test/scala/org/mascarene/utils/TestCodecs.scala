package org.mascarene.utils

import org.scalatest.wordspec.AnyWordSpecLike
import org.scalatest.matchers.should.Matchers

class TestCodecs extends AnyWordSpecLike with Matchers {
  "Codecs base64 encoder/decoder" must {
    "provide same string when encode/decode" in {
      val testString = "SomeTestString"
      Codecs.base64Decode(Codecs.base64Encode(testString)) shouldBe testString
    }
  }
}
